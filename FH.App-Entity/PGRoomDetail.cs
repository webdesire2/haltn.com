﻿namespace FH.App.Entity
{
   public class PGRoomDetail
    {
        public int RoomId { get; set; }
        public string RoomType { get; set; }
        public int ExpectedRent { get; set; }
        public int ExpectedDeposit { get; set; }
        public int TotalSeat { get; set; }
        public int AvailableSeat { get; set; }

        public int Quantity { get; set; }
    }
}
