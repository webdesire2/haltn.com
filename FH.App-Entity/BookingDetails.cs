﻿using System.Collections.Generic;

namespace FH.App.Entity
{
   public class BookingDetail
    {
        public int BookingId { get; set; }
        public int PropertyId { get; set; }   
        public string PropertyType { get; set; }
        public double TotalAmount { get; set; }
        public int GSTPercent { get; set; }
        public double GSTAmount { get; set; }        
        public double Discount { get; set; }
        public string CouponCode { get; set;}
        public double PayableAmount { get; set; }
        public int CreatedBy { get; set; }

        public virtual IEnumerable<BookingSegment> BookingSegment { get; set; }
        public virtual IEnumerable<BookingPayment> BookingPayment { get; set; }
    }

    public class BookingSegment
    {
        public int BookingID { get; set; }
        public int PropertyID { get; set;}        
        public int RoomID { get; set; }
        public int Qty { get; set; }
        public double Amount { get; set; }

        public string RoomType { get; set; }
    }
}
