﻿namespace FH.App.Entity
{
    public class CityAreaMaster
    {
        public string AreaName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CityId { get; set; }
    }
}
