﻿namespace FH.App.Entity
{
   public class PGRoomMeta
    {
        public string Url { get; set; }
        public string PropertyType { get; set; }
        public string Heading { get; set; }
    }
}
