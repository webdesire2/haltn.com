﻿
using System;

namespace FH.App.Entity
{
    public class UserDetail
    { 
        public string UserId { get; set; }  
        public string Name { get; set;}
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Password { get; set;}
        public string ConfirmPassword { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int? OTP { get; set; }
        public string OTPExpiredOn { get; set; }
        public string UserType { get; set; }
        public string Message { get; set; }
    }
}
