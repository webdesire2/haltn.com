﻿namespace FH.App.Entity
{
    public class AmenityMaster
    {
        public int AmenityId { get; set; }
        public string AmenityName { get; set; }
        public string AmenityType { get; set; }
    }
}
