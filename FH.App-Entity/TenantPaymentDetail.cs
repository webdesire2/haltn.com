﻿using System;

namespace FH.App.Entity
{
    public class TenantPaymentDetail
    {
        public int TenantPaymentId { get; set; }
        public int TenantId { get; set; }
        public int PaymentId { get; set; }
        public double DepositAmount { get; set; }
        public string PaymentType { get; set; }
        public int PaymentStatus { get; set; }
        public DateTime? RentForMonthStartDate { get; set; }
        public DateTime? RentForMonthEndDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public string PaymentMode { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
    }

    public class RTenantPaymentDetail
    {
        public int TenantPaymentId { get; set; }
        public int TenantId { get; set; }
        public int PaymentId { get; set; }
        public double DepositAmount { get; set; }
        public string PaymentType { get; set; }
        public string PaymentStatus { get; set; }
        public DateTime? RentForMonthStartDate { get; set; }
        public DateTime? RentForMonthEndDate { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public string PaymentMode { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
    }
}
