﻿using System;

namespace FH.App.Entity
{
    public class PropertyDetail_PG
    {
        public string PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string CompanyName { get; set; }
        public string PropertyType { get; set; }   
        public string UserType { get; set; }     
        public string Locality { get; set; }
        public string Street { get; set; }     
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string OfferTitle { get; set; }
        public int? OfferPercent { get; set; }
        public string RoomType { get; set; }
        public int ExpectedRent { get; set; }
        public int ExpectedDeposit { get; set; }
        public string TenantGender { get; set; }
        public string PreferredGuest { get; set; }
        public Nullable<DateTime> AvailableFrom { get; set; }
        public bool Fooding { get; set; }
        public bool Laundry { get; set; }
        public bool RoomCleaning { get; set; }        
        public string Description { get; set; }
    }
}
