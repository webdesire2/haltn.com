﻿using System.Collections.Generic;

namespace FH.App.Entity
{
    public class PropertyList
    {
        public string PropertyId { get; set; }
        public string CompanyName { get; set; }
        public string PropertyName { get; set; }
        public string PropertyType { get; set; }
        public string Locality { get; set; }
        public string ApartmentType { get; set; }
        public string BhkType { get; set; }
        public int ExpectedRent { get; set; }
        public int ExpectedDeposit { get; set; }
        public string PropertySize { get; set; }
        public string PreferredTenants { get; set; }
        public string TenantGender { get; set; }
        public string Parking { get; set; }
        public string LocationMapLink { get; set; }
        public int Bathrooms { get; set; }
        public int Balconies { get; set; }
        public string Furnishing { get; set; }
        public string ImageUrl { get; set; }
        public string PropertyUserType { get; set; }
        public string Address { get; set; }
        public string Street {  get; set; }
        public string Offer { get; set; }
        public IEnumerable<PropertyGallery> Galleries { get; set; }
    }

    public class PropertySearchParameter
    {
        public string PropertyType { get; set; }
        public string Locality { get; set; }
        public string City { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Apartment { get; set; }
        public string Bhk { get; set; }
        public string FurnishingType { get; set; }
        public string PreferredTenant { get; set; }
        public string Facing { get; set; }
        public string RoomType { get; set; }
        public string TenantGender { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string SearchText { get; set; }

        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDesc { get; set; }
        public string MetaJson { get; set; }

        public string Heading { get; set; }
        public string HeadingContent { get; set; }
             
             
    }    
}
