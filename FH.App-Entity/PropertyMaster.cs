﻿
using System.Collections.Generic;

namespace FH.App.Entity
{
    public class PropertyMaster
    {
        public PropertyDetail PropertyDetail { get; set; }
        public IEnumerable<PropertyAmenity> PropertyAmenities { get; set; }
        public IEnumerable<AmenityMaster> AmenityMaster { get; set; }
        public IEnumerable<FurnishingDetail> Furnishing { get; set; }
        public IEnumerable<PropertyGallery> Galleries { get; set; }       
    }
}
