﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FH.App_Entity.DTO
{
    public class SiteMapDTO
    {
        public string Url { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
