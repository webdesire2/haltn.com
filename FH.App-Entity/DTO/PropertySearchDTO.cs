﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FH.App.DTO
{
    public class PropertySearchDTO
    {
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDesc { get; set; }
    }
}
