﻿using System;

namespace FH.App.Entity
{
    public class MyOrderDTO
    {
        public int PaymentId { get; set; }
        public int PropertyId { get; set; }
        public int OrderId { get; set; }
        public string PaymentStatus { get; set; }
        public string PaymentType { get; set; }
        public double Amount { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime RentForMonthStartDate { get; set; }
        public DateTime RentForMonthEndDate { get; set; }
    }

    public class MyCurrentStayDTO
    {  
        public int TenantId { get; set; }
        public double MonthlyRent { get; set; }
        public double SecurityAmount { get; set; }
        public double SecurityDeposit { get; set; }
        public double SecurityAmountDue { get; set; }
        public int PropertyId { get; set; }
        public string OwnerName { get; set; } 
        public string OwnerMobile { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int RoomNo { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string PaymentType { get; set; }
        public string Mobile { get; set; }
        public string PropertyName { get; set;}
    }

    public class InvoiceDTO
    {
        public int PaymentId { get; set; }
        public string TenantName {get;set;}        
        public string TenantMobile { get; set; }
        public string OwnerName { get; set; }
        public string CompanyName { get; set; }
        public string OwnerMobile { get; set; }
        public string StatusMessage { get; set; }
        public string PropertyId { get; set; }
        public string Street { get; set; }
        public string PaymentType { get; set; }
        public DateTime? RentFromDate { get; set; }
        public DateTime? RentToDate { get; set; }
        public double Amount { get; set; }
        public string PaymentStatus { get; set; }
        public DateTime PaymentDate { get; set; }
        public string PaymentMode { get; set; }
        public string GSTNo { get; set; }
        public string PANNo { get; set; }
        public string PropertyName { get; set;}
       
    }
}
