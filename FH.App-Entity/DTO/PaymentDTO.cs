﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FH.App.Entity
{
    public class PaymentDetailDTO
    {
        public int PaymentId { get; set; }
        public string PaymentType { get; set; }
        public DateTime? PaymentDate { get; set; }
        public double Amount { get; set; }
        public string UseCode { get; set; }
        public double UseCodeAmount { get; set; }
        public string PaymentMode { get; set; }
        public string Status { get; set; }
        public string StatusMessage { get; set; }
    }
}
