﻿
using System;

namespace FH.App.Entity
{
   public class UserWallet
    {
        public int WalletId { get; set; }
        public int UserId { get; set; }
        public string TransactionType { get; set; }
        public double Amount { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
