﻿using System;

namespace FH.App.Entity
{
    public class PaymentDetail
    {
        public int PaymentId { get; set; }
        public int CreatedBy { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public string TracingId { get; set; }
        public string BankRefNo { get; set; }
        public string FailureMessage { get; set; }
        public string PaymentMode { get; set; }
        public string CardName { get; set; }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string GatewayFullResponse { get; set; }
        public string UseCode { get; set; }
        public double UseCodeAmount { get; set; }
        public string PaymentType { get; set; }
        public string PaymentMID { get; set; }
    }
}
