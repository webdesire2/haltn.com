﻿using System;
namespace FH.App.Entity
{    
    public class PropertyDetail
    {
        public string PropertyId { get; set; }
        public string CompanyName { get; set; }
        public string PropertyType { get; set; }    
        public string UserType { get; set; }    
        public string ApartmentType { get; set; }
        public string ApartmentName { get; set; }
        public string BhkType { get; set; }
        public string Locality { get; set; }
        public string Street { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string OfferTitle { get; set; }
        public int? OfferPercent { get; set; }

        public int ExpectedRent { get; set; }
        public int ExpectedDeposit { get; set; }
        public bool IsNegotiable { get; set; }
        public Nullable<DateTime> AvailableFrom { get; set; }
        public string PropertySize { get; set; }
        public string PropertyAge { get; set; }
        public string Furnishing { get; set; }
        public string Facing { get; set; }
        public string WaterSupply { get; set; }
        public string FloorNo { get; set; }
        public string TotalFloor { get; set; }
        public string Bathrooms { get; set; }
        public string Balconies { get; set; }        
        public string GateSecurity { get; set; }
        public string Parking { get; set; }        
        public string Description { get; set; }
        public string RoomType { get; set; }
        public string PreferredTenant { get; set; }
        
    }
    
}
