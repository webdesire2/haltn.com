﻿using System;
using System.Collections.Generic;

namespace FH.App_Entity
{
    public class SeoDE
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Url { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDesc { get; set; }
        public string MetaJson { get; set; }
        public int UserId { get; set; }

        public int PageSize { get; set; }
        public int PageNo { get; set; }

        public int PropertyId { get; set; }
        public int CategoryId { get; set; }

        public string City { get; set; }

        public string Lat { get; set; }
        public string Lng { get; set; }

        public string Heading { get; set; }
        public string HeadingContent { get; set; }

        public string PropertyType { get; set; }
        public string Prefix { get; set; }

        public string SearchKey { get; set; }
        public int LocalityId { get; set; }
    }

    public class SEOCategory
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Url { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDesc { get; set; }
        public string MetaJson { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string City { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }

        public string Heading { get; set; }
        public string HeadingContent { get; set; }

        public string PropertyType { get; set; }
        public string Prefix { get; set; }
    }

    public class SEOCategoryVM
    {
        public string Category { get; set; }
        public int PageSize { get; set; }
        public int TotalRecord { get; set; }
        public int PageNo { get; set; }
        public IEnumerable<SEOCategory> SEOCategories { get; set; }
    }
}
