﻿
using System.Collections.Generic;

namespace FH.App.Entity
{
    public class PropertyMaster_PG
    {
        public PropertyDetail_PG PropertyDetail_PG { get; set; }
        public IEnumerable<PGRoomDetail> RoomDetail { get; set; }
        public IEnumerable<RoomAmenity> RoomAmenities { get; set; }
        public IEnumerable<PGRule> PgRules { get; set; }
        public IEnumerable<PropertyAmenity> PropertyAmenities { get; set; }
        public IEnumerable<AmenityMaster> AmenityMaster { get; set; }
        public IEnumerable<PropertyGallery> Galleries { get; set; }
    }
}
