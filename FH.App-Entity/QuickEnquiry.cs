﻿
namespace FH.App.Entity
{
    public class QuickEnquiry
    {
        public string UserName { get; set; }
        public string UserMobile { get; set; }
        public string UserEmail { get; set; }
        public string Description { get; set; }
    }
}
