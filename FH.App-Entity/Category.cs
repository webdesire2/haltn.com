﻿namespace FH.App.Entity
{
    public class CategoryData
    {
        public string Id { get; set; }
        public string Category  { get; set; }
        public string Url { get; set; }
        public string City { get; set; }
        public string Lat { get; set; }
        public int Lng { get; set; }
        public int Heading { get; set; }
        public string PropertyType { get; set; }
    }

 
}
