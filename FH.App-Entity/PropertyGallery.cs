﻿namespace FH.App.Entity
{
    public class PropertyGallery
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
