﻿using System;
namespace FH.App.Entity
{
    public class PropertyFeedBack
    {
        public int PropertyFeedBackId { get; set; }
        public int PropertyId { get; set; }
        public int FoodQuality { get; set; }
        public int Security { get; set; }
        public int Wifi { get; set; }
        public int Staff { get; set; }
        public int Maintenance { get; set; }
        public string Description { get; set; }
        public string FeedBackByName { get; set; }
        public int? CreatedBy { get; set; }
        
    }
}
