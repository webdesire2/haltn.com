﻿using System.Collections;

namespace FH.App.Entity
{
    public class BookingPayment
    {
        public int Id { get; set; }
        public int BookingDetailId { get; set; }
        public int PropertyId { get; set; }
        public int UserId { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public string TracingId { get; set; }
        public string BankRefNo { get; set; }
        public string FailureMessage { get; set; }
        public string PaymentMode { get; set; }
        public string CardName { get; set; }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }        
    }
}
