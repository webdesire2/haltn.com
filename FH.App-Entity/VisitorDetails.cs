﻿
using System;

namespace FH.App.Entity
{
    public class LeadDetails
    {        
        public string PropertyId { get; set; }
        public string UserId { get; set; }        
        public string UserName { get; set; }
        public string UserMobile { get; set; }
        public string VisitDate { get; set; }
        public string VisitTime { get; set; }
        public int OTP { get; set; }
        public string OTPExpiredOn { get; set; }
        public string Message { get; set; }
        public string PropertyDetail { get; set; }
        public string PropertyUserType { get; set; }
        public string PropertyUserName { get; set; }
        public string PropertyUserMobile { get; set; }   
        public string LocationMapLink { get; set; }
        
    }
}
