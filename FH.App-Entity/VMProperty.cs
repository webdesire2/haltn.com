﻿using System.Collections.Generic;

namespace FH.App.Entity
{
    public class VMProperty
    {
        public PropertyMaster PropertyMaster { get; set; }
        public PropertyMaster_PG PropertyMaster_PG { get; set; }
        public IEnumerable<PropertyList> SimilarProperties { get; set; }
        public IEnumerable<PropertyFeedBack> PGFeedBack { get; set; }

        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDesc { get; set; }
        public string MetaJson { get; set; }
    }
}
