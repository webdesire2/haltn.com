﻿
namespace FH.App.Entity
{
    public class FurnishingDetail
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
