﻿using System;
using System.Data;
using FH.App.Data.Repositories;

namespace FH.App.Service
{
    public class UnitOfWork:IDisposable
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;

        private IUserBL _user;
        private IPropertyBL _property;
        //private ISeoBL _seo;
        
        private SellerAgreementBL _sellerAgreementBL;
        public UnitOfWork()
        {           
            _connection = ConnectionFactory.GetConnection;
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }
         
        public IUserBL UserBL
        {
            get
            {
                return _user ?? (_user = new UserBL(_transaction));
            }
        }

        public IPropertyBL PropertyBL
        {
            get
            {
                return _property ?? (_property = new PropertyBL(_transaction));
            }
        }

        public SellerAgreementBL SellerAgreementBL
        {
            get
            {
                return _sellerAgreementBL ?? (_sellerAgreementBL = new SellerAgreementBL(_transaction));
            }
        }
        
        private void Reset()
        {
            _user = null;
            _property = null;            
            _sellerAgreementBL= null;
        }
        public void Commit()
        {           
            try
            {
                _transaction.Commit();              
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                if (_transaction != null)
                {
                    _transaction.Dispose();
                    _transaction = null;
                }
                if (_connection != null)
                {
                    _connection.Dispose();
                    _connection = null;
                }

                Reset();
            }
        }

        public void Dispose()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }

            if (_connection != null)
            {
                _connection.Dispose();
                _connection = null;
            }
        }
     
    }
}
