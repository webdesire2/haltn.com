﻿using FH.App.Entity;
using FH.App.Data;
using System.Data;
using System.Collections.Generic;
using System;
using FH.App_Entity;

namespace FH.App.Service
{
    public class PropertyBL:IPropertyBL
    {
        private readonly IPropertyDA propertyDA;        
        public PropertyBL(IDbTransaction transaction)
        {
            propertyDA = new PropertyDA(transaction);
        }
        public IEnumerable<PropertyList> GetAllProperty(PropertySearchParameter input, out int totalRecord)
        {
            return propertyDA.GetAllProperty(input,out totalRecord);
        }       

        public VMProperty GetProperty(int propertyId)
        {
            return propertyDA.GetProperty(propertyId);
        }

        public LeadDetails ScheduleVisitVerify(LeadDetails entity)
        {
            return propertyDA.ScheduleVisitVerify(entity);
        }

        public LeadDetails ScheduleVisit(LeadDetails entity)
        {
            return propertyDA.ScheduleVisit(entity);
        }
        public LeadDetails VeriyEnquiryDetail(LeadDetails entity)
        {
            return propertyDA.VeriyEnquiryDetail(entity);
        }

        public LeadDetails EnquiryDetail(LeadDetails entity)
        {
            return propertyDA.EnquiryDetail(entity);
        }
        public IEnumerable<PropertyList> GetFlat(bool isFurnish, bool isForBechelors)
        {
           return propertyDA.GetFlat(isFurnish,isForBechelors);
        }
        public IEnumerable<PropertyList> GetFlatMate(string gender)
        {
            return propertyDA.GetFlatMate(gender);
        }
        public IEnumerable<PropertyList> GetPG(string gender)
        {
            return propertyDA.GetPG(gender);
        }
        public IEnumerable<PropertyList> GetRoom()
        {
           return propertyDA.GetRoom();
        }       
        public IEnumerable<PropertyList> GetWishListProperty(string propertyIds)
        {
            return propertyDA.GetWishListProperty(propertyIds);
        }
        public IEnumerable<PropertyList> GetSimilarProperty(PropertySearchParameter input)
        {
            return propertyDA.GetSimilarProperty(input);
        }
        public IEnumerable<CityAreaMaster> GetCityLocalityList()
        {
            return propertyDA.GetCityLocalityList();
        }

        public IEnumerable<PropertyFeedBack> GetPGFeedBack(int pid)
        {
            return propertyDA.GetPGFeedBack(pid);
        }

        public bool AddPGFeedback(PropertyFeedBack entity)
        {
            try
            {
                propertyDA.AddPGFeedback(entity);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public dynamic GetPropertyListMetaTag(string categorySlug)
        {
            return propertyDA.GetPropertyListMetaTag(categorySlug);
        }

        public IEnumerable<PGRoomMeta> GetPGRoomMetaDetails()
        {
            return propertyDA.GetPGRoomMetaDetails();
        }
        public IEnumerable<SEOCategory> GetSimilarCategories(string input)
        {
            return propertyDA.GetSimilarCategories(input);
        }
    }

    public interface IPropertyBL
    {
        IEnumerable<PropertyList> GetAllProperty(PropertySearchParameter input, out int totalRecord);        
        VMProperty GetProperty(int propertyId);
        LeadDetails ScheduleVisitVerify(LeadDetails entity);
        LeadDetails ScheduleVisit(LeadDetails entity);
        LeadDetails VeriyEnquiryDetail(LeadDetails entity);
        LeadDetails EnquiryDetail(LeadDetails entity);        
        IEnumerable<PropertyList> GetWishListProperty(string propertyIds);
        IEnumerable<PropertyList> GetSimilarProperty(PropertySearchParameter input);
        IEnumerable<PropertyList> GetFlat(bool isFurnish, bool isForBechelors);
        IEnumerable<PropertyList> GetFlatMate(string gender);
        IEnumerable<PropertyList> GetPG(string gender);
        IEnumerable<PropertyList> GetRoom();
        IEnumerable<CityAreaMaster> GetCityLocalityList();
        IEnumerable<PropertyFeedBack> GetPGFeedBack(int pid);
        bool AddPGFeedback(PropertyFeedBack entity);
        dynamic GetPropertyListMetaTag(string categorySlug);
        IEnumerable<PGRoomMeta> GetPGRoomMetaDetails();
        IEnumerable<SEOCategory> GetSimilarCategories(string input);
    }
}
