﻿using System.Data;
using FH.App.Data;
using FH.App.Entity;

namespace FH.App.Service
{
    public class UserBL:IUserBL
    {
        private readonly IUserDA _IUserDA;

        public UserBL(IDbTransaction transaction)
        {
            _IUserDA = new UserDA(transaction);
        }

        public UserDetail FindByEmailMobile(UserDetail entity)
        {
            return _IUserDA.FindByEmailMobile(entity);
        }

        public int AddUser(UserDetail entity)
        {
           return _IUserDA.AddUser(entity);
        }
        public UserDetail IsValidLogin(UserDetail entity)
        {
            return _IUserDA.IsValidLogin(entity);
        }

        public UserDetail ResetPassword(UserDetail user)
        {
            return _IUserDA.ResetPassword(user);
        }
        public UserDetail IsValidOTP(int userId, int otp, System.DateTime otpExpiredOn)
        {
            return _IUserDA.IsValidOTP(userId, otp, otpExpiredOn);
        }

        public string GetUserByMobile(UserDetail entity)
        {
            return _IUserDA.GetUserByMobile(entity);
        }

        public TenantPaymentDetail GetPaymentLinkDetail(int PaymentLinkId)
        {
            return _IUserDA.GetPaymentLinkDetail(PaymentLinkId);
        }


        public UserDetail GetUserByUserId(int userId)
        {
            return _IUserDA.GetUserByUserId(userId);
        }

        public void UpdateUser(UserDetail entity)
        {
             _IUserDA.UpdateUser(entity);
        }
        public void AddQuickEnquiry(QuickEnquiry entity)
        {
            _IUserDA.AddQuickEnquiry(entity);
        }

        public MyCurrentStayDTO GetMyCurrentStay(int TenantId)
        {
            return _IUserDA.GetMyCurrentStay(TenantId);
        }
        
        public MyOrderDTO GetAllPayment(int tenantId)
        {
            return _IUserDA.GetAllPayment(tenantId);
        }
    }

    public interface IUserBL
    {
        UserDetail FindByEmailMobile(UserDetail entity);
        int AddUser(UserDetail entity);
        UserDetail IsValidLogin(UserDetail entity);
        UserDetail ResetPassword(UserDetail user);
        UserDetail IsValidOTP(int userId, int otp, System.DateTime otpExpiredOn);
        string GetUserByMobile(UserDetail entity);
        UserDetail GetUserByUserId(int userId);
        void UpdateUser(UserDetail entity);
        void AddQuickEnquiry(QuickEnquiry entity);
        TenantPaymentDetail GetPaymentLinkDetail(int PaymentLinkId);
        MyCurrentStayDTO GetMyCurrentStay(int TenantId);
        MyOrderDTO GetAllPayment(int tenantId);
    }
}
 