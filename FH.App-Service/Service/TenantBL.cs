﻿using System;
using System.Collections.Generic;
using System.Linq;
using FH.App.Data;
using FH.App.Entity;

namespace FH.App.Service
{
    public class TenantBL
    {
        private readonly TenantDA _tenantDA;

        public TenantBL()
        {
            _tenantDA = new TenantDA();
        }
        
        public int AddNewPayment(TenantMaster de)
        {   
            return _tenantDA.ExecuteWithTransaction<int>(de, TenantCallValue.AddNewPayment);
        }

        public int AddRNewPayment(TenantMaster de)
        {
            return _tenantDA.ExecuteWithTransaction<int>(de, TenantCallValue.AddRNewPayment);
        }
        
        public int AddNewTanent(TenantMaster de)
        {
            return _tenantDA.ExecuteWithTransaction<int>(de, TenantCallValue.AddNewTanent);
        }
        

        public IEnumerable<MyOrderDTO> GetAllPayment(TenantMaster de)
        {
            return _tenantDA.Execute<MyOrderDTO>(de, TenantCallValue.GetAllPayment);
        }

        public IEnumerable<RTenantPaymentDetail> GetTenantPaymentLog(TenantMaster de)
        {
            return _tenantDA.Execute<RTenantPaymentDetail>(de, TenantCallValue.GetTenantPaymentLog);
        }

        public IEnumerable<PGRoomData> getAvailableRooms(TenantMaster de)
        {
            return _tenantDA.Execute<PGRoomData>(de, TenantCallValue.getAvailableRooms);
        }

        public IEnumerable<PGRoomData> SgetAvailableRooms(TenantMaster de)
        {
            return _tenantDA.Execute<PGRoomData>(de, TenantCallValue.SgetAvailableRooms);
        }
        public IEnumerable<PGRoomData> occupancyDetails(TenantMaster de)
        {
            return _tenantDA.Execute<PGRoomData>(de, TenantCallValue.occupancyDetails);
        } 


        public IEnumerable<PropertyList> getMyProperties(TenantMaster de)
        {
            return _tenantDA.Execute<PropertyList>(de, TenantCallValue.getMyProperties);
        }
        public IEnumerable<TenantMaster> GetMyTanent(TenantMaster de)
        {
            return _tenantDA.Execute<TenantMaster>(de, TenantCallValue.GetMyTanent);
        }

        public IEnumerable<TenantMaster> SGetMyTanent(TenantMaster de)
        {
            return _tenantDA.Execute<TenantMaster>(de, TenantCallValue.SGetMyTanent);
        } 

        public QueryResponse RSendPaymentLink(TenantMaster de)
        { 
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.RSendPaymentLink).FirstOrDefault();
        }

        public QueryResponse SSendPaymentLink(TenantMaster de)
        {
            return _tenantDA.Execute<QueryResponse>(de, TenantCallValue.SSendPaymentLink).FirstOrDefault();
        }

        public MyCurrentStayDTO GetSingleTenant(TenantMaster de)
        {
            return _tenantDA.Execute<MyCurrentStayDTO>(de, TenantCallValue.GetSingleTenant).FirstOrDefault();
        } 

        public MyCurrentStayDTO GetTanentStayDetails(TenantMaster de)
        {
            return _tenantDA.Execute<MyCurrentStayDTO>(de, TenantCallValue.GetTanentStayDetails).FirstOrDefault();
        }

        public IEnumerable<TenantMaster> GetRentPaidTanent(TenantMaster de)
        {
            return _tenantDA.Execute<TenantMaster>(de, TenantCallValue.GetRentPaidTanent);
        }

        public IEnumerable<TenantMaster> SGetRentPaidTanent(TenantMaster de)
        {
            return _tenantDA.Execute<TenantMaster>(de, TenantCallValue.SGetRentPaidTanent);
        }
        //
        public TenantPaymentDetail GetPaymentLinkDetail(TenantMaster de)
        {
            return _tenantDA.Execute<TenantPaymentDetail>(de, TenantCallValue.GetPaymentLinkDetail).FirstOrDefault();
        }

        public TenantMaster GetTenantMaster(TenantMaster de)
        {
            return _tenantDA.Execute<TenantMaster>(de, TenantCallValue.GetTenantMaster).FirstOrDefault();
        }

        public TenantMaster GetRTenantMaster(TenantMaster de)
        {
            return _tenantDA.Execute<TenantMaster>(de, TenantCallValue.GetRTenantMaster).FirstOrDefault();
        }

        public MyCurrentStayDTO GetMyCurrentStay(TenantMaster de)
        {
            return _tenantDA.Execute<MyCurrentStayDTO>(de, TenantCallValue.GetMyCurrentStay).FirstOrDefault();
        }       

        public PaymentDetailDTO GetPaymentDetail(TenantMaster de)
        {
            return _tenantDA.Execute<PaymentDetailDTO>(de, TenantCallValue.GetPaymentDetail).FirstOrDefault();
        }

        public int CheckOutStay(TenantMaster de)
        {
            return _tenantDA.Execute<int>(de, TenantCallValue.AddCheckOut).FirstOrDefault();
        }
    }
}
