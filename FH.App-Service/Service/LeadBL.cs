﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FH.App.Data;
using FH.App.Entity;


namespace FH.App.Service
{
    public class LeadBL
    {
        private readonly LeadDA _leadDA;
        public LeadBL()
        {
            _leadDA = new LeadDA();
        }

        public IEnumerable<AllLeadList> GetAllPGLead(LeadDE de)
        {
            return _leadDA.Execute<AllLeadList>(de, LeadCallValue.GetPGLeadByFilter);
        }
    }
}
