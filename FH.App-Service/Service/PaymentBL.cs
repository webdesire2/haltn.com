﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using CCA.Util;
using FH.App.Entity;
using System.Security.Cryptography;
using FH.App.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Paytm;

namespace FH.App.Service
{
    public class PaymentBL
    {
        private readonly PaymentDA _paymentDA;

        public PaymentBL()
        {
            _paymentDA = new PaymentDA();
        }

        public int UpdatePayment(PaymentDetail de)
        {
            return _paymentDA.Execute<int>(de, PaymentCallValue.UpdatePayment).FirstOrDefault();
        }

        public InvoiceDTO GetTenantInvoice(int paymentId,int userId)
        {
            PaymentDetail de = new PaymentDetail()
            {
                PaymentId=paymentId,
                CreatedBy=userId
            };

            return _paymentDA.Execute<InvoiceDTO>(de, PaymentCallValue.GetTenantInvoice).FirstOrDefault();
        }
            
        public string EPPaymentRequest(double amount,int orderId, TenantMaster tenantDetail)
        {
            try
            {
                CCACrypto ccaCrypto = new CCACrypto();
                string CCAMerchentID = ConfigurationManager.AppSettings["CCAMerchentID"].ToString();
                string CCAccessCode = ConfigurationManager.AppSettings["CCAccessCode"].ToString();
                string CCAWorkingKey = ConfigurationManager.AppSettings["CCAWorkingKey"].ToString();
                string CCARedirectUrl = ConfigurationManager.AppSettings["CCARedirectUrl"].ToString();
                string CCAProdUrl = ConfigurationManager.AppSettings["CCAProdUrl"].ToString();
                string ccaRequest = "";
                string strEncRequest = "";
                //billing_name,billing_address,billing_city,billing_state,billing_zip,billing_country,billing_tel,billing_email,customer_identifier
                ccaRequest = "merchant_id=" + CCAMerchentID + "&order_id=" + orderId + "&amount=" + amount + "&currency=INR&redirect_url=" + CCARedirectUrl + "&cancel_url=" + CCARedirectUrl + "&sub_account_id=" + tenantDetail.PaytmMID + "";
                ccaRequest += "&billing_name" + tenantDetail.Name;
                ccaRequest += "&billing_address" + tenantDetail.Name;
                ccaRequest += "&billing_city=Gurgaon";
                ccaRequest += "&billing_state=Haryana" ;
                ccaRequest += "&billing_zip=122001"; 
                ccaRequest += "&billing_country=India"; 
                ccaRequest += "&billing_tel=" + tenantDetail.Mobile;
                ccaRequest += "&billing_email=" + tenantDetail.Email;
                ccaRequest += "&customer_identifier=" + tenantDetail.TenantId;
                strEncRequest = ccaCrypto.Encrypt(ccaRequest, CCAWorkingKey);
                return strEncRequest;
            }
            catch(Exception ex)
            {
                throw new ApplicationException();
            }            
        }
        
        public string ErrorCode(string code)
        {
            string desc = "";
            switch(code)
            {
                case "E000": desc="Transaction Success";  break;
                case "E001": desc = "Unauthorized Payment Mode";break;
                case "E006":desc = "Transaction Already Paid, Received Confirmation from the Bank, Yet to Settle the transaction with the Bank";break;
                case "E007":desc = "Transaction Failed";break;
                case "E008":desc = "Failure from Third Party due to Technical Error";break;
                case "E0039":desc = "Mandatory value Email in wrong format";break;
                case "E00310":desc = "Mandatory value mobile number in wrong format";break;
                case "E00331":desc = "UPI Transaction Initiated Please Accept or Reject the Transaction";break;
                case "E0801":desc = "FAIL";break;
                case "E0803":desc = "Canceled by user";break;
                case "E0815":desc = "Not sufficient funds";break;

                default:desc = code; break;
            }

            return desc;
        }
        
        private string Encrypt(string textToEncrypt,string key)
        {
            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            rijndaelCipher.Mode = CipherMode.ECB;
            rijndaelCipher.Padding = PaddingMode.PKCS7; rijndaelCipher.KeySize = 0x80;
            rijndaelCipher.BlockSize = 0x80;
            byte[] pwdBytes = Encoding.UTF8.GetBytes(key);
            byte[] keyBytes = new byte[0x10];
            int len = pwdBytes.Length; if (len > keyBytes.Length) { len = keyBytes.Length; }
            Array.Copy(pwdBytes, keyBytes, len); rijndaelCipher.Key = keyBytes;
            rijndaelCipher.IV = keyBytes; ICryptoTransform transform = rijndaelCipher.CreateEncryptor();
            byte[] plainText = Encoding.UTF8.GetBytes(textToEncrypt);
            return Convert.ToBase64String(transform.TransformFinalBlock(plainText, 0, plainText.Length));
        }

        private string GenerateSHA512String(string inputString)
        {
            SHA512 sha512 = SHA512Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha512.ComputeHash(bytes);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i <= hash.Length - 1; i++)
            {
                stringBuilder.Append(hash[i].ToString("x2"));
            }
            return stringBuilder.ToString();

        }        
    }
}
