﻿
using System.Data;
using FH.App.Data.Repositories;

namespace FH.App.Service
{
    public class SellerAgreementBL
    {
        IDbTransaction _transaction;

        public SellerAgreementBL(IDbTransaction transaction)
        {
            _transaction = transaction;
        }

        public void UpdateAgreementStatus(int userId)
        {
            SellerAgreementDA da = new SellerAgreementDA(_transaction);
            da.UpdateAgreementStatus(userId);
        }
    }
}
