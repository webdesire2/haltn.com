﻿using FH.App_Data.Repositories;
using FH.App_Entity.DTO;
using System.Collections.Generic;

namespace FH.App.Service
{
    public class SitemapBL
    {
        private readonly SQLRepository _sqlRepositry;

        public SitemapBL()
        {
            _sqlRepositry = new SQLRepository();
        }

        public IEnumerable<SiteMapDTO> GetAllSEOCategory()
        {
            string query = $"select Url,CreatedOn from seo_category";
            return  _sqlRepositry.Execute<SiteMapDTO>(query);
        }
    }
}
