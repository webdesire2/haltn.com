﻿using FH.App.Entity;
using FH.App.Data;
using System.Linq;
using System.Collections.Generic;

namespace FH.App.Service
{
    public class UseCodeMasterBL
    {
        private readonly UseCodeMasterDA _useCodeDA;

        public UseCodeMasterBL()
        {
            _useCodeDA = new UseCodeMasterDA();
        }

        public UseCodeMaster GetByCode(UseCodeMaster de)
        {
            var data= _useCodeDA.Execute<UseCodeMaster>(de, UseCodeCallValue.GetByCode);           
            return data.FirstOrDefault();            
        }
        
        public IEnumerable<UseCodeMaster> GetAllActiveCode()
        {
            return _useCodeDA.Execute<UseCodeMaster>(new UseCodeMaster(), UseCodeCallValue.GetActiveCode);
        }
    }
}
