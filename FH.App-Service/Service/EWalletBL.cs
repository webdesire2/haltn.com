﻿using FH.App.Entity;
using FH.App.Data;
using System.Collections.Generic;
using System.Linq;

namespace FH.App.Service
{
    public class EWalletBL
    {
        private readonly EWalletDA _da;

        public EWalletBL()
        {
            _da = new EWalletDA();
        }

        public IEnumerable<UserWallet> GetAll(EWalletDE de)
        {
            return _da.Execute<UserWallet>(de, EWalletCallValue.GetAll);
        }

        public MyOrderDTO GetWalletAmount(EWalletDE de)
        {
            return _da.Execute<MyOrderDTO>(de, EWalletCallValue.GetWalletBalance).FirstOrDefault();
        }

        public MyOrderDTO AddDebitNode(EWalletDE de)
        {
            return _da.Execute<MyOrderDTO>(de, EWalletCallValue.AddDebitNode).FirstOrDefault();
        }

        public int AddOrder(EWalletDE de)
        {
            return _da.Execute<int>(de, EWalletCallValue.AddOrder).FirstOrDefault();
        }
    }
}
