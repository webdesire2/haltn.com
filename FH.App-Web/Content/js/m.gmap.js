﻿function nearBy() {
    var nearByPlace = {
        airport: [],
        bus_station: [],
        train_station: [],
        hospital: [],
        atm: [],
        school: [],
        movie_theature: [],
        shopping_mall: [],
        gym:[],
        restaurant: []        
    }
    map = {
        place: nearByPlace,
        currentPosition:{lat:parseFloat($('#hdnLat').val()), lng:parseFloat($('#hdnLng').val())},
        mapObject:null,
        currentPin: null,
        reset: function () {
            this.mapObject = new google.maps.Map(document.createElement('div'), {
                zoom: 14,
                maxZoom:14,
                center: map.currentPosition,
                disableDefaultUI: true,
                zoomControl: true,
                scrollwheel: false                
            });           
        },
        calculateDistance: function (a, b) {
            var service = new google.maps.DistanceMatrixService;
            for (var i = 0, g = []; i < a.length; i++) {
                g.push(a[i].geometry.location);
            }
            (new google.maps.DistanceMatrixService).getDistanceMatrix({
                origins: [map.currentPosition],
                destinations: g,
                travelMode:'DRIVING',
                unitSystem: google.maps.DirectionsUnitSystem.METRIC
            }, callback)
            function callback(result,status)
            {               
                for (var f = 0; f < result.destinationAddresses.length; f++) {
                    var name = a[f].name, lat, lng;
                    if (a[f].geometry ? (lat = a[f].geometry.location.lng(), lng = a[f].geometry.location.lat()) : (lat = a[f].latitude, lng = a[f].longitude), name.length >22 && (name = a[f].name.substring(0,22) + "...")) {
                        $('#' + b + '-list').append('<tr><td class="list-group-item nb-list-item" data-type="' + b + '" data-lat="' + lat + '" data-lng="' + lng + '" data-name="' + name + '" title="' + name + '">' + name + '<span class="float-right">' + result.rows[0].elements[f].distance.text + ' | ' + result.rows[0].elements[f].duration.text + '</span></td></tr>');
                    }
                    else
                    {
                        $('#' + b + '-list').append('<tr><td class="list-group-item nb-list-item" data-type="' + b + '" data-lat="' + lat + '" data-lng="' + lng + '" data-name="' + name + '" title="' + name + '">' + name + '<span class="float-right">' + result.rows[0].elements[f].distance.text + ' | ' + result.rows[0].elements[f].duration.text + '</span></td></tr>');
                    }

                }                    
            }                
        },
        showUtilityOnMap:function(a)
        {
            if (a == 'airport') {
                var airPortplace = new Array;
                if ($('#hdnCity').val()== 'GN')
                {                    
                    var f = {};
                    f.name = "Indira Gandhi International Airport ", f.geometry = {}, f.geometry.location = new google.maps.LatLng(28.54944559999999, 77.1180861), f.vicinity = "New Delhi, Delhi", airPortplace.push(f);
                }
                map.place[a] = airPortplace;
                map.calculateDistance(airPortplace, a);               
            }

            else
            if (map.place[a] == null || map.place[a].length==0) {
                var service = new google.maps.places.PlacesService(map.mapObject);
                service.nearbySearch({
                    location: this.currentPosition,
                    rankBy: google.maps.places.RankBy.DISTANCE,
                    type: a
                }, callback);

                function callback(results, status) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        if (results.length > 5)
                        { results = results.slice(0, 5); }

                        map.place[a] = results;
                        map.calculateDistance(results, a);                       
                    }
                }
            }
            else {
                map.calculateDistance(map.place[a], a);              
            }            
        },        
        showTransitInfo:function(a)
        {
            map.reset();
            if ($('#transitInfo').data("loaded") != 1) {
                map.showUtilityOnMap('airport'), map.showUtilityOnMap('bus_station'), map.showUtilityOnMap('train_station'), $('#transitInfo').data("loaded", 1);
            }
            else {
                (a==undefined || a=="")?(map.showAirport(), map.showBusStop(), map.showTrainStation()):
                      map.showUtilityOnMap(map.place[a])
                
            }
        },
        showEssentialInfo:function(a)
        {
            map.reset();
            if ($('#essentialInfo').data("loaded") != 1) {
                map.showUtilityOnMap('hospital'), map.showUtilityOnMap('atm'), map.showUtilityOnMap('school'), $('#essentialInfo').data("loaded", 1);
            }
            else {
                (a == undefined || a == "") ? (map.showHospital(), map.showBankAndATM(), map.showSchool()) :
                      map.showUtilityOnMap(map.place[a])
            }           
        },
        showUtilityInfo: function (a) {
            map.reset();
            if ($('#utilityInfo').data("loaded") != 1) {
                map.showUtilityOnMap('movie_theater'), map.showUtilityOnMap('shopping_mall'), map.showUtilityOnMap('gym'), $('#utilityInfo').data("loaded", 1);
            }
            else {
                (a == undefined || a == "") ? (map.showMovieTheature(), map.showShoppingMall(), map.showGym()) :
                      map.showUtilityOnMap(map.place[a])
            }           
        },
        showRestaurentInfo: function (a) {
            map.reset();
            if ($('#restaurentInfo').data("loaded") != 1) {
                map.showUtilityOnMap('restaurant'), $('#restaurentInfo').data("loaded", 1);
            }
            else {                
                (a == undefined || a == "") ? (map.showRestaurent()) :
                      map.showUtilityOnMap(map.place[a])
            }
        },
        showAirport:function()
        {
            map.showUtilityOnMap(map.place.airport);
        },        
        showBusStop: function () {
            map.showUtilityOnMap(map.place.bus_station);
        },
        showTrainStation: function () {
            map.showUtilityOnMap(map.place.train_station);
        },
        showHospital: function () {
            map.showUtilityOnMap(map.place.hospital);
        },
        showBankAndATM: function () {
            map.showUtilityOnMap(map.place.atm);
        },
        showSchool: function () {
            map.showUtilityOnMap(map.place.school);
        },
        showMovieTheature: function () {
            map.showUtilityOnMap(map.place.movie_theater);
        },
        showShoppingMall: function () {
            map.showUtilityOnMap(map.place.shopping_mall);
        },
        showGym: function () {
            map.showUtilityOnMap(map.place.gym);
        },
        showRestaurent: function () {
            map.showUtilityOnMap(map.place.restaurant);
        }
    }
}

