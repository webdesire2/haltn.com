﻿$('.bhk-type,.room-type,.furnishing,.tenant,.gender,.apartment,.preferred-tenant,.facing').click(function () {
    $(this).toggleClass('active');
    if ($(this).hasClass('bhk-type'))
    {
        if($(this).hasClass('active'))
            $('#hdnBhkType').val(concatData($('#hdnBhkType').val(), $(this).data('key')));
        else
            $('#hdnBhkType').val(removeData($('#hdnBhkType').val(), $(this).data('key')));
    }
    if ($(this).hasClass('apartment'))
    {
        if ($(this).hasClass('active'))
            $('#hdnApartment').val(concatData($('#hdnApartment').val(), $(this).data('key')));
        else
            $('#hdnApartment').val(removeData($('#hdnApartment').val(), $(this).data('key')));
    }
    if ($(this).hasClass('room-type'))
    {        
        if ($(this).hasClass('active'))
            $('#hdnRoomType').val(concatData($('#hdnRoomType').val(), $(this).data('key')));
        else
            $('#hdnRoomType').val(removeData($('#hdnRoomType').val(), $(this).data('key')));
    }
    if ($(this).hasClass('furnishing'))
    {        
        if ($(this).hasClass('active'))
            $('#hdnFurnishingType').val(concatData($('#hdnFurnishingType').val(), $(this).data('key')));
        else
            $('#hdnFurnishingType').val(removeData($('#hdnFurnishingType').val(), $(this).data('key')));
    }
    if ($(this).hasClass('tenant'))
    {        
        if ($(this).hasClass('active'))
            $('#hdnPrefferedTenant').val(concatData($('#hdnPrefferedTenant').val(), $(this).data('key')));
        else
            $('#hdnPrefferedTenant').val(removeData($('#hdnPrefferedTenant').val(), $(this).data('key')));
    }
    if ($(this).hasClass('gender'))
    {        
        if ($(this).hasClass('active'))
            $('#hdnTenantGender').val($(this).data('key'));
        else
            $('#hdnTenantGender').val($(this).data('key'));
    } 
    if ($(this).hasClass('preferred-tenant'))
    {        
        if ($(this).hasClass('active'))
            $('#hdnPreferredTenant').val(concatData($('#hdnPreferredTenant').val(), $(this).data('key')));
        else
            $('#hdnPreferredTenant').val(removeData($('#hdnPreferredTenant').val(), $(this).data('key')));
    }
    if($(this).hasClass('facing'))
    {        
        if ($(this).hasClass('active'))
            $('#hdnFacing').val(concatData($('#hdnFacing').val(), $(this).data('key')));
        else
            $('#hdnFacing').val(removeData($('#hdnFacing').val(), $(this).data('key')));
    }

    //propertyLoad('', true);
    filterClickEvent(1);
});

$('#btnLocalitySearch').click(function () {
    debugger;
    filterClickEvent(1);
});
function concatData(oldValue,newValue)
{       
    if(oldValue=="")
        return newValue;
    else if(newValue!="")
        return oldValue+"_"+newValue;
    else
        return oldValue;    
}

function removeData(oldValue,removeValue)
{
    var newValue="";
    var value=[];
    if(oldValue!="")
        value=oldValue.split('_');
    for(var i=0;i<value.length;i++)
    {
        if(value[i] !=removeValue)
        { if(newValue=="")newValue=value[i];else newValue=newValue+"_"+value[i]; }
    }
    return newValue;
}

function propertySearch() {    
    var city = $('#city option:selected').val();
    var locality = $('#txtLocality').val();
    var lat = $('#hdnLat').val();
    var lng = $('#hdnLng').val();
    var ptype = $('#search_tab_in li').find('span.currently').attr('id').toUpperCase();
    if (city.trim() == "") 
        return showError("Please select city.");        
    
    if (locality.trim() == "" || lat == "" || lng == "") 
        return showError("Please enter locality.");       
    
if (ptype == "FLAT") {
    var bhk="";
        $('input[name="bhk"]').each(function () {
            if ($(this).is(":checked")) {
                if (bhk == "")
                { bhk = $(this).data('value'); }
                else
                { bhk += "_" + $(this).data('value') }
            }
        });
         
    if(bhk=="")
        window.location.href = "/property/search?ptype=flat&city=" + city + "&locality=" + locality + "&lat=" + lat + "&lng=" + lng;
        else
        window.location.href = "/property/search?ptype=flat&city=" + city + "&locality=" + locality + "&lat=" + lat + "&lng=" + lng + "&bhkType=" + bhk;
    }
else if (ptype == "ROOM") {
    var roomType="";
        $('input[name="roomType"]').each(function () {
            if ($(this).is(":checked")) {
                if (roomType == "")
                { roomType = $(this).val(); }
                else
                { roomType += "_" + $(this).val() }
            }
        });

    if(roomType=="")
        window.location.href = "/property/search?ptype=Room&city=" + city + "&locality=" + locality + "&lat=" + lat + "&lng=" + lng;
        else
        window.location.href = "/property/search?ptype=Room&city=" + city + "&locality=" + locality + "&lat=" + lat + "&lng=" + lng + "&roomtype=" + roomType;
    }
else if(ptype == "PG" || ptype=="FLATMATE") {
        var gender = "";
        $('input[name="gender"]').each(function () {
            if ($(this).is(":checked")) {
                if (gender == "")
                { gender = $(this).val(); }
                else
                { gender += "_" + $(this).val() }
            }
        });

        if(gender=="")
            window.location.href = "/property/search?ptype="+ptype+"&city=" + city + "&locality=" + locality + "&lat=" + lat + "&lng=" + lng;
        else
            window.location.href = "/property/search?ptype="+ptype+"&city=" + city + "&locality=" + locality + "&lat=" + lat + "&lng=" + lng + "&gender=" + gender;
    }
}

function propertysearchByArea(locality,lat,lng)
{
    debugger;
    var propType = $('#localityListSection ul li').find('a.active').data('tab').toLowerCase();
    window.location.href = "/property/search?ptype="+propType+"&locality=" + urlString(locality) + "&lat=" + lat + "&lng=" + lng;        
}

function getPropertyFlat(city, locality, apartment, bhkType, furnishingType, preferredTenant, facing, minPrice, maxPrice, lat, lng, page) {
    $.post("/property/searchitem", { PropertyType: 'FLAT', City: city, Locality: locality, Apartment: apartment, Bhk: bhkType, FurnishingType: furnishingType, PreferredTenant: preferredTenant, Facing: facing, MinPrice: minPrice, MaxPrice: maxPrice, Latitude: lat, Longitude: lng, PageIndex: page }, function (data, status) {
        $('#dvLoadProperty').html(data);
        $('.loader').hide();
    });    
}

function getPropertyFlatmate(city, locality, apartment, bhkType, furnishingType, preferredTenant, facing, roomType, minPrice, maxPrice, lat, lng,page) {    
    $.post("/property/searchitem", { PropertyType: 'FLATMATE', City: city, Locality: locality, Apartment: apartment, Bhk: bhkType, FurnishingType: furnishingType, PreferredTenant: preferredTenant, Facing: facing, RoomType: roomType, MinPrice: minPrice, MaxPrice: maxPrice, Latitude: lat, Longitude: lng,PageIndex:page }, function (data, status) {
        $('#dvLoadProperty').html(data);
        $('.loader').hide();
    });
}

function getPropertyRoom(city, locality, apartment, roomType, preferredTenant, minPrice, maxPrice, lat, lng, page) {    
    $.post("/property/searchitem", { PropertyType: 'ROOM', City: city, Locality: locality, Apartment: apartment, RoomType: roomType, PreferredTenant: preferredTenant, MinPrice: minPrice, MaxPrice: maxPrice, Latitude: lat, Longitude: lng, PageIndex: page }, function (data, status) {
        $('#dvLoadProperty').html(data);
        $('.loader').hide();
    });
}

function getPropertyPG(city, locality, roomType, preferredGender, minPrice, maxPrice, lat, lng,page) {    
    $.post("/property/searchitem", { PropertyType: 'PG', City: city, Locality: locality, RoomType: roomType, TenantGender: preferredGender, MinPrice: minPrice, MaxPrice: maxPrice, Latitude: lat, Longitude: lng,PageIndex:page }, function (data, status) {
        $('#dvLoadProperty').html(data);
        $('.loader').hide();
    });
}

function propertyLoad(page, isPostBack) {    
    var city = $('#hdnCity').val(), locality = $('#hdnLocality').val(), minPrice = $('#hdnMinPrice').val(), maxPrice = $('#hdnMaxPrice').val();
    var lat = $('#hdnLat').val(), lng = $('#hdnLng').val();
    if ($('#hdnPtype').val().toUpperCase() == 'FLAT') {
        if(isPostBack)
        { $('.loader').show(); updateURL('flat', city, locality, $('#hdnApartment').val(), $('#hdnBhkType').val(), '', $('#hdnFurnishingType').val(), $('#hdnPreferredTenant').val(), '', $('#hdnFacing').val(), minPrice, maxPrice, lat, lng, page); }
        getPropertyFlat(city, locality, $('#hdnApartment').val(), $('#hdnBhkType').val(), $('#hdnFurnishingType').val(), $('#hdnPreferredTenant').val(), $('#hdnFacing').val(), minPrice, maxPrice, lat, lng, page);       
    }
    if ($('#hdnPtype').val().toUpperCase() == 'FLATMATE') {
        if (isPostBack)
        { $('.loader').show(); updateURL('flatmate', city, locality, $('#hdnApartment').val(), $('#hdnBhkType').val(), $('#hdnRoomType').val(), $('#hdnFurnishingType').val(), $('#hdnPreferredTenant').val(), '', $('#hdnFacing').val(), minPrice, maxPrice, lat, lng, page);}
        getPropertyFlatmate(city, locality, $('#hdnApartment').val(), $('#hdnBhkType').val(), $('#hdnFurnishingType').val(), $('#hdnPreferredTenant').val(), $('#hdnFacing').val(), $('#hdnRoomType').val(), minPrice, maxPrice, lat, lng, page);        
    }
    if ($('#hdnPtype').val().toUpperCase() == 'ROOM') {
        if (isPostBack)
        { $('.loader').show(); updateURL('room', city, locality, $('#hdnApartment').val(), '', $('#hdnRoomType').val(), '', $('#hdnPreferredTenant').val(), '', '', minPrice, maxPrice, lat, lng, page);}
        getPropertyRoom(city, locality, $('#hdnApartment').val(), $('#hdnRoomType').val(), $('#hdnPreferredTenant').val(), minPrice, maxPrice, lat, lng, page);        
    }
    if ($('#hdnPtype').val().toUpperCase() == 'PG') {
        if (isPostBack)
        { $('.loader').show(); updateURL('pg', city, locality, '', '', $('#hdnRoomType').val(), '', '', $('#hdnTenantGender').val(), '', minPrice, maxPrice, lat, lng, page); }
        getPropertyPG(city, locality, $('#hdnRoomType').val(), $('#hdnTenantGender').val(), minPrice, maxPrice, lat, lng, page);       
    }    

    $("html, body").animate({ scrollTop: 0 }, "fast");
} 

function Paging(page) {    
    //propertyLoad(page,true);
    filterClickEvent(page);
}


function filterClickEvent(page) {
    debugger;
    var type = $('#hdnPtype').val();
    var city = $('#hdnCity').val(), locality = $('#hdnLocality').val(), minPrice = $('#hdnMinPrice').val(), maxPrice = $('#hdnMaxPrice').val();
    var lat = $('#hdnLat').val(), lng = $('#hdnLng').val();

    if (type.toUpperCase() === "FLAT") {
        updateURL('flat', city, locality, $('#hdnApartment').val(), $('#hdnBhkType').val(), '', $('#hdnFurnishingType').val(), $('#hdnPreferredTenant').val(), '', $('#hdnFacing').val(), minPrice, maxPrice, lat, lng, page);
    }
    else if (type.toUpperCase() === "FLATMATE") {
        updateURL('flatmate', city, locality, $('#hdnApartment').val(), $('#hdnBhkType').val(), $('#hdnRoomType').val(), $('#hdnFurnishingType').val(), $('#hdnPreferredTenant').val(), '', $('#hdnFacing').val(), minPrice, maxPrice, lat, lng, page);
    }
    else if (type.toUpperCase() === "ROOM") {
        updateURL('room', city, locality, $('#hdnApartment').val(), '', $('#hdnRoomType').val(), '', $('#hdnPreferredTenant').val(), '', '', minPrice, maxPrice, lat, lng, page);
    }
    else if (type.toUpperCase() === "PG") {
        updateURL('pg', city, locality, '', '', $('#hdnRoomType').val(), '', '', $('#hdnTenantGender').val(), '', minPrice, maxPrice, lat, lng, page);
    }
}

function updateURL(pType,city, locality, apartment, bhkType,roomType ,furnishingType, preferredTenant,gender, facing, minPrice, maxPrice, lat, lng, page)
{
    var newUrl = "";
    if (city !== "" && city !== undefined)
        newUrl += "City=" + city; 
    if (locality !== "" && locality !== undefined)
        newUrl += "&Locality=" + locality;  
    if (pType !== "" && pType !== undefined)
        newUrl += "&PropertyType=" + pType;
    if (lat !== "" && lng !== "")
        newUrl += "&Latitude=" + lat + "&Longitude=" + lng; 
    if (apartment !== "" && apartment !== undefined)
        newUrl += "&Apartment=" + apartment; 
    if (bhkType !== "" && bhkType !== undefined)
        newUrl += "&Bhk=" + bhkType;  
    if (roomType !== "" && roomType !== undefined)
        newUrl += "&RoomType=" + roomType;  
    if (furnishingType != "" && furnishingType != undefined)
        newUrl += "&FurnishingType=" + furnishingType;  
    if (preferredTenant != "" && preferredTenant != undefined)
        newUrl += "&PreferredTenant=" + preferredTenant;
    if (gender != "" && gender != undefined)
        newUrl += "&TenantGender=" + gender;
    if (facing != "" && facing != undefined)
        newUrl += "&Facing=" + facing;
    
    if (parseInt(minPrice)!=0 || parseInt(maxPrice)!=200000)
        newUrl += "&MinPrice=" + minPrice + "&MaxPrice=" + maxPrice;

    if (page != "" && page != undefined && page != null)
        newUrl += "&PageIndex=" + page;
    
    if (history.pushState) {
        var url = window.location.protocol + "//" + window.location.host + "/property/search?" + newUrl;
        window.history.pushState({ path: url }, '', url);
    }

    location.reload();

}


function openWindow(e,url) {
    if (!e.target.classList.contains("dontOpen") && !e.target.parentElement.parentElement.classList.contains("dontOpen")) {
        window.open(url);
    }
    return false;
}

$('body').on('click', '.bottom_schedule_modal_btn', function () {    
    $('.bottom_back_drop').addClass('active');
    $('.bottom_schedule_modal').addClass('active');
});

$('body').on('click', '.bottom_close_btn', function () {
    $('.bottom_back_drop').removeClass('active');
    $('.bottom_custom_modal').removeClass('active');
});

$('body').on('click', '#custom_date_time_btn', function () {
    $(this).hide();
    $('#custom_date_time').show();
});

$('#dvScheduleVisit').on('click', '.schedule_visit_close', function () {
    $('#dvScheduleVisit').html('');
});

function getScheduleVisit(propertyId,btnId)
{
    if(propertyId!=="" && btnId!=="")
    {       
        $('.loader').show();
        $.get("/Property/getScheduleVisit", function (data) {
            $('#dvScheduleVisit').append(data);
            $('#scheduleVisitPropertyId').val(propertyId);
            $('#selectedScheduleVisitBtnId').val(btnId);
            $('.loader').hide();
        });
    }
}

function isVisitorExist() {
    var err = '';
    if (err = validate("Mobile no", $('#scheduleMobile').val(), ['mobile'])) return showError(err);    
    $('.loader').show();
    $.post("/user/isMobileExist", { mobile: $('#scheduleMobile').val() }, function (data) {
        if (data.Success == true)
        { $('#scheduleUser').val(data.User); $('#scheduleCheckMobile').hide(); $('#scheduleVerifyUser').show(); }
        else
        { $('#scheduleUser').val(data.User); $('#scheduleCheckMobile').hide(); $('#scheduleNewUser').show(); }
        $('.loader').hide();
    });
}

function scheduleVisitVerify() {
    var user = $('#scheduleUser').val(),propertyId = $('#scheduleVisitPropertyId').val(),otp = $('#scheduleOTP').val(),visitDate = $('.custom_date').data('value'),visitTime = $('.custom_time').data('value'),err = '';    
    if (err = validate("OTP",otp, ['otp'])) return showError(err);
    if (err = validate("Visit Date",visitDate,['req'])) return showError(err);
    if (err = validate("Visit Time", visitTime, ['req'])) return showError(err);    
    $('.loader').show();
    $.post("/property/VerifyScheduleVisit", { OTP: otp, UserId: user, PropertyId: propertyId, VisitDate: visitDate, VisitTime: visitTime }, function (data) {
        if (data.Success == true) {
            $('#scheduleSuccessBox .smg').html('');
            $('#scheduleSuccessBox .smg').html(data.Message);
            $('#scheduleSuccessBox').show(); $('#scheduleVerifyUser').hide();            
            $('.schedule_visit_close').attr('onClick', 'location.reload();');
        }
        else
            showError(data.ErrorMessage);
        $('.loader').hide();
    });
}

function scheduleVisit() {
    var propertyId = $('#scheduleVisitPropertyId').val(),visitDate = $('.custom_date').data('value'),visitTime = $('.custom_time').data('value'),err='';
    if (err = validate("Visit Date", visitDate, ['req'])) return showError(err);
    if (err = validate("Visit Time", visitTime, ['req'])) return showError(err);
    $('.loader').show();
    $.post("/property/ScheduleVisit", { PropertyId: propertyId, VisitDate: visitDate, VisitTime: visitTime }, function (data) {
        if (data.Success == true) {            
            $('#scheduleSuccessBox .smg').html('');
            $('#scheduleSuccessBox .smg').html(data.Message);
            $('#scheduleSuccessBox').show(); $('#scheduleCheckMobile').hide();            
            var selectedBtn = $('#btnScheduleVisit_' + $('#selectedScheduleVisitBtnId').val());
            $(selectedBtn).removeClass('btn-primary-border').text('Scheduled').attr('disabled','disabled');
        }
        else
            showError(data.ErrorMessage);
        $('.loader').hide();
    });
}

function newUserRegistration() {
    var name = $('#scheduleName').val().trim(),email = $('#scheduleEmail').val().trim(),mobile = $('#scheduleMobile').val().trim(),err='';    
    if (err = validate("Name",name, ['req', 'alpha', { 'maxLen': { 'l': '50' } }])) return showError(err);    
    if (err = validate("Mobile", mobile, ['mobile'])) return showError(err);
    if (err = validate("Email",email, ['req','email'])) return showError(err);    
    $('.loader').show();
    $.post("/user/NewUserRegistrationAjax", { Name: name, Mobile: mobile, Email: email }, function (data) {
        if (data.Success == true) {
            $('#scheduleVerifyUser').show(); $('#scheduleNewUser').hide(); $('#scheduleUser').val(data.User);
        }
        else
            showError(data.ErrorMessage);
        $('.loader').hide();
    });
}

function newUserRegistrationContact() {
    var name = $('#contactName').val().trim(), email = $('#contactEmail').val().trim(), mobile = $('#contactMobile').val().trim(), err ='';
    if (err = validate("Name", name, ['req', 'alpha', { 'maxLen': { 'l': '50' } }])) return showError(err);
    if (err = validate("Mobile", mobile, ['mobile'])) return showError(err);
    if (err = validate("Email", email, ['req', 'email'])) return showError(err);

    $('.loader').show();
    $.post("/user/NewUserRegistrationAjax", { Name: name, Mobile: mobile, Email: email }, function (data) {
        if (data.Success == true) {
            $('#contactVerifyUser').show(); $('#contactNewUser').hide(); $('#contactUser').val(data.User);
        }
        else
            showError(data.ErrorMessage);
        $('.loader').hide();
    });
}

function getContactDetail(propertyId, btnId)
{
    debugger;
    if (propertyId != "" && btnId != "") {        
        $('.loader').show();
        $.post("/Property/GetEnquiryDetail", { propertyId: propertyId }, function (data) {
            if (data.Success == false)
                showError(data.ErrorMessage);
            else {
                $('#dvScheduleVisit').append(data);
                $('#contactPropertyId').val(propertyId);
                $('#selectedContactBtnId').val(btnId);
                if ($('#isUserLogin').val() == "1")
                {
                    var selectedBtn = $('#btnEnquiry_' + btnId);
                    $(selectedBtn).removeClass('btn-primary-border').text('Contacted').attr('disabled','disabled');
                }
            }
            $('.loader').hide();
        });
    }
}

function isVisitorExistForContact() {
    var mobile = $('#contactMobile').val(),err='';
    if (err = validate("Mobile", mobile, ['mobile'])) return showError(err);
    $('.loader').show();
    $.post("/user/isMobileExist", { mobile: mobile }, function (data) {
        if (data.Success == true)
        { $('#contactUser').val(data.User); $('#contactCheckMobile').hide(); $('#contactVerifyUser').show(); }
        else
        { $('#contactUser').val(data.User); $('#contactNewUser').show(); $('#contactCheckMobile').hide(); }
        $('.loader').hide();
    });
}

function contactVerifyUser() {
    var user = $('#contactUser').val(),propertyId = $('#contactPropertyId').val(),otp = $('#contactOTP').val(),err='';
    if (err = validate("OTP",otp, ['otp'])) return showError(err);    
    $('.loader').show();
    $.post("/property/VerifyEnquiryDetail", { OTP: otp, UserId: user, PropertyId: propertyId }, function (data) {
        if (data.Success == true) { 
            $('#contactSuccessBox .smg').html('');
            $('#contactSuccessBox .smg').html(data.Message);
            $('#contactSuccessBox').show(); $('#contactVerifyUser').hide();            
            $('#modalPopupContactDetail').find('.bottom_close_btn').attr('onClick', 'location.reload();');
        }
        else
            showError(data.ErrorMessage);
        $('.loader').hide();
    });
}

$('body').on('keypress paste', '#scheduleName,#contactName', function (e) {
    return isAlpha(e);
});

$('body').on('keypress paste', '#scheduleMobile,#contactMobile', function (e) {
    return isInt(e);
});

$('#dvLoadProperty').on('click', '.fav-icon', function () {
    $(this).attr('data-prefix', function (i, v) {
        var count = $('.wishlist_count').data('count');
        if (v == 'far') {
            console.log("sdd"+count);
            $(this).attr('data-prefix', 'fas');
            $('.wishlist_count').data('count', count + 1);
            $('.wishlist_count sup').text(count + 1);
            addToWishList($(this).data('pid'));
        }
        else {
            $(this).attr('data-prefix', 'far');
            $('.wishlist_count').data('count', count - 1);
            $('.wishlist_count sup').text(count - 1);
            removeToWishList($(this).data('pid'));
        }
    });
});

function addToWishList(value) {
    if (value) {
        var existingValue = readCookie('wishlist');
        if (existingValue == null || existingValue.trim() == "")
            setCookie(value);
        else {
            var oldValue = existingValue.split('|');
            for (var i = 0; i < oldValue.length; i++) {
                if (value == oldValue[i]) {                    
                    return;
                }
            }
            setCookie(existingValue+"|"+value);
        }       
    }    
}

function removeToWishList(value)
{   
    var existingValue = readCookie('wishlist');
    if(existingValue!=null || existingValue.trim()!="")
    {
        var oldValue = existingValue.split('|');
        for (var i = 0; i < oldValue.length; i++) {
            if (value == oldValue[i]) {
                oldValue.splice(i, 1);
            }
        }        
        if (oldValue.length > 0)
            setCookie(oldValue.join('|'));
        else
            expireCookie();
    }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function setCookie(value)
{
    var days = 10;
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    var expires = "; expires=" + date.toGMTString();    
        document.cookie = "wishlist=" + value + expires + "; path=/";
}

function expireCookie() {
    var days = -1;
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    var expires = "; expires=" + date.toGMTString();
    document.cookie = "wishlist=" + '' + expires + "; path=/";
}
function urlString(i)
{
   return i.replace(' ', '-').replace('/', '-').replace(',', '-');    
}

$('#btnQuickEnquiry').click(function () {
    var err = '';
    if (err = validate("Name", $('#qeUserName').val(), ['req', 'alpha', { 'maxLen': { 'l': '50' } }])) return showError(err);
    if (err = validate("Mobile no", $('#qeUserMobile').val(), ['mobile'])) return showError(err);
    if (err = validate("Email", $('#qeUserEmail').val(), ['req', 'email', { 'maxLen': { 'l': '500' } }])) return showError(err);
    if (err = validate("Description", $('#qeDescription').val(), [{ 'maxLen': { 'l': '500' } }])) return showError(err);

    loading.show('#btnQuickEnquiry');
    $.post("/home/quickenquiry", { UserName: $('#qeUserName').val(), UserMobile: $('#qeUserMobile').val(), UserEmail: $('#qeUserEmail').val(), Description: $('#qeDescription').val() }, function (data, status) {
        data.Success ? (showSuccessMessage('You query has been submitted.Our Team will contact you soon'), $('#qeUserName').val(''), $('#qeUserMobile').val(''), $('#qeUserEmail').val(''), $('#qeDescription').val('')) : showError(data.ErrorMessage);
    }).always(function () {
        loading.hide('#btnQuickEnquiry', 'Submit');
    });
});