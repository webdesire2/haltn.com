$(document).ready(function () {
    $('.search-box .back-btn').click(function () {
        $('.search-box').removeClass('active');
        $('html, body').removeClass('overflow_hidden');
    });

            $('.menuBtn').click(function () {
                $('.customMenuRight').addClass('customMenuRightActive');
                $('.backDropBg').addClass('backDropShow');
                $('body').css('overflow','hidden');
            });
            $('.customMenuClose').click(function () {
                $('.customMenuRight').removeClass('customMenuRightActive');
                $('.backDropBg').removeClass('backDropShow');
                $('body').css('overflow','auto');
            });

            $('.backDropBg').click(function () {
                $('.customMenuRight').removeClass('customMenuRightActive');
                $('.backDropBg').removeClass('backDropShow');
                $('body').css('overflow','auto');
            });
            $('.menuBtn, .customMenuRight ').click(function (e) {
                e.stopPropagation();
            })

            var flag = false;
            $('body').on('click', '.mCirclePlus', function (e) {
                e.preventDefault();
                if (!flag) {
                    flag = true;
                    $('.mCircleMore li').slideToggle(function () {
                        flag = false;
                    });
                    $('.mCirclePlus li').toggleClass('mCircleActive');

                }
            })

});


