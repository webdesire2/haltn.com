﻿using FH.App.Entity;
using FH.Web.Security;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Security;

namespace FH.Web.Security
{
    public class AuthCookie
    {
        public static void AddAuthCookie(UserDetail userDetail,HttpRequestBase request,HttpResponseBase response)
        {
            userDetail.UserId = EncreptDecrpt.Encrypt(userDetail.UserId);
            string userData = JsonConvert.SerializeObject(userDetail);
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                1,
                userDetail.Mobile,
                DateTime.Now,
                DateTime.Now.AddDays(30),
                false,
                userData);

            string encTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            if(!request.IsLocal)
                faCookie.Domain = ".haltn.com";
            faCookie.Expires = DateTime.Now.AddDays(30);
            response.Cookies.Add(faCookie);
            //formsAuthentication.SetAuthCookie(userDetail);
        }

    }
}