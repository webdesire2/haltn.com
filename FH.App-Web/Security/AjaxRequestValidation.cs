﻿using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace FH.Web.Security
{
    public class AjaxRequestValidation:ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            if (request.IsAjaxRequest())
            {
                string cookieToken = "";
                string formToken = "";
                string tokenHeaders="";
                try
                {
                    tokenHeaders = request.Headers.Get("RequestVerificationToken");                   
                        string[] tokens = tokenHeaders.Split(':');
                        if (tokens.Length == 2)
                        {
                            cookieToken = tokens[0].Trim();
                            formToken = tokens[1].Trim();
                        }

                        AntiForgery.Validate(cookieToken, formToken);                   
                }
                catch
                {
                    filterContext.HttpContext.Response.StatusCode= (int)HttpStatusCode.NotAcceptable;                    
                    filterContext.Result = new JsonResult
                    {
                        Data = new { Error = "Unauthorized request." },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
            }           
        }
    }
}