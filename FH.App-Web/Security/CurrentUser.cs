﻿

using System.Web;

namespace FH.Web.Security
{
    public class CurrentUser
    {
        public static CustomPrincipal User
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }

    }
}