﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FH.Web.Security
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        protected virtual CustomPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            string nonAuthorizeAction ="postyourproperty";
            var rd = filterContext.RequestContext.RouteData;
            if (rd.GetRequiredString("action").ToLower() == nonAuthorizeAction)
                return;
            
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                string Roles =(CurrentUser !=null && CurrentUser.Roles.Length>0?CurrentUser.Roles[0]:"");
                if (!String.IsNullOrEmpty(Roles))
                {
                    if (!CurrentUser.IsInRole(Roles))
                    {
                        filterContext.Result = new ViewResult()
                        {
                            ViewName = "_UnAuthorize"
                        };
                    }
                }

                if (!String.IsNullOrEmpty(Users))
                {
                    if (!Users.Contains(CurrentUser.UserId.ToString()))
                    {                        
                        filterContext.Result = new RedirectToRouteResult(new
                        RouteValueDictionary(new { controller = "Home", action = "Index" }));

                        // base.OnAuthorization(filterContext); //returns to login url
                    }
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new
                      RouteValueDictionary(new { controller = "User", action = "Login" }));
            }

        }
    }
}