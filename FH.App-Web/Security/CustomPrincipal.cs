﻿using System.Linq;
using System.Security.Principal;
using System.Collections.Generic;

namespace FH.Web.Security
{
    public class CustomPrincipal : IPrincipal
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string UserType { get; set; }
        public string[] Roles { get; set; }

        public IIdentity Identity { get; private set; }

        public CustomPrincipal(string userName)
        {
            this.Identity = new GenericIdentity(userName);
        }

        public bool IsInRole(string role)
        {
            string[] roles = {"Agent","Owner","Buyer", "Receptionist" };
            if (roles.Any(r => role.Contains(r)))
            {
                return true; 
            }
            else
            {
                return false;
            }
        }
    }
    
}