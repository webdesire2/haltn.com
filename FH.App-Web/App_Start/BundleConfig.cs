﻿using System.Web.Optimization;
namespace FH.App_Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            StyleBundle styleBndl = new StyleBundle("~/bundle/style");
            styleBndl.Include(   
                "~/Content/css/bootstrap.min.css",
                               "~/Content/css/d.custom.css",
                                "~/Content/css/responsive.css"                                                           
                );

            StyleBundle styleMobile = new StyleBundle("~/bundle/mstyle");
            styleMobile.Include(
                "~/Content/css/bootstrap.min.css",
                               "~/Content/css/m.custom.css",
                                "~/Content/css/responsive.css"
                );

            ScriptBundle scriptBndl = new ScriptBundle("~/bundle/js");
            scriptBndl.Include(
                                 "~/Content/js/jquery-2.1.1.min.js"
                              );

            ScriptBundle scriptComm = new ScriptBundle("~/bundle/jsproj");
            scriptComm.Include("~/Content/js/popper.min.js",
                                 "~/Content/js/bootstrap-4.3.1.min.js",
                                 "~/Content/js/fontAweome.js",
                "~/Content/js/common.js",
                                 "~/Content/js/search.js");

            ScriptBundle scriptMobile = new ScriptBundle("~/bundle/mjsproj");
            scriptMobile.Include("~/Content/js/popper.min.js",
                                  "~/Content/js/bootstrap-4.3.1.min.js",
                                  "~/Content/js/fontAweome.js",
                 "~/Content/js/common.js",
                                  "~/Content/js/m.search.js"   ,
                                  "~/Content/js/m.custom.js"
                                  );

            bundles.Add(styleBndl);
            bundles.Add(scriptBndl);
            bundles.Add(scriptComm);
            bundles.Add(styleMobile);
            bundles.Add(scriptMobile);
            BundleTable.EnableOptimizations = true;
            
        }
    }
}