﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FH.App_Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute(
               name: "Search",
               url: "search/{ptype}/{name}",
               defaults: new { controller = "Property", action = "PSearch"}
           );

            routes.MapRoute(
                name: "Details",
                url: "{controller}/{action}/{pid}/{name}",
                defaults: new { controller = "Property", action = "Details",name=UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "sitemap",
                url: "sitemap.xml",
                 defaults: new { controller = "Home", action = "Sitemap" }
                );

            routes.MapRoute(
                name: "about",
                url: "about",
                defaults: new { controller = "Home", action = "aboutus" }
            );

            routes.MapRoute(
                name: "contact",
                url: "contact-us",
                defaults: new { controller = "Home", action = "contactus" }
            );

            routes.MapRoute(
                name: "team",
                url: "team",
                defaults: new { controller = "Home", action = "Team" }
            );
            routes.MapRoute(
                name: "term-condition",
                url: "term-and-condition",
                defaults: new { controller = "Home", action = "TermAndCondition" }
            );
            routes.MapRoute(
                name: "privacy",
                url: "privacy",
                defaults: new { controller = "Home", action = "privacy" }
            );

            routes.MapRoute(
                name: "faq",
                url: "faq",
                defaults: new { controller = "Home", action = "Faq" }
            );

            routes.MapRoute(
               name: "listing-policy",
               url: "listing-policy",
               defaults: new { controller = "Home", action = "ListingPolicy" }
           );

            routes.MapRoute(
               name: "buyer-manual",
               url: "buyer-manual",
               defaults: new { controller = "Home", action = "BuyerManual" }
           );

            routes.MapRoute(
               name: "seller-manual",
               url: "seller-manual",
               defaults: new { controller = "Home", action = "SellerManual" }
           );

            routes.MapRoute(
                name: "Home",
                url: "{ptype}",
                defaults: new { controller = "Home", action = "Index", ptype = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Payment",
               url: "{controller}/{action}/{pid}",
               defaults: new { controller = "Payment", action = "Response", pid = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "User", 
               url: "user/pbl/{pid}", 
               defaults: new { controller = "User", pid = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{ptype}",
                defaults: new { controller = "Home", action = "Index", ptype = UrlParameter.Optional }
            );
        }
    }
}
