﻿using System;
using System.IO;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
//using FH.Logger;

namespace FH.App_Web
{
    public class BaseController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            string strController = filterContext.RouteData.Values["controller"].ToString();
            string strAction = filterContext.RouteData.Values["action"].ToString();
            Exception e = filterContext.Exception;
           // Log.Write(strController, strAction, "", e.Message, "User",true);

            filterContext.ExceptionHandled = true;

            if (Request.IsAjaxRequest())
            {
                filterContext.Result = new JsonResult
                {
                    Data = new { Success = false, ErrorMessage = "Something Went Wrong,Please Try Again." },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {               
                filterContext.Result = new ViewResult()
                {
                    ViewName = "Error"
                };
            }
        }

        public string RenderPartialToString(PartialViewResult partialViewResult)
        {
            var httpContext = HttpContext;

            if (httpContext == null)
            {
                throw new NotSupportedException("An HTTP context is required to render the partial view to a string");
            }

            var controllerName = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();

            var controller = (ControllerBase)ControllerBuilder.Current.GetControllerFactory().CreateController(httpContext.Request.RequestContext, controllerName);

            var controllerContext = new ControllerContext(httpContext.Request.RequestContext, controller);

            var view = ViewEngines.Engines.FindPartialView(controllerContext, partialViewResult.ViewName).View;

            var sb = new StringBuilder();

            using (var sw = new StringWriter(sb))
            {
                using (var tw = new HtmlTextWriter(sw))
                {
                    view.Render(new ViewContext(controllerContext, view, partialViewResult.ViewData, partialViewResult.TempData, tw), tw);
                }
            }

            return sb.ToString();
        }
    }
}
