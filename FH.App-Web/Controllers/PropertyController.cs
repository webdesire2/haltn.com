﻿using System.Web.Mvc;
using FH.App.Entity;
using FH.App.Service;
using FH.Util;
using System.Configuration;
using System;
using System.Linq;
using System.Collections.Generic;
using FH.Validator;
using System.Globalization;
using FH.Web.Security;
using System.Web;
using FH.App_Entity;

namespace FH.App_Web.Controllers
{ 
    public class PropertyController : BaseController
    {

        #region Search

        public ActionResult property()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Search(PropertySearchParameter input)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(input.PropertyType))
                { return PartialView("_Error"); }
                //if (string.IsNullOrWhiteSpace(input.Locality) || (input.Locality.Trim().Equals("Gurgaon, Haryana", StringComparison.OrdinalIgnoreCase) && string.IsNullOrEmpty(input.SearchText)))
                //{
                //    return Content("Please enter a valid location.");
                //}
                var vmProperty = GetSearchProperty(input);
                vmProperty.Input.MetaTitle = string.Concat(input.PropertyType, " near ", input.Locality);
                vmProperty.Input.MetaKeyword = string.Concat(input.PropertyType, " near ", input.Locality);
                vmProperty.Input.MetaDesc = string.Concat(input.PropertyType, " near ", input.Locality);
                
                return View(vmProperty);
            }
            catch
            {
                return PartialView("_Error");
            }
        }

        [HttpGet]
        public ActionResult PSearch(string ptype, string name)
        {
            if (string.IsNullOrWhiteSpace(ptype))
            { return PartialView("_Error"); }
            else if (ptype.ToUpper() != EnPropertyType.Flat.ToString().ToUpper() && ptype.ToUpper() != EnPropertyType.Flatmate.ToString().ToUpper() && ptype.ToUpper() != EnPropertyType.Room.ToString().ToUpper() && ptype.ToUpper() != EnPropertyType.PG.ToString().ToUpper())
            { return PartialView("_Error"); }

            if (string.IsNullOrWhiteSpace(name))
            { return PartialView("_Error"); }

            PropertySearchParameter input = new PropertySearchParameter();
            input.PageIndex = 1;
            input.PropertyType = string.IsNullOrWhiteSpace(ptype) ? "" : ptype.ToUpper();
            input.SearchText = name;            
            input.RoomType = "";
            input.TenantGender = "";


            var vmProperty = GetSearchProperty(input);
            IEnumerable<SEOCategory> simCategory;
            dynamic metaTag;
            using (var uow = new UnitOfWork()) 
            {
                metaTag = uow.PropertyBL.GetPropertyListMetaTag(name);
                simCategory = uow.PropertyBL.GetSimilarCategories(name);
            }
            vmProperty.simCategory = simCategory; 

             
            if (metaTag != null)
            {
               vmProperty.Input.MetaTitle = metaTag.MetaTitle;
                vmProperty.Input.MetaKeyword = metaTag.MetaKeyword;
                vmProperty.Input.MetaDesc = metaTag.MetaDesc;
                vmProperty.Input.MetaJson = metaTag.MetaJson;
                vmProperty.Input.Heading = metaTag.Heading;
                vmProperty.Input.HeadingContent = metaTag.HeadingContent;
            }
            return View("Search", vmProperty);
        }

        [HttpGet]
        public ActionResult GetAllCategoriesMeta()
        {
            IEnumerable<PGRoomMeta> allCategoriesMetaList;
            using (UnitOfWork uow = new UnitOfWork())
            {

                allCategoriesMetaList = uow.PropertyBL.GetPGRoomMetaDetails();
            }

            return Json(new {Data = allCategoriesMetaList },JsonRequestBehavior.AllowGet);

        }

            private VMPropertyListing GetSearchProperty(PropertySearchParameter input)
        {
            int pageSize = Convert.ToInt16(ConfigurationManager.AppSettings["PropertySearchPageSize"]);
            int totalRecord;
            
            VMPropertyListing vmProperty = new VMPropertyListing();
            PropertySearchParameter entity = new PropertySearchParameter();

            if (!string.IsNullOrWhiteSpace(input.PropertyType) && (input.PropertyType.ToUpper() == "FLAT" || input.PropertyType.ToUpper() == "FLATMATE"))
            {
                entity.PropertyType = input.PropertyType.ToUpper();
                entity.Apartment = findValueByMaster(ApartmentType.GetAll(), input.Apartment);
                entity.Bhk = findValueByMaster(BHKType.GetAll(), input.Bhk);
                entity.FurnishingType = findValueByMaster(FurnishingType.GetAll(), input.FurnishingType);
                entity.PreferredTenant = findValueByMaster(TenantType.GetAll(), input.PreferredTenant);
                entity.Facing = findValueByMaster(FacingType.GetAll(), input.Facing);               
                entity.RoomType = findValueByMaster(RoomType.GetAll(), input.RoomType);
            }
            else if (!string.IsNullOrWhiteSpace(input.PropertyType) && input.PropertyType.ToUpper() == "ROOM")
            {  
                entity.PropertyType = "ROOM";
                entity.Apartment = findValueByMaster(ApartmentType.GetAll(), input.Apartment);
                entity.RoomType = findValueByMaster(RoomType.GetAll(), input.RoomType);
                entity.PreferredTenant = findValueByMaster(TenantType.GetAll(), input.PreferredTenant);
            }
            else if (!string.IsNullOrWhiteSpace(input.PropertyType) && input.PropertyType.ToUpper() == "PG")
            {
                entity.PropertyType = "PG";
                entity.RoomType = findValueByMaster(RoomType.GetAll(), input.RoomType);
                entity.RoomType = string.IsNullOrWhiteSpace(entity.RoomType) ? "" : entity.RoomType == "Private Room" ? "Single,Single & Sharing" : entity.RoomType == "Shared Room" ? "Single & Sharing,Sharing" : "Single,Single & Sharing,Sharing";
                entity.TenantGender = (!string.IsNullOrWhiteSpace(input.TenantGender) ? input.TenantGender.Replace('_', ',') : "");
            }

            entity.Locality =string.IsNullOrEmpty(input.SearchText)? input.Locality:input.SearchText;
            entity.Latitude = input.Latitude == null ? "" : input.Latitude;
            entity.Longitude = input.Longitude == null ? "" : input.Longitude;
            entity.MinPrice = input.MinPrice;
            entity.MaxPrice = input.MaxPrice == 0 ? 200000 : input.MaxPrice;
            entity.City = input.City;
            entity.PageIndex = input.PageIndex == 0 ? 1 : input.PageIndex;
            entity.PageSize = pageSize;
            
            using (UnitOfWork uow = new UnitOfWork())
            {

                vmProperty.PropertyList = uow.PropertyBL.GetAllProperty(entity, out totalRecord);
                vmProperty.Paging = new Pager(totalRecord, input.PageIndex == 0 ? 1 : input.PageIndex, pageSize);
                vmProperty.Locality = input.Locality;
            }
            
            vmProperty.Input = entity;
            vmProperty.Input.RoomType = input.RoomType==null?"":input.RoomType;
            vmProperty.Input.Locality = string.IsNullOrEmpty(input.SearchText) ? input.Locality :"";
            vmProperty.Input.SearchText = input.SearchText;
            return vmProperty;
        }
        #endregion


        //[HttpGet]
        //public ActionResult Search(string pType,string city, string locality, string lat, string lng, string bhkType, string furnishingType,string preferredTenant,string apartment,string facing ,string roomType, string gender,int minPrice=0,int maxPrice=200000,int page = 1)
        //{          

        //    if (string.IsNullOrEmpty(lat) || string.IsNullOrEmpty(lng))
        //    { return PartialView("_Error"); }

        //    if(string.IsNullOrWhiteSpace(pType))
        //    { return PartialView("_Error"); }
        //    else if(pType.ToUpper() != EnPropertyType.Flat.ToString().ToUpper() && pType.ToUpper() !=EnPropertyType.Flatmate.ToString().ToUpper() && pType.ToUpper() !=EnPropertyType.Room.ToString().ToUpper() && pType.ToUpper() !=EnPropertyType.PG.ToString().ToUpper())
        //    { return PartialView("_Error"); }

        //    PropertySearchParameter input = new PropertySearchParameter();
        //    input.PageIndex=page;
        //    input.PropertyType = string.IsNullOrWhiteSpace(pType)?"":pType.ToUpper();
        //    input.City = string.IsNullOrWhiteSpace(city)?"":city;
        //    input.Locality = locality;
        //    input.Latitude = lat;
        //    input.Longitude = lng;
        //    input.Apartment = string.IsNullOrWhiteSpace(apartment) ? "" : apartment;
        //    input.Bhk =string.IsNullOrWhiteSpace(bhkType)?"":bhkType;
        //    input.FurnishingType = string.IsNullOrWhiteSpace(furnishingType) ? "" : furnishingType;
        //    input.PreferredTenant= string.IsNullOrWhiteSpace(preferredTenant) ? "" : preferredTenant;
        //    input.RoomType = string.IsNullOrWhiteSpace(roomType) ? "" : roomType;
        //    input.TenantGender = string.IsNullOrWhiteSpace(gender) ? "" : gender;
        //    input.MinPrice = minPrice;
        //    input.MaxPrice = maxPrice;
        //    input.Facing = string.IsNullOrWhiteSpace(facing)?"":facing;

        //    input.MetaTitle = $"{pType.ToUpper()} in {locality} | {WebConfigHelper.WebsiteName}";
        //    input.MetaKeyword= $"{pType.ToUpper()} in {locality}";
        //    input.MetaDesc= $"{pType.ToUpper()} in {locality}";

        //    return View(input);
        //}

        //[HttpPost]
        //public PartialViewResult SearchItem(PropertySearchParameter input)
        //{
        //    int pageSize =Convert.ToInt16(ConfigurationManager.AppSettings["PropertySearchPageSize"]);                        
        //    int totalRecord;

        //    VMPropertyListing vmProperty = new VMPropertyListing();
        //    PropertySearchParameter entity = new PropertySearchParameter();

        //    if(!string.IsNullOrWhiteSpace(input.PropertyType) && (input.PropertyType.ToUpper()=="FLAT" || input.PropertyType.ToUpper() == "FLATMATE"))
        //    {
        //        entity.PropertyType =input.PropertyType.ToUpper() ;
        //        entity.Apartment= findValueByMaster(ApartmentType.GetAll(), input.Apartment);
        //        entity.Bhk = findValueByMaster(BHKType.GetAll(), input.Bhk);
        //        entity.FurnishingType= findValueByMaster(FurnishingType.GetAll(), input.FurnishingType);
        //        entity.PreferredTenant= findValueByMaster(TenantType.GetAll(), input.PreferredTenant);
        //        entity.Facing = findValueByMaster(FacingType.GetAll(), input.Facing);
        //        if(input.PropertyType.ToUpper()=="FLATMATE")
        //        entity.RoomType = findValueByMaster(RoomType.GetAll(), input.RoomType);
        //    }
        //    else if (!string.IsNullOrWhiteSpace(input.PropertyType) && input.PropertyType.ToUpper() == "ROOM")
        //    {
        //        entity.PropertyType = "ROOM";
        //        entity.Apartment = findValueByMaster(ApartmentType.GetAll(), input.Apartment);
        //        entity.RoomType = findValueByMaster(RoomType.GetAll(), input.RoomType);
        //        entity.PreferredTenant= findValueByMaster(TenantType.GetAll(), input.PreferredTenant);
        //    }
        //    else if (!string.IsNullOrWhiteSpace(input.PropertyType) && input.PropertyType.ToUpper() == "PG")
        //    {
        //        entity.PropertyType = "PG";
        //        entity.RoomType = findValueByMaster(RoomType.GetAll(), input.RoomType);
        //        entity.RoomType=string.IsNullOrWhiteSpace(entity.RoomType)?"":entity.RoomType == "Private Room" ? "Single,Single & Sharing" : entity.RoomType=="Shared Room"? "Single & Sharing,Sharing":"Single,Single & Sharing,Sharing";
        //        entity.TenantGender =(!string.IsNullOrWhiteSpace(input.TenantGender)?input.TenantGender.Replace('_',','):"");
        //    }

        //    entity.Locality = input.Locality;
        //    entity.Latitude = input.Latitude==null?"":input.Latitude ;
        //    entity.Longitude = input.Longitude==null?"":input.Longitude;
        //    entity.MinPrice = input.MinPrice;
        //    entity.MaxPrice = input.MaxPrice;
        //    entity.City = input.City;
        //    entity.PageIndex = input.PageIndex==0?1:input.PageIndex;
        //    entity.PageSize = pageSize;


        //    using (UnitOfWork uow = new UnitOfWork())
        //    {

        //        vmProperty.PropertyList = uow.PropertyBL.GetAllProperty(entity, out totalRecord);
        //        vmProperty.Paging = new Pager(totalRecord, input.PageIndex == 0 ? 1 : input.PageIndex, pageSize);
        //        vmProperty.Locality = input.Locality;
        //    }

        //    if(Request.Browser.IsMobileDevice)
        //        return PartialView("_Search_List_Mobile", vmProperty);
        //    else
        //        return PartialView("_Property_List_Desktop", vmProperty);           
        //}

        [HttpGet]
        [AjaxRequestValidation]
        public ActionResult GetScheduleVisit()
        {
            if(Request.Browser.IsMobileDevice)
            return PartialView("_Schedule_Visit_Mobile");
            else
                return PartialView("_Schedule_Visit_Desktop");
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult GetEnquiryDetail(LeadDetails entity)
        {
            bool isMobile = Request.Browser.IsMobileDevice;
            int dPropertyId = Validation.DecryptToInt(entity.PropertyId);
            int dUserId = (CurrentUser.User != null ? Validation.DecryptToInt(CurrentUser.User.UserId) : 0);
            if (dPropertyId == 0)
                return ValidationError("Invalid input.");

            string propertyIdForCookies = entity.PropertyId.Replace('=', '_');
            if (Request.Cookies["Enquiry_" + propertyIdForCookies] != null)
                return ValidationError("You have already Contacted.");

            if (dUserId > 0)
            {
                using (var uow = new UnitOfWork())
                {
                    entity.PropertyId = dPropertyId.ToString();
                    entity.UserId = dUserId.ToString();
                    LeadDetails detail = uow.PropertyBL.EnquiryDetail(entity);

                    if (detail != null && !string.IsNullOrEmpty(detail.Message))
                        return ValidationError(detail.Message);
                    else if (detail != null && !string.IsNullOrEmpty(detail.PropertyDetail))
                    {
                        uow.Commit();
                        var visitScheduled = new HttpCookie("Enquiry_" + propertyIdForCookies);
                        visitScheduled.Expires = DateTime.Now.AddHours(24);
                        Response.Cookies.Add(visitScheduled);

                        if (!string.IsNullOrEmpty(detail.PropertyUserType))
                        {
                            // var userType = (detail.PropertyUserType.ToUpper()=="OWNER"?"Owner":"Agent");

                            //string visitorMsg = "Hi " + detail.UserName + " ,%0a You have showing interest in " + detail.PropertyId + ", " + detail.PropertyDetail + " . Contact to " + detail.PropertyUserName + " " + detail.PropertyUserMobile + ". %0aRegards TELONESTAY ";

                            //string agentMsg = detail.UserName + " showing interest in PropertyID (" + dPropertyId + " " + detail.PropertyDetail + ",,,,,,).Contact on " + detail.UserMobile + "%0a Regards TELONESTAY ";

                            string visitorMsg = "Hello! you are showing interest in Property " + detail.PropertyId + " on Haltn. Please contact to Property Owner " + detail.PropertyUserName + ", mobileNo. " + detail.PropertyUserMobile + ". Thank you!";
                            string agentMsg = "Hello! A user showing interest in property " + detail.PropertyId + " on Haltn. Please contact to User " + detail.UserName + ", MobileNo " + detail.UserMobile + ". Thank you!";
                            string OWNER = System.Configuration.ConfigurationManager.AppSettings["OWNER"].ToString();
                            string USERid = System.Configuration.ConfigurationManager.AppSettings["USERid"].ToString();

                            SendSMS.Send(detail.UserMobile, visitorMsg, USERid);
                            SendSMS.Send(detail.PropertyUserMobile, agentMsg, OWNER);
                           

                            if (isMobile)
                            { return PartialView("_Enquiry_Mobile",detail); }
                            else
                            { return PartialView("_Enquiry",detail); }                           
                        }                        
                    }
                }
            }

            if(isMobile)
                return PartialView("_Enquiry_Mobile", new LeadDetails());
            else
                return PartialView("_Enquiry",new LeadDetails());
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult VerifyScheduleVisit(LeadDetails entity)
        {
            int dPropertyId = Validation.DecryptToInt(entity.PropertyId);
            int dUserId= Validation.DecryptToInt(entity.UserId);
            if (dPropertyId == 0)
                return ValidationError("Invalid input.");
            if (dUserId == 0)
                return ValidationError("Invalid input.");

            if (entity.OTP <1000 || entity.OTP>9999)
                return ValidationError("4 digit OTP required.");
            
            if (!string.IsNullOrEmpty(entity.VisitDate))
            {                
                DateTime outAvailableFrom;
                if (DateTime.TryParseExact(entity.VisitDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outAvailableFrom))
                {
                    entity.VisitDate = DateTime.ParseExact(entity.VisitDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return ValidationError("Incorrect visit date.");
            }
            else
                return ValidationError("Visit date required.");

            if (string.IsNullOrWhiteSpace(entity.VisitTime))
                return ValidationError("Visit time required.");
            else
            {
                if (ValidateTime(entity.VisitTime) == false)
                    return ValidationError("Incorrect visit time.");
            }

            string propertyIdForCookies =entity.PropertyId.Replace('=', '_');
            if (Request.Cookies["ScheduledVisit_" + propertyIdForCookies] != null)            
                return ValidationError("Your visit for this property already scheduled.");                            

            using (var uow = new UnitOfWork())
            {
                entity.PropertyId = dPropertyId.ToString();
                entity.UserId = dUserId.ToString();
                entity.VisitTime = entity.VisitTime.ToUpper();      
                entity.OTPExpiredOn = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                LeadDetails detail = uow.PropertyBL.ScheduleVisitVerify(entity);

                    if (detail != null && !string.IsNullOrEmpty(detail.Message))                    
                      return ValidationError(detail.Message);                    
                    else if (detail != null && !string.IsNullOrEmpty(detail.PropertyDetail))
                    {
                    UserDetail userDetail = uow.UserBL.GetUserByUserId(dUserId);
                    uow.Commit();                    
                    if (userDetail != null && !string.IsNullOrEmpty(userDetail.UserId))
                    {                        
                        AuthCookie.AddAuthCookie(userDetail, Request, Response);
                    }
                    var visitScheduled = new HttpCookie("ScheduledVisit_"+propertyIdForCookies);
                        visitScheduled.Expires = DateTime.Now.AddHours(24);
                        Response.Cookies.Add(visitScheduled);
                    var visitDate = !string.IsNullOrEmpty(detail.VisitDate) ? Convert.ToDateTime(detail.VisitDate).ToString("dd MMM yyyy") : detail.VisitDate;


                   // string visitorMsg = "Your visit for " + detail.PropertyDetail + " has been scheduled for " + visitDate + " " + detail.VisitTime + ". Contact to "+detail.PropertyUserName+" on "+detail.PropertyUserMobile;

                    string visitorMsg = "Your visit for " + detail.PropertyDetail + " has been scheduled for " + visitDate + " " + detail.VisitTime + ". Contact to " + detail.PropertyUserName + " on " + detail.PropertyUserMobile+".  Location:";
                    visitorMsg +=!string.IsNullOrEmpty(detail.LocationMapLink)?detail.LocationMapLink:"__";
                    visitorMsg += "%0aWith Regards, %0aTelonestay";
                    string agentMsg = detail.UserName + " scheduled visit for property id " + dPropertyId + " (" + detail.PropertyDetail + ") for " + visitDate + " " + detail.VisitTime + ". Contact on "+ detail.UserMobile + ". %0aWith Regards,  %0aTelonestay ";

                    string ShaduleVisitAgent = System.Configuration.ConfigurationManager.AppSettings["ShaduleVisitAgent"].ToString();
                    string ShaduleVisitUser = System.Configuration.ConfigurationManager.AppSettings["ShaduleVisitUser"].ToString();

                    SendSMS.Send(detail.UserMobile, visitorMsg, ShaduleVisitUser);
                    SendSMS.Send(detail.PropertyUserMobile, agentMsg, ShaduleVisitAgent);
                    string message = "Contact to " + detail.PropertyUserName + " on " + detail.PropertyUserMobile + ".";
                    return Json(new { Success = true,Message=message});
                }             

            }
            return Json(new { Sucess = false, ErrorMessage = "Something went wrong,Please try again." });
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult ScheduleVisit(LeadDetails entity)
        {
            int dPropertyId = Validation.DecryptToInt(entity.PropertyId);            
            if (dPropertyId == 0)
                return ValidationError("Invalid input.");
            
            if (!string.IsNullOrEmpty(entity.VisitDate))
            {
                DateTime outAvailableFrom;
                if (DateTime.TryParseExact(entity.VisitDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out outAvailableFrom))
                {
                    entity.VisitDate = DateTime.ParseExact(entity.VisitDate, "dd-MMM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                }
                else
                    return ValidationError("Incorrect visit date.");
            }
            else
                return ValidationError("Visit date required.");

            if (string.IsNullOrWhiteSpace(entity.VisitTime))
                return ValidationError("Visit time required.");
            else
            {
                if (ValidateTime(entity.VisitTime) == false)
                    return ValidationError("Incorrect visit time.");
            }

            string propertyIdForCookies = entity.PropertyId.Replace('=', '_');
            if (Request.Cookies["ScheduledVisit_" + propertyIdForCookies] != null)
                return ValidationError("Your visit for this property already scheduled.");


            using (var uow = new UnitOfWork())
            {
                entity.PropertyId = dPropertyId.ToString();
                entity.UserId = EncreptDecrpt.Decrypt(CurrentUser.User.UserId);
                entity.VisitTime = entity.VisitTime.ToUpper();                
                LeadDetails detail = uow.PropertyBL.ScheduleVisit(entity);

                if (detail != null && !string.IsNullOrEmpty(detail.Message))
                    return ValidationError(detail.Message);
                else if (detail != null && !string.IsNullOrEmpty(detail.PropertyDetail))
                {                    
                    uow.Commit();                    
                    var visitScheduled = new HttpCookie("ScheduledVisit_" + propertyIdForCookies);
                    visitScheduled.Expires = DateTime.Now.AddHours(24);
                    Response.Cookies.Add(visitScheduled);

                    var visitDate = !string.IsNullOrEmpty(detail.VisitDate) ? Convert.ToDateTime(detail.VisitDate).ToString("dd MMM yyyy") : detail.VisitDate;

                    string visitorMsg = "Your visit for " + detail.PropertyDetail + " has been scheduled for " + visitDate + " " + detail.VisitTime + ". Contact to " + detail.PropertyUserName + " on " + detail.PropertyUserMobile + ".  Location:";
                    visitorMsg += !string.IsNullOrEmpty(detail.LocationMapLink) ? detail.LocationMapLink : "__";
                    visitorMsg += "%0aWith Regards, %0aTelonestay";
                    string agentMsg = detail.UserName + " scheduled visit for property id " + dPropertyId + " (" + detail.PropertyDetail + ") for " + visitDate + " " + detail.VisitTime + ". Contact on " + detail.UserMobile + ". %0aWith Regards,  %0aTelonestay ";

                    string ShaduleVisitAgent = System.Configuration.ConfigurationManager.AppSettings["ShaduleVisitAgent"].ToString();
                    string ShaduleVisitUser = System.Configuration.ConfigurationManager.AppSettings["ShaduleVisitUser"].ToString();

                    //   string visitorMsg = "Your visit for " + detail.PropertyDetail + " has been scheduled for " + visitDate + " " + detail.VisitTime + ".";
                    // string agentMsg = detail.UserName + " scheduled visit for property id" + dPropertyId + " (" + detail.PropertyDetail + ") for " + visitDate + " " + detail.VisitTime + ".";
                    SendSMS.Send(detail.UserMobile, visitorMsg, ShaduleVisitUser);
                    SendSMS.Send(detail.PropertyUserMobile, agentMsg, ShaduleVisitAgent);
                    string message = "Contact to " + detail.PropertyUserName + " on " + detail.PropertyUserMobile + ".";
                    return Json(new { Success = true,Message=message});
                }

            }
            return Json(new { Sucess = false, ErrorMessage = "Something went wrong,Please try again." });
        }
        
        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult VerifyEnquiryDetail(LeadDetails entity)
        {
            int dPropertyId = Validation.DecryptToInt(entity.PropertyId);
            int dUserId = Validation.DecryptToInt(entity.UserId);
            if (dPropertyId == 0)
                return ValidationError("Invalid input.");
            if (dUserId == 0)
                return ValidationError("Invalid input.");

            if (entity.OTP < 1000 || entity.OTP > 9999)
                return ValidationError("4 digit OTP required.");
            
            string propertyIdForCookies = entity.PropertyId.Replace('=', '_');
            if (Request.Cookies["Enquiry_" + propertyIdForCookies] != null)
                return ValidationError("You have already Contacted.");
            
            using (var uow = new UnitOfWork())
            {
                entity.PropertyId = dPropertyId.ToString();
                entity.UserId = dUserId.ToString();                
                entity.OTPExpiredOn = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                LeadDetails detail = uow.PropertyBL.VeriyEnquiryDetail(entity);

                if (detail != null && !string.IsNullOrEmpty(detail.Message))
                    return ValidationError(detail.Message);
                else if (detail != null && !string.IsNullOrEmpty(detail.PropertyDetail))
                {
                    UserDetail userDetail = uow.UserBL.GetUserByUserId(dUserId);
                    uow.Commit();
                    if (CurrentUser.User == null)
                    {                        
                        if (userDetail != null && !string.IsNullOrEmpty(userDetail.UserId))
                        {
                            AuthCookie.AddAuthCookie(userDetail, Request, Response);
                        }
                    }
                    var visitScheduled = new HttpCookie("Enquiry_" + propertyIdForCookies);
                    visitScheduled.Expires = DateTime.Now.AddHours(24);
                    Response.Cookies.Add(visitScheduled);
                    
                    if (!string.IsNullOrEmpty(detail.PropertyUserType))
                    {
                        string userType =(detail.PropertyUserType.ToUpper() == "OWNER"?"Owner":"Agent");
                        string visitorMsg = "Hello! you are showing interest in Property " + detail.PropertyId + " on Haltn. Please contact to Property Owner " + detail.PropertyUserName + ", mobileNo. " + detail.PropertyUserMobile + ". Thank you!";
                        //string visitorMsg = "Hi " + detail.UserName + " , %0a You have showing interest in " + detail.PropertyId + ", " + detail.PropertyDetail + " . Contact to " + detail.PropertyUserName + " " + detail.PropertyUserMobile + ". %0aRegards TELONESTAY ";
                        //visitorMsg += !string.IsNullOrEmpty(detail.LocationMapLink) ? " Location " + detail.LocationMapLink : "";

                        string agentMsg = "Hello! A user showing interest in property " + detail.PropertyId + " on Haltn. Please contact to User " + detail.UserName + ",MobileNo " + detail.UserMobile + ". Thank you!";
                        //string agentMsg = detail.UserName + " showing interest in PropertyID (" + dPropertyId + " " + detail.PropertyDetail + ",,,,,,).Contact on " + detail.UserMobile + "%0aRegards TELONESTAY ";

                        string OWNER = System.Configuration.ConfigurationManager.AppSettings["OWNER"].ToString();
                        string USERid = System.Configuration.ConfigurationManager.AppSettings["USERid"].ToString();

                        SendSMS.Send(detail.UserMobile, visitorMsg, USERid);
                        SendSMS.Send(detail.PropertyUserMobile, agentMsg, OWNER);
                        string message="Contact to "+detail.PropertyUserName +" on " + detail.UserMobile + ".";
                        return Json(new { Success = true, Message = message });                       
                    }                    
                }

                return Json(new { Sucess = false, ErrorMessage = "Something went wrong,Please try again." });
            }
        }
        
        [HttpGet]          
        public ActionResult Details(string pid,string name)
        {            
            int propertyId = 0;
            if(!string.IsNullOrEmpty(pid))
            {
                if (Validation.IsInt(pid))
                    propertyId =Convert.ToInt32(pid);
               // propertyId = Validation.DecryptToInt(pid);
            }

            if (propertyId == 0)
                return PartialView("_Error");
            else
            {
                using (var uow = new UnitOfWork())
                {
                    VMProperty vmProperty = uow.PropertyBL.GetProperty(propertyId);                   
                    if (vmProperty.PropertyMaster !=null && vmProperty.PropertyMaster.PropertyDetail != null && vmProperty.PropertyMaster.PropertyDetail.PropertyId != "0")
                    {
                        if(!Request.Browser.IsMobileDevice || true)
                        {
                            PropertySearchParameter input = new PropertySearchParameter();
                            input.PropertyType = vmProperty.PropertyMaster.PropertyDetail.PropertyType;
                            input.Latitude = vmProperty.PropertyMaster.PropertyDetail.Latitude;
                            input.Longitude = vmProperty.PropertyMaster.PropertyDetail.Longitude;
                            input.Bhk = vmProperty.PropertyMaster.PropertyDetail.BhkType;                           

                            vmProperty.SimilarProperties=uow.PropertyBL.GetSimilarProperty(input);
                        }
                        
                        ViewBag.PropertyId =EncreptDecrpt.Encrypt(pid);
                       return View(vmProperty);
                    }
                    else if (vmProperty.PropertyMaster_PG !=null && vmProperty.PropertyMaster_PG.PropertyDetail_PG != null && vmProperty.PropertyMaster_PG.PropertyDetail_PG.PropertyId != "0")
                    {
                        if (!Request.Browser.IsMobileDevice || true)
                        {
                            PropertySearchParameter input = new PropertySearchParameter();
                            input.PropertyType = vmProperty.PropertyMaster_PG.PropertyDetail_PG.PropertyType;
                            input.Latitude = vmProperty.PropertyMaster_PG.PropertyDetail_PG.Latitude;
                            input.Longitude = vmProperty.PropertyMaster_PG.PropertyDetail_PG.Longitude;                            
                            vmProperty.SimilarProperties = uow.PropertyBL.GetSimilarProperty(input);
                            //vmProperty.PGFeedBack =uow.PropertyBL.GetPGFeedBack(propertyId);                            
                        }
                        ViewBag.PropertyId = EncreptDecrpt.Encrypt(pid);
                        return View(vmProperty);
                    }
                    else
                        return PartialView("_Error");
                }
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult postPGFeedBack(PropertyFeedBack pg)
        {
            if (CurrentUser.User == null)
                return Json(new { Success = false, ErrorMessage = "login" });

            if(pg.PropertyId>0)
            {
                if (!string.IsNullOrEmpty(pg.Description) && pg.Description.Length > 500)
                    return Json(new { Suceess = false, ErrorMessage = "Your Review can't be more than 500 characters." }); 
                if(pg.FoodQuality>0 && pg.FoodQuality<=5)
                    return Json(new { Suceess = false, ErrorMessage = "Please rate to food quality." });
                if (pg.Security > 0 && pg.Security <= 5)
                    return Json(new { Suceess = false, ErrorMessage = "Please rate to security." });
                if (pg.Wifi > 0 && pg.Wifi <= 5)
                    return Json(new { Suceess = false, ErrorMessage = "Please rate to wifi quality." });
                if (pg.Staff > 0 && pg.Staff<= 5)
                    return Json(new { Suceess = false, ErrorMessage = "Please rate to staff quality." });
                if (pg.Maintenance > 0 && pg.Maintenance <= 5)
                    return Json(new { Suceess = false, ErrorMessage = "Please rate to maintenance quality." });
                
                using (var uow = new UnitOfWork())
                {
                    int userId =Convert.ToInt32(CurrentUser.User.UserId);
                    pg.CreatedBy = userId;
                    if(uow.PropertyBL.AddPGFeedback(pg))
                    {
                        uow.Commit();
                        return Json(new { Success =true,Message="Your review posted successfully.Will be live post review."});
                    }                    
                }
            }

            return Json(new { Suceess = false,ErrorMessage="Something went wrong." });
        }

        public ActionResult WishList()
        {
            if (Request.Cookies["wishlist"] != null)
            {                
                string wishlist=Request.Cookies["wishlist"].Value;
                if(!string.IsNullOrWhiteSpace(wishlist) && wishlist.Split('|').Length>0)
                {
                    using(var uow=new UnitOfWork())
                    {
                        return View(uow.PropertyBL.GetWishListProperty(wishlist));
                    }
                }
            }
                return View();
        }
        
        private string findValueByMaster(IDictionary<string,string> lst,string input)
        {
            string output = string.Empty;

            if (!string.IsNullOrEmpty(input))
            {
                var aryType = input.Split('_');
                foreach (var item in aryType)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        string value = lst.Where(x => x.Key.ToUpper() == item.ToUpper().Trim()).Select(x => x.Value).FirstOrDefault();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (string.IsNullOrEmpty(output))
                                output = value;
                            else
                                output += "," + value;
                        }
                    }
                }
            }
            return output;
        }
        private bool ValidateTime(string visitingTime)
        {            
            var aryVisitingTime = visitingTime.Split(':');
            if (aryVisitingTime.Length == 3)
            {
                int[] timeHH = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
                string[] timeMM = { "00","30" };
                string[] timeAM = { "AM", "PM" };

                if (!timeHH.Any(s => s.ToString()==aryVisitingTime[0]))
                    return false;

                if (!timeMM.Any(s => s==aryVisitingTime[1]))
                    return false;

                if (!timeAM.Any(s => s==aryVisitingTime[2]))
                    return false;
            }
            return true;
        }
        private JsonResult ValidationError(string message)
        {
            return Json(new { Success = false, ErrorMessage = message });
        }
    }
}   