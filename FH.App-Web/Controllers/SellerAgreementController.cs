﻿using System.Web.Mvc;
using FH.Validator;
using FH.App.Service;

namespace FH.App_Web.Controllers
{
    public class SellerAgreementController : Controller
    {   
        [HttpGet]
        public ActionResult Accepted()
        {
            if(Request.QueryString["u"]!=null)
            {
                string sUserId = Request.QueryString["u"];
                if(!string.IsNullOrWhiteSpace(sUserId))
                {
                    int userId=Validation.DecryptToInt(sUserId);
                    if(userId>0)
                    {
                        using (var uow = new UnitOfWork())
                        {
                            uow.SellerAgreementBL.UpdateAgreementStatus(userId);
                            uow.Commit();
                        }
                            return View();
                    }
                }
            }

            return PartialView("_Error");
        }
    }
}