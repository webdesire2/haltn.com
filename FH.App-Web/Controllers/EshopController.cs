﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using FH.Web.Security;
using Newtonsoft.Json;
using FH.App.Service;
using FH.App.Entity;
using FH.App_Web;

namespace FH.App.Controllers
{
    public class EshopController : ApiController
    {
        private readonly EWalletBL _walletBL;

        public EshopController()
        {
            _walletBL = new EWalletBL();
        }
        
        [HttpGet]
        [Route("api/eshop/getUserDetail")]
        public HttpResponseMessage GetUserDetail(string token)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(token))
                    return ErrorResponse(Request, "Invalid token", null);

                string decryptToken = EncreptDecrpt.Decrypt(token);
                if (string.IsNullOrEmpty(decryptToken))
                    return ErrorResponse(Request, "Invalid token", null);

                dynamic jsonData = JsonConvert.DeserializeObject<dynamic>(decryptToken);
                string userName = jsonData.userName;
                string mobileNo = jsonData.mobile;
                string email = jsonData.email;
                string client = jsonData.client;

                if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(mobileNo))
                    return ErrorResponse(Request, "Invalid token", null);

                return SuccessResponse(Request, "", new { userName = userName, userMobile = mobileNo,userEmail=email, client = client });
            }
            catch
            {
                return ErrorResponse(Request, "Something went wrong,Please try again.", null);
            }
        }

        [HttpGet]
        [Route("api/eshop/getUserWallet")]
        public HttpResponseMessage GetUserWallet(string token,double orderAmount)
        {
            try
            {

                if (string.IsNullOrWhiteSpace(token))
                    return ErrorResponse(Request, "Invalid token", null);

                string decryptToken = EncreptDecrpt.Decrypt(token);
                if (string.IsNullOrEmpty(decryptToken))
                    return ErrorResponse(Request, "Invalid token", null);

                dynamic jsonData = JsonConvert.DeserializeObject<dynamic>(decryptToken);
                string userId = jsonData.token;

                if (string.IsNullOrWhiteSpace(userId))
                    return ErrorResponse(Request, "Invalid token", null);

                if (orderAmount <= 0)
                    return ErrorResponse(Request, "Order amount should be valid amount.", null);

                int user = Validator.Validation.DecryptToInt(userId);

                EWalletDE de = new EWalletDE()
                {
                    UserId = user
                };

                var userWallet = _walletBL.GetWalletAmount(de);

                if (userWallet.Amount >= orderAmount)
                    return SuccessResponse(Request, "User have sufficient wallet amount.", null);
                else
                {
                    string webUrl = WebConfigHelper.WebUrl+"/mywallet/add";
                    return ErrorResponse(Request, "User have insufficient wallet amount.", new { redirectUrl=webUrl });
                }
            }
            catch
            {
                return ErrorResponse(Request, "Something went wrong,Please try again.", null);
            }
        }
        
        [HttpPost]
        [Route("api/eshop/postOrderDetail")]
        public HttpResponseMessage PostOrderDetail([FromBody] PostOrderDetailReq req)
        {
            try
            {

                if (string.IsNullOrWhiteSpace(req.Token))
                    return ErrorResponse(Request, "Invalid token", null);

                string decryptToken = EncreptDecrpt.Decrypt(req.Token);
                if (string.IsNullOrEmpty(decryptToken))
                    return ErrorResponse(Request, "Invalid token", null);

                dynamic jsonData = JsonConvert.DeserializeObject<dynamic>(decryptToken);
                string userId = jsonData.token;

                if (string.IsNullOrWhiteSpace(userId))
                    return ErrorResponse(Request, "Invalid token", null);

                if (req.OrderAmount <= 0)
                    return ErrorResponse(Request, "Order amount should be valid amount.", null);

                int user = Validator.Validation.DecryptToInt(userId);

                EWalletDE de = new EWalletDE()
                {
                    UserId = user,
                    Amount=req.OrderAmount
                };

                _walletBL.AddDebitNode(de);
                return SuccessResponse(Request,"User order amount added successfully.",null);
               
            }
            catch
            {
                return ErrorResponse(Request, "Something went wrong,Please try again.", null);
            }
        }
        
        #region Util

        private HttpResponseMessage SuccessResponse(HttpRequestMessage request,string message,object data)
        {
            var res = new
            {
                success=1,
                message=message,
                data=data
            };
            return request.CreateResponse(HttpStatusCode.OK,res);
        }

        private HttpResponseMessage ErrorResponse(HttpRequestMessage request, string message, object data)
        {
            var res = new
            {
                success =0,
                message = message,
                data = data
            };
            return request.CreateResponse(HttpStatusCode.OK, res);
        }

        #endregion

        public class PostOrderDetailReq
        {
            public string Token { get; set; }
            public double OrderAmount { get; set; }
        }
    }
}