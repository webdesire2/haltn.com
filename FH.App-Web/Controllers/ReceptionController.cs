﻿using System.Web.Mvc;
using FH.Web.Security;
using FH.App.Service;
using FH.App.Entity;
using FH.Validator;
using FH.Util;
using FH.App.Web.ViewModel;
using System;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Razorpay.Api;
using System.Collections.Generic;
using Newtonsoft.Json;
using Paytm;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Linq;

namespace FH.App_Web.Controllers 
{
    public class ReceptionController : BaseController
    {
        private readonly TenantBL _tenantBL;
        private readonly UseCodeMasterBL _useCodeBL;

        public ReceptionController()
        {
            _tenantBL = new TenantBL();
            _useCodeBL = new UseCodeMasterBL();
            if (CurrentUser.User.Roles[0] == "Receptionist")
            {
                Redirect("Index");
            }
        }

        [HttpGet]
        public ActionResult Index()
        {
            if (CurrentUser.User.Roles[0] == "Receptionist")
            {
                return Redirect("MyTanent");  
            }   
            return View();
        }
         
        [HttpGet]
        public ActionResult AddNew()
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            { 
                return RedirectToAction("Index");
            }
            TenantMaster de = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                CheckInDate = DateTime.Now
            }; 
             
            var PGRoomData = _tenantBL.getAvailableRooms(de);
            return View(PGRoomData);
        }

        [HttpPost]
        public ActionResult GetAvailableRooms(TenantMaster req)
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            {
                return RedirectToAction("Index");
            }
            TenantMaster de = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                CheckInDate = req.CheckInDate
            }; 
            var result = _tenantBL.getAvailableRooms(de);
            if (result != null)
            {
                var result1 = JsonConvert.SerializeObject(result);
                return JsonResultHelper.Success(result1);
            }
            else 
            { 
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }
         
        [HttpGet]
        public ActionResult MyTanent(TenantMaster req)
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            {
                return RedirectToAction("Index");
            }

            ViewBag.RoomNo = req.RoomNo;
            ViewBag.TenantStatus = req.TenantStatus;
            ViewBag.RentPaid = req.RentPaid;

            if (req.TenantStatus == 0) {
                ViewBag.TenantStatus = 1;
                ViewBag.RentPaid = 2;
                req.TenantStatus = 1;
                req.RentPaid = 2;
            }


            TenantMaster de = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                TenantStatus = req.TenantStatus,
                RentPaid = req.RentPaid,
                RoomNo = req.RoomNo,
                Mobile = req.Mobile,
                IsActive = true,
            };

            var tenantData = _tenantBL.GetMyTanent(de);
            var mytanent = _tenantBL.GetRentPaidTanent(de);

            foreach (var singleData in tenantData)
            {
                singleData.RentPaid = 0;
                foreach (var singletanent in mytanent)
                {
                    if (singletanent.TenantId == singleData.TenantId)
                    {
                        singleData.RentPaid = 1;
                    }
                } 
            }

            return View(tenantData);
        }


        [HttpGet]
        public ActionResult PropertyMap(PropertyMapVM req)
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            {
                return RedirectToAction("Index");
            }
            TenantMaster de = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                CheckInDate = DateTime.Now
            };
            var result = _tenantBL.getAvailableRooms(de);
            //var tenant = _tenantBL.GetAllTenant(de);
            var ddd = result.FirstOrDefault();
            req.Pid = ddd.PropertyId;
            req.PGRoomData = result;

            return View(req);
        }
         


        [HttpPost]
        public ActionResult AddNewTananetSendOtp(TenantMaster req)
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            {
                return RedirectToAction("Index");
            }
            Random rdmNo = new Random();
            int OTP = 1111; // Convert.ToInt16(rdmNo.Next(1000, 9999).ToString("D4"));

            TenantMaster data = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                PropertyId = req.PropertyId,
                Name = req.Name,
                Mobile = req.Mobile,
                RoomNo = req.RoomNo, 
                RoomId = req.RoomId,
                Occupancy = req.Occupancy,
                Email = req.Email,
                SecurityAmount = req.SecurityAmount,
                MonthlyRent = req.MonthlyRent,
                CheckInDate = req.CheckInDate,
                SecurityDeposit = req.SecurityDeposit,
                OTP = OTP,
                Payment = req.Payment
            };  
            Session["NewTanentData"] = data;

            string OTPTEMP01 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP01"].ToString();
            //SendSMS.Send(data.Mobile, "Dear user, %0aYour OTP for Telonestay is " + OTP + ". Please do not share it. %0aWith Regards %0aTelonestay ", OTPTEMP01);
             
            var Mysession = Session["NewTanentData"] as TenantMaster;
            if (Mysession.Mobile != "" && Mysession.OTP != 0 )
            { 
                return JsonResultHelper.Success("Click on Final Submit to Add Tenant");
            } 
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult GenrateSRentQr(TenantPaymentDetail pd)
        {
            string error = PaySecurityValidation.Validate(pd);

            if (!string.IsNullOrEmpty(error))
                return JsonResultHelper.Error(error);

            TenantMaster tde = new TenantMaster()
            {
                TenantId = pd.TenantId,
            };
             
            var tenant = _tenantBL.GetRTenantMaster(tde);

            if (tenant == null)
                return JsonResultHelper.Error("you are not registered as tenant.");
            
            TenantPaymentDetail tpd = new TenantPaymentDetail()
            {
                PaymentType = "SECURITY",
                DepositAmount = pd.DepositAmount
            };

            TenantMaster de = new TenantMaster()
            {
                TenantId = tenant.TenantId,
                TenantPaymentDetail = tpd
            };

            int paymentId = _tenantBL.AddRNewPayment(de);
            if (paymentId > 0)
            {
                Dictionary<string, string> body = new Dictionary<string, string>();
                Dictionary<string, string> head = new Dictionary<string, string>();
                Dictionary<string, Dictionary<string, string>> requestBody = new Dictionary<string, Dictionary<string, string>>();

                string PaytmMID = System.Configuration.ConfigurationManager.AppSettings["PaytmMID"].ToString();
                string PaytmKey = System.Configuration.ConfigurationManager.AppSettings["PaytmKey"].ToString();
                body.Add("mid", PaytmMID);
                body.Add("orderId", paymentId.ToString());
                body.Add("amount", pd.DepositAmount.ToString());
                body.Add("businessType", "UPI_QR_CODE");
                body.Add("posId", tenant.PropertyId.ToString());
                var result = JsonConvert.SerializeObject(body);
                string paytmChecksum = Checksum.generateSignature(JsonConvert.SerializeObject(body), PaytmKey);

                head.Add("custId", tenant.TenantId.ToString());
                head.Add("clientId", tenant.TenantId.ToString());
                head.Add("version", "V1");
                head.Add("signature", paytmChecksum);
                requestBody.Add("body", body);
                requestBody.Add("head", head);
                string post_data = JsonConvert.SerializeObject(requestBody);
                string url = "https://securegw.paytm.in/paymentservices/qr/create";
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = post_data.Length;
                using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    requestWriter.Write(post_data);
                }
                string responseData = string.Empty;
                using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                {
                    responseData = responseReader.ReadToEnd();
                    Console.WriteLine(responseData);
                }

                var JsonData = JsonConvert.DeserializeObject<JObject>(responseData);
                string status = JsonData["body"]["resultInfo"]["resultStatus"].ToString();

                if (status == "SUCCESS")
                {
                    JsonData["body"]["OrderId"] = paymentId.ToString();
                    string transation = JsonData["body"].ToString();
                    string txnToken = JsonData["body"]["image"].ToString();
                    return JsonResultHelper.Success("QR Genrated", transation);
                }
                else
                {
                    return JsonResultHelper.Error("Some Error Occer. Please Try Again");
                }

            }
            return JsonResultHelper.Error("Some Error Occer. Please Try Again");
        }

            [HttpPost]
        [AjaxRequestValidation]
        public ActionResult GenrateRentQr(TenantMaster req)
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            {
                return RedirectToAction("Index");
            }
            TenantPaymentDetail pd = new TenantPaymentDetail();
            PaymentBL payBL = new PaymentBL();
            double couponAmount = 0;
            string couponCode = null;

            string error = string.Empty;  // PayRentValidation.Validate(pd, cm);

            if (string.IsNullOrWhiteSpace(error))
            {
                TenantMaster tde = new TenantMaster()
                {
                    TenantId = req.TenantId,
                };

                var tenantDetail = _tenantBL.GetRTenantMaster(tde);

                if (tenantDetail == null)
                    return JsonResultHelper.Error("you are not registered as tenant.");

                var checkOutDate = DateTime.ParseExact("0001-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); //DateTime('01/01/0001 00:00:00');
                if (tenantDetail.CheckOutDate != null)
                {
                    checkOutDate = (DateTime)tenantDetail.CheckOutDate;
                }
                if (tenantDetail.CheckInDate.Month == DateTime.Now.Month && tenantDetail.CheckInDate.Year == DateTime.Now.Year)
                {
                    var rentPerDay = tenantDetail.MonthlyRent > 0 ? tenantDetail.MonthlyRent / 30 : 0;
                    var fromDate = tenantDetail.CheckInDate;
                    pd.RentForMonthStartDate = fromDate;
                    pd.RentForMonthEndDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));
                    int days = (30 - Convert.ToDateTime(pd.RentForMonthStartDate).Day) + 1;
                    //int days = (Convert.ToDateTime(pd.RentForMonthEndDate).Day - Convert.ToDateTime(pd.RentForMonthStartDate).Day)+1;
                    pd.DepositAmount = Math.Round(rentPerDay * days, 2);
                    if (pd.DepositAmount < 1) { pd.DepositAmount = 1; }
                }
                else if (checkOutDate.Month == DateTime.Now.Month && checkOutDate.Year == DateTime.Now.Year)
                {
                    var lastDay = new DateTime(checkOutDate.Year, checkOutDate.Month, DateTime.DaysInMonth(checkOutDate.Year, checkOutDate.Month));
                    var rentPerDay = tenantDetail.MonthlyRent > 0 ? tenantDetail.MonthlyRent / lastDay.Day : 0;
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    pd.RentForMonthStartDate = fromDate;
                    pd.RentForMonthEndDate = checkOutDate;
                    int days = checkOutDate.Day;
                    tenantDetail.MonthlyRent = Math.Round(rentPerDay * days, 2);
                    pd.DepositAmount = Math.Round(rentPerDay * days, 2);
                    if (pd.DepositAmount < 1) { pd.DepositAmount = 1; }

                }
                else
                {
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    pd.RentForMonthStartDate = fromDate;
                    pd.RentForMonthEndDate = fromDate.AddMonths(1).AddDays(-1);
                    pd.DepositAmount = tenantDetail.MonthlyRent;
                }

                TenantPaymentDetail tpd = new TenantPaymentDetail()
                {
                    PaymentType = "RENT",
                    DepositAmount = pd.DepositAmount,
                    RentForMonthStartDate = pd.RentForMonthStartDate,
                    RentForMonthEndDate = pd.RentForMonthEndDate
                };

                PaymentDetail payment = new PaymentDetail()
                {
                    UseCode = couponCode,
                    UseCodeAmount = couponAmount
                };

                TenantMaster de = new TenantMaster()
                {
                    TenantId = tenantDetail.TenantId,
                    TenantPaymentDetail = tpd,
                    PaymentDetail = payment
                };

                int paymentId = _tenantBL.AddRNewPayment(de);
                if (paymentId > 0)
                {

                    Dictionary<string, string> body = new Dictionary<string, string>();
                    Dictionary<string, string> head = new Dictionary<string, string>();
                    Dictionary<string, Dictionary<string, string>> requestBody = new Dictionary<string, Dictionary<string, string>>();

                    string PaytmMID = System.Configuration.ConfigurationManager.AppSettings["PaytmMID"].ToString();
                    string PaytmKey = System.Configuration.ConfigurationManager.AppSettings["PaytmKey"].ToString();
                    body.Add("mid", PaytmMID);
                    body.Add("orderId", paymentId.ToString());
                    body.Add("amount", pd.DepositAmount.ToString());
                    body.Add("businessType", "UPI_QR_CODE");
                    body.Add("posId", tenantDetail.PropertyId.ToString());
                    var result = JsonConvert.SerializeObject(body);
                    string paytmChecksum = Checksum.generateSignature(JsonConvert.SerializeObject(body), PaytmKey);

                    head.Add("custId", tenantDetail.TenantId.ToString());
                    head.Add("clientId", tenantDetail.TenantId.ToString());
                    head.Add("version", "V1");
                    head.Add("signature", paytmChecksum);
                    requestBody.Add("body", body);
                    requestBody.Add("head", head);
                    string post_data = JsonConvert.SerializeObject(requestBody);
                    string url = "https://securegw.paytm.in/paymentservices/qr/create";
                    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                    webRequest.Method = "POST";
                    webRequest.ContentType = "application/json";
                    webRequest.ContentLength = post_data.Length;
                    using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
                    {
                        requestWriter.Write(post_data);
                    }
                    string responseData = string.Empty;
                    using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                    {
                        responseData = responseReader.ReadToEnd();
                        Console.WriteLine(responseData);
                    }

                    var JsonData = JsonConvert.DeserializeObject<JObject>(responseData);
                    string status = JsonData["body"]["resultInfo"]["resultStatus"].ToString();

                    if (status == "SUCCESS")
                    {
                        JsonData["body"]["OrderId"] = paymentId.ToString();
                        string transation = JsonData["body"].ToString();
                        string txnToken = JsonData["body"]["image"].ToString();
                        return JsonResultHelper.Success("QR Genrated", transation);
                    }
                    else
                    {
                        return JsonResultHelper.Error("Some Error Occer. Please Try Again");
                    }

                }
            }
            return JsonResultHelper.Error("Some Error Occer. Please Try Again");
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult checkPayment(TenantPaymentDetail req)
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            {
                return RedirectToAction("Index");
            }

            if (req.PaymentId > 0)
            {
                Dictionary<string, string> body = new Dictionary<string, string>();
                Dictionary<string, string> head = new Dictionary<string, string>();
                Dictionary<string, Dictionary<string, string>> requestBody = new Dictionary<string, Dictionary<string, string>>();

                string PaytmKey = System.Configuration.ConfigurationManager.AppSettings["PaytmKey"].ToString();
                string PaytmMID = System.Configuration.ConfigurationManager.AppSettings["PaytmMID"].ToString();
                body.Add("mid", PaytmMID);
                body.Add("orderId", req.PaymentId.ToString());

                string paytmChecksum = Checksum.generateSignature(JsonConvert.SerializeObject(body), PaytmKey);
                head.Add("signature", paytmChecksum);
                requestBody.Add("body", body);
                requestBody.Add("head", head);
                string post_data = JsonConvert.SerializeObject(requestBody);
                string url = "https://securegw.paytm.in/v3/order/status";

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = post_data.Length;

                using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    requestWriter.Write(post_data);
                }

                string responseData = string.Empty;

                using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                {
                    responseData = responseReader.ReadToEnd();
                    Console.WriteLine(responseData);
                }

                var JsonData = JsonConvert.DeserializeObject<JObject>(responseData);

                string transationStatus = JsonData["body"]["resultInfo"]["resultStatus"].ToString();
                string resultMsg = JsonData["body"]["resultInfo"]["resultMsg"].ToString();
                string resultCode = JsonData["body"]["resultInfo"]["resultCode"].ToString();
                string TXNID = JsonData["body"]["txnId"].ToString();
                string BANKTXNID = "";
                string TXNAMOUNT = JsonData["body"]["txnAmount"].ToString();
                string PAYMENTMODE = "";

                string PaymentStatus = "FAILURE";
                string StatusMessage = "Transaction Failure";
                if (transationStatus == "TXN_SUCCESS")
                {
                    StatusMessage = "Transaction Success";
                    PaymentStatus = "SUCCESS";
                    PAYMENTMODE = JsonData["body"]["paymentMode"].ToString();
                    BANKTXNID = JsonData["body"]["bankTxnId"].ToString();
                }
                if (transationStatus == "PENDING")
                {
                    StatusMessage = "Transaction Pending";
                    PaymentStatus = "PENDING"; 
                }

                PaymentDetail de = new PaymentDetail()
                {
                    PaymentId = req.PaymentId,
                    Status = PaymentStatus,  //responseCode == "E000" ? "SUCCESS" : "FAILURE"
                    TracingId = TXNID,
                    BankRefNo = BANKTXNID,
                    Amount = float.Parse(TXNAMOUNT, CultureInfo.InvariantCulture.NumberFormat),
                    PaymentMode = PAYMENTMODE, //method
                    StatusCode = resultCode,
                    StatusMessage = StatusMessage
                };
                PaymentBL _paymentBL = new PaymentBL();

                var responce = _paymentBL.UpdatePayment(de);

                return JsonResultHelper.Success("Payment Update Successfully");
            }
            return JsonResultHelper.Error("Some Error Occer. Please Try Again");
        }
         

        [HttpPost]
        public ActionResult AddTanent(TenantMaster req)
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            {
                return RedirectToAction("Index");
            }
            var Mysession = Session["NewTanentData"] as TenantMaster;
            if (  Mysession.Mobile != ""   && req.Mobile == Mysession.Mobile && Mysession.CreatedBy == Validation.DecryptToInt(CurrentUser.User.UserId)) {    
                TenantMaster data = new TenantMaster()
                {
                    CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                    PropertyId = Mysession.PropertyId,
                    Name = Mysession.Name,
                    Mobile = Mysession.Mobile,
                    RoomNo = Mysession.RoomNo,
                    RoomId = Mysession.RoomId,
                    Occupancy = Mysession.Occupancy,
                    Email = Mysession.Email,
                    SecurityAmount = Mysession.SecurityAmount,
                    MonthlyRent = Mysession.MonthlyRent,
                    CheckInDate = Mysession.CheckInDate,
                    SecurityDeposit = Mysession.SecurityDeposit
                };   
                var result = _tenantBL.AddNewTanent(data);
                if (result != 0) {
                    if (Mysession.Payment == 0) {
                        return JsonResultHelper.Success("New Tanent Added Successfully");
                    }  
                    TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), TenantId = result };
                    var tanentdata = _tenantBL.GetSingleTenant(de);

                    if (Mysession.Payment <= tanentdata.SecurityAmount) {
                        TenantMaster data1 = new TenantMaster()  {
                            CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                            Payment = Mysession.Payment,
                            PaymentType = "SECURITY",
                            TenantId = tanentdata.TenantId,
                            RentForMonthStartDate = DateTime.Now,
                            RentForMonthEndDate = DateTime.Now,
                        };

                        var result1 = _tenantBL.SSendPaymentLink(data1);
                        if (result1 != null) {
                            var message = "Dear " + tanentdata.Name + ", Bill from Haltn for Rs. " + Mysession.Payment + " for rent. Click2pay haltn.com/user/pbl/" + result1.Id.ToString() + " for SECURITY to continue reliable services";
                            string OTPTEMP05 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP05"].ToString();
                            SendSMS.Send(tanentdata.Mobile, message, OTPTEMP05);
                            Session["NewTanentData"] = null;
                            return JsonResultHelper.Success("New Tanent Added Successfully");
                        }  else  {
                            Session["NewTanentData"] = null;
                            return JsonResultHelper.Success("New Tanent Added Successfully But Tokan Not Adeed");
                        }
                    } else  {
                        return JsonResultHelper.Success("New Tanent Added Successfully But Tokan Not Adeed");
                    } 
                } else {
                    return JsonResultHelper.Error("Some error occer"); 
                }
            } else {
                return JsonResultHelper.Error("Incorrect OTP.");
            }
        } 







        [HttpGet]
        public ActionResult GetTenantPaymentLog(int tenantId = 0)
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            {
                return RedirectToAction("Index");
            }
            if (tenantId > 0)
            {
                TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), TenantId = tenantId };
                var lstLog = _tenantBL.GetTenantPaymentLog(de);



                //TanentNewPayment - This is Cash Collection Start
                var tenantData = _tenantBL.GetTanentStayDetails(de);
                 
                if (tenantData != null)
                {
                    tenantData.SecurityAmountDue = tenantData.SecurityAmount - tenantData.SecurityDeposit;

                    if (tenantData.CheckInDate.Month == DateTime.Now.Month && tenantData.CheckInDate.Year == DateTime.Now.Year)
                    {
                        var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / 30 : 0;
                        var fromDate = tenantData.CheckInDate;

                        tenantData.FromDate = fromDate;
                        tenantData.ToDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));

                        int days = (30 - tenantData.FromDate.Day) + 1;
                        //int days = (tenantData.ToDate.Day - tenantData.FromDate.Day)+1;
                        tenantData.MonthlyRent = Math.Round(rentPerDay * days, 2);
                        if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                    }
                    else if (tenantData.CheckOutDate.Month == DateTime.Now.Month && tenantData.CheckOutDate.Year == DateTime.Now.Year)
                    {
                        var lastDay = new DateTime(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month, DateTime.DaysInMonth(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month));
                        var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / lastDay.Day : 0;
                        var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        tenantData.FromDate = fromDate;
                        tenantData.ToDate = tenantData.CheckOutDate;
                        int days = tenantData.ToDate.Day;
                        tenantData.MonthlyRent = Math.Round(rentPerDay * days, 2);
                        if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                    }
                    else
                    {
                        var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        tenantData.FromDate = fromDate;
                        tenantData.ToDate = fromDate.AddMonths(1).AddDays(-1);
                    }

                    foreach (var item in lstLog)
                    {
                        if (item.RentForMonthEndDate == tenantData.ToDate && item.RentForMonthStartDate == tenantData.FromDate && item.DepositAmount == tenantData.MonthlyRent && item.PaymentStatus == "True")
                        {
                            tenantData.MonthlyRent = 0; break;
                        }
                    }

                }

                //TanentNewPayment - This is Cash Collection Start
                 

                VMPaymentDetails result = new VMPaymentDetails();
                result.TenantPaymentHistory = lstLog;
                result.TanentStayDetails = tenantData;

                if (Request.Browser.IsMobileDevice)
                    return PartialView("_Tenant_Payment_Log_Mobile", result);
                else
                    return PartialView("_Tenant_Payment_Log_Desktop", result);
            }

            return JsonResultHelper.Error("Invalid input.");
        }

        //RSendPaymentLink
        //SSendPaymentLink

        [HttpPost] 
        public ActionResult RSendPaymentLink(CollectCashData req)
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            {
                return RedirectToAction("Index");
            }
            TenantMaster data = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = req.rentAmount,
                PaymentType = "RENT",
                TenantId = req.TenantId,
                RentForMonthStartDate = req.rentForMonthStartDate,
                RentForMonthEndDate = req.rentForMonthEndDate,
            };

            var result = _tenantBL.RSendPaymentLink(data);

            if (result != null)
            { 
                TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), TenantId = req.TenantId };
                var tanentdata = _tenantBL.GetSingleTenant(de);

                var message = "Dear " + tanentdata.Name + ", Bill from Haltn for Rs. " + req.rentAmount + " for rent. Click2pay haltn.com/user/pbl/" + result.Id.ToString() + " for RENT to continue reliable services";
                //var message = "Dear " + tanentdata.Name + ", %0aBill from Telonestay for Rs. " + req.rentAmount + " (Rent). Click2pay telonestay.com/user/Pbl/" + result.Id.ToString() + " to continue reliable services.";
                string OTPTEMP05 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP05"].ToString(); 
                SendSMS.Send(tanentdata.Mobile, message, OTPTEMP05);

                return JsonResultHelper.Success(result.Id.ToString());
            } 
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }

        [HttpPost]
        public ActionResult SSendPaymentLink(CollectCashData req)
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            {
                return RedirectToAction("Index");
            }
            TenantMaster data = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = req.rentAmount,
                PaymentType = "SECURITY",
                TenantId = req.TenantId,
                RentForMonthStartDate = DateTime.Now,
                RentForMonthEndDate = DateTime.Now,
            };
             
            var result = _tenantBL.SSendPaymentLink(data);

            if (result != null)
            {
                TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), TenantId = req.TenantId };
                var tanentdata = _tenantBL.GetSingleTenant(de);

                var message = "Dear " + tanentdata.Name + ", Bill from Haltn for Rs. " + req.rentAmount + " for rent. Click2pay haltn.com/user/pbl/" + result.Id.ToString() + " for SECURITY to continue reliable services";
                //var message = "Dear " + tanentdata.Name + ", %0aBill from Telonestay for Rs. " + req.rentAmount + " (Security). Click2pay telonestay.com/user/Pbl/" + result.Id.ToString() + " to continue reliable services.";
                string OTPTEMP05 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP05"].ToString();
                SendSMS.Send(tanentdata.Mobile, message, OTPTEMP05);
                 
                return JsonResultHelper.Success(result.Id.ToString());
            } 
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }
        //DownloadInvoice

        [HttpGet]
        public ActionResult DownloadInvoice(int invoiceId)
        {
            if (CurrentUser.User.Roles[0] != "Receptionist")
            {
                return RedirectToAction("Index");
            }
            PaymentBL _paymentBL = new PaymentBL();
            var data = _paymentBL.GetTenantInvoice(invoiceId, Validation.DecryptToInt(CurrentUser.User.UserId));
            var invoiceHtml = RenderPartialToString(PartialView("_PaymentInvoice", data));
             
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                StringReader sr = new StringReader(invoiceHtml);
                iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 0f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                string invoiceName = "Invoice-" + invoiceId + ".pdf";
                return File(stream.ToArray(), "application/pdf", invoiceName);
            }
        }

    }
}