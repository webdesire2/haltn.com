﻿using System.Web.Mvc;
using System.Collections.Generic;
using FH.App.Entity;
using FH.App.Service;
using System.Net;
using FH.Validator;
using System;
using System.Linq;
using FH.App_Web;
using FH.Web.Security;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Xml.Linq;
using FH.App_Entity.DTO;
using System.Text;
using FH.App.Util; 
using System.Net.Mail;
using System.Configuration;

namespace FH.App.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index(string ptype)
        {
            ViewBag.propertyType = "FLAT";
            if (!string.IsNullOrEmpty(ptype))
            {
                if(ptype.ToUpper().Contains("FLATMATE"))
                    ViewBag.propertyType ="FLATMATE";
               else if (ptype.ToUpper().Contains("ROOM"))
                    ViewBag.propertyType = "ROOM";
                else if(ptype.ToUpper().Contains("PG"))
                ViewBag.propertyType = "PG";
            }
            
            using (UnitOfWork uow = new UnitOfWork())
            {
                PropertySearchParameter propertyS = new PropertySearchParameter()
                {
                    PropertyType = "PG",
                    City = "GN",
                    Locality = "Gurgaon, Haryana",
                    Latitude = "28.4594965",
                    Longitude = "77.0266383",
                    PageIndex = 1,
                    RoomType = "",
                    TenantGender = "",
                    MinPrice = 0,
                    MaxPrice = 20000,
                    PageSize = 30
                };
/*                propertyS.RoomType = findValueByMaster(RoomType.GetAll(), input.RoomType);
                propertyS.RoomType = string.IsNullOrWhiteSpace(propertyS.RoomType) ? "" : propertyS.RoomType == "Private Room" ? "Single,Single & Sharing" : propertyS.RoomType == "Shared Room" ? "Single & Sharing,Sharing" : "Single,Single & Sharing,Sharing";
                propertyS.TenantGender = (!string.IsNullOrWhiteSpace(input.TenantGender) ? input.TenantGender.Replace('_', ',') : "");
*/
                int totalRecord;
                ViewBag.allCategoriesMetaList = uow.PropertyBL.GetPGRoomMetaDetails();
                ViewBag.properties = uow.PropertyBL.GetAllProperty(propertyS, out totalRecord);
            } 

            return View();

        }
        
        [HttpGet]
        public PartialViewResult GetFlat(bool furnish=false,bool bachelors=false)
        {
            using (var uow = new UnitOfWork())
            {
                if(Request.Browser.IsMobileDevice)
                return PartialView("_Home_Property_Mobile", uow.PropertyBL.GetFlat(furnish,bachelors));
                else
                {
                    var list = uow.PropertyBL.GetFlat(furnish,bachelors).ToList().Take(4);
                    return PartialView("_Home_Property_Desktop", list);
                }
            }
        }

        [HttpGet]
        public PartialViewResult GetFlatMate(string gender)
        {
            using (var uow = new UnitOfWork())
            {
                if (Request.Browser.IsMobileDevice)
                    return PartialView("_Home_Property_Mobile", uow.PropertyBL.GetFlatMate(gender));
                else
                {                    
                    return PartialView("_Home_Property_Desktop", uow.PropertyBL.GetFlatMate(gender).Take(4));
                }
            }
        }

        [HttpGet]
        public PartialViewResult GetPG(string gender)
        {
            using (var uow = new UnitOfWork())
            {
                if (Request.Browser.IsMobileDevice)
                    return PartialView("_Home_Property_Mobile", uow.PropertyBL.GetPG(gender));
                else
                {
                    return PartialView("_Home_Property_Desktop", uow.PropertyBL.GetPG(gender).Take(4));
                }
            }
        }

        [HttpGet]
        public PartialViewResult GetRoom()
        {
            using (var uow = new UnitOfWork())
            {
                if (Request.Browser.IsMobileDevice)
                    return PartialView("_Home_Property_Mobile", uow.PropertyBL.GetRoom());
                else
                {
                    return PartialView("_Home_Property_Desktop", uow.PropertyBL.GetRoom().Take(4));
                }
            }
        }

        [HttpGet]
        public PartialViewResult GetCityLocality()
        {
            using (var uow = new UnitOfWork())
            {
                return PartialView("_City_Area_List_Section", uow.PropertyBL.GetCityLocalityList());                
            }
        }

        [HttpGet]
        public ActionResult AboutUs()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CovidSafe()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ContactUs()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Home()
        {
            return View();
        }

        //[HttpGet]
        //public ActionResult team()
        //{
        //    return View();
        //}

        [HttpGet]
        public ActionResult TermAndCondition()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Faq()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ListingPolicy()
        {
            return View();
        }

        //[HttpGet]
        //public ActionResult BuyerManual()
        //{
        //    return View();
        //}

        //[HttpGet]
        //public ActionResult SellerManual()
        //{
        //    return View();
        //}

        [HttpPost]
        [FH.Web.Security.AjaxRequestValidation]
        public JsonResult QuickEnquiry(QuickEnquiry entity)
        {
            if (string.IsNullOrWhiteSpace(entity.UserName))
                return Json(new { Success = false,ErrorMessage="User name required." });
            else if(entity.UserName.Length>50)
                return Json(new { Success = false, ErrorMessage = "User name can't be more than 50 characters." });

            if (Validation.MobileNo(entity.UserMobile))
                return Json(new { Success = false, ErrorMessage = "Enter valid mobile no." });

            if(string.IsNullOrWhiteSpace(entity.UserEmail))
                return Json(new { Success = false, ErrorMessage = "Enter valid email address." });
            if (Validation.EmailAddress(entity.UserEmail))
                return Json(new { Success = false, ErrorMessage = "Enter valid email address." });

            if(!string.IsNullOrWhiteSpace(entity.Description) && entity.Description.Length>500)
                return Json(new { Success = false, ErrorMessage = "Description can't be more than 500 characters." });

            using (var uow = new UnitOfWork())
            {   
                    uow.UserBL.AddQuickEnquiry(entity);
                    uow.Commit();
            }
            //SendQueryMail( entity );
            return Json(new { Success = true });
        }

        private void SendQueryMail(QuickEnquiry entity) {
            SmtpClient client = new SmtpClient();
            //SmtpClient client = new SmtpClient("some.server.com");
            ////If you need to authenticate
            string fromEmailCode = ConfigurationManager.AppSettings["FromEmailCode"].ToString();
            string fromEmail = ConfigurationManager.AppSettings["FromEmail"].ToString();
            string toEmail = ConfigurationManager.AppSettings["ToEmail"].ToString();
            
            //client.Credentials = new NetworkCredential("ddss1762000@gmail.com", "Deepak@1234");
            client.Credentials = new NetworkCredential(fromEmail, fromEmailCode);
                
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(fromEmail);
            mailMessage.To.Add(toEmail);
            mailMessage.Subject = "New Enquiry From TeloneStay";
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = $"<h1>User details are - </h1><h2>Name : {entity.UserName} </h2> <h2>Email: {entity.UserEmail}</h2> <h2>Mobile Number: {entity.UserMobile}</h2> <h2>Description: {entity.Description}</h2>";
             
            client.Send(mailMessage); 
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult EShopNow()
        {
            int userId = Validation.DecryptToInt(CurrentUser.User.UserId);
            if (userId > 0)
            {
                string client = "telonestay";
                string userName = CurrentUser.User.Name;
                string mobile = CurrentUser.User.Mobile;
                string token = CurrentUser.User.UserId;
                string email = CurrentUser.User.Email;
                var querystring = new
                {
                    client,userName,mobile,email,token                    
                };
                
               string jsonData = JsonConvert.SerializeObject(querystring,Formatting.Indented);
                string url=string.Concat(WebConfigHelper.ShoppingSiteUrl,"?u=",EncreptDecrpt.Encrypt(jsonData));
                
               return Redirect(url);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult VoucherCodeList()
        {
            UseCodeMasterBL _useCodeBL = new UseCodeMasterBL();
           var voucerCode= _useCodeBL.GetAllActiveCode();
            return View(voucerCode);
        }

        [HttpGet]
        public ActionResult Sitemap()
        {
             XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
             XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";

            SitemapBL bl = new SitemapBL();
            var data=bl.GetAllSEOCategory();

            var sitemap = new XDocument(
               new XDeclaration("1.0", "utf-8", "yes"),
                   new XElement(xmlns + "urlset",
                     new XAttribute("xmlns", xmlns),
                     new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                     new XAttribute(xsi + "schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"),
                     from item in data
                     select CreateElement(item, xmlns)
                     )
                );

            return new XmlActionResult(sitemap);
        }


        private XElement CreateElement(SiteMapDTO item,XNamespace xmlns)
        {
            string url = WebConfigHelper.WebUrl + "/search/pg/"+item.Url.ToLower();
            var itemElement = new XElement(xmlns + "url", new XElement(xmlns + "loc",url));            
            itemElement.Add(new XElement(xmlns + "lastmod", item.CreatedOn.ToString("yyyy-MM-dd")));            
            itemElement.Add(new XElement(xmlns + "priority",1));
            return itemElement;
        }
    }
}