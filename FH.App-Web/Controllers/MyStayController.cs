﻿using System.Web.Mvc;
using FH.Web.Security;
using FH.App.Service;
using FH.App.Entity;
using FH.Validator;
using FH.Util;
using FH.App.Web.ViewModel;
using System;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Razorpay.Api;
using System.Collections.Generic;

namespace FH.App_Web.Controllers
{
    [CustomAuthorize]
    public class MyStayController : BaseController
    {
        private readonly TenantBL _tenantBL;
        private readonly UseCodeMasterBL _useCodeBL;
        
        public MyStayController()
        {
            _tenantBL = new TenantBL();
            _useCodeBL = new UseCodeMasterBL();
        }

        [HttpGet]
        public ActionResult PayNow()
        {
            TenantMaster de = new TenantMaster() 
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId)
            };
             
            var tenantData = _tenantBL.GetMyCurrentStay(de);

            VMMyStay vm = new VMMyStay(); 
             
            if (tenantData != null)   
            {  
                tenantData.SecurityAmountDue = tenantData.SecurityAmount - tenantData.SecurityDeposit;

                if(tenantData.CheckInDate.Month==DateTime.Now.Month && tenantData.CheckInDate.Year==DateTime.Now.Year)
                {
                    var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / 30 : 0;
                    var fromDate= tenantData.CheckInDate;
                    tenantData.FromDate = fromDate; 
                    tenantData.ToDate=new DateTime(fromDate.Year, fromDate.Month,DateTime.DaysInMonth(fromDate.Year,fromDate.Month));

                    int days = (30 - tenantData.FromDate.Day) + 1;
                    //int days = (tenantData.ToDate.Day - tenantData.FromDate.Day)+1;
                    tenantData.MonthlyRent =Math.Round(rentPerDay * days,2);
                    if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                } else if (tenantData.CheckOutDate.Month == DateTime.Now.Month && tenantData.CheckOutDate.Year == DateTime.Now.Year) {
                    var lastDay = new DateTime(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month, DateTime.DaysInMonth(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month));
                    var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / lastDay.Day : 0;
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    tenantData.FromDate = fromDate;
                    tenantData.ToDate = tenantData.CheckOutDate;
                    int days = tenantData.ToDate.Day; 
                    tenantData.MonthlyRent = Math.Round(rentPerDay * days, 2);
                    if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                } else { 
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    tenantData.FromDate = fromDate;
                    tenantData.ToDate = fromDate.AddMonths(1).AddDays(-1);                    
                } 
                 
                de.PropertyId = tenantData.PropertyId;
                var data = _tenantBL.GetAllPayment(de);

                foreach (var item in data)
                {
                    if (item.RentForMonthEndDate == tenantData.ToDate && item.RentForMonthStartDate == tenantData.FromDate && item.Amount == tenantData.MonthlyRent && item.PaymentStatus == "SUCCESS")
                    {
                        tenantData.MonthlyRent = 0;
                        break;
                    }  
                }

                vm.IsTenantRegistered = true;
                vm.PropertyDetailPageUrl = string.Concat(System.Configuration.ConfigurationManager.AppSettings["WebUrl"].ToString(), "/property/details/", tenantData.PropertyId);                
                vm.MyStayDetail = tenantData;
                vm.MyOrderDTOs = data;                
            }
            else
                vm.IsTenantRegistered = false;
           
            return View(vm);
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult PayRent(TenantPaymentDetail pd, UseCodeMaster cm)
        {
            PaymentBL payBL = new PaymentBL();           
            double couponAmount =0;
            string couponCode = null;

            string error = string.Empty;  // PayRentValidation.Validate(pd, cm);
           
            if (string.IsNullOrWhiteSpace(error))
            {
                TenantMaster tde = new TenantMaster()
                {
                    CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId)
                };

                var tenantDetail = _tenantBL.GetTenantMaster(tde);

                if (tenantDetail == null)
                    return JsonResultHelper.Error("You are not registered as tenant. Or No Account as Property. Please Contact to Owner.");
 
                var checkOutDate = DateTime.ParseExact("0001-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); //DateTime('01/01/0001 00:00:00');
                if (tenantDetail.CheckOutDate != null)
                {
                    checkOutDate = (DateTime)tenantDetail.CheckOutDate;
                }
                if (tenantDetail.CheckInDate.Month == DateTime.Now.Month && tenantDetail.CheckInDate.Year == DateTime.Now.Year)
                {
                    var rentPerDay = tenantDetail.MonthlyRent > 0 ? tenantDetail.MonthlyRent / 30 : 0;
                    var fromDate = tenantDetail.CheckInDate;
                    pd.RentForMonthStartDate = fromDate;
                    pd.RentForMonthEndDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));
                    int days = (30 - Convert.ToDateTime(pd.RentForMonthStartDate).Day) + 1;
                    //int days = (Convert.ToDateTime(pd.RentForMonthEndDate).Day - Convert.ToDateTime(pd.RentForMonthStartDate).Day)+1;
                    pd.DepositAmount =Math.Round(rentPerDay*days,2);
                    if (pd.DepositAmount < 1) { pd.DepositAmount = 1; }
                } else if(checkOutDate.Month == DateTime.Now.Month && checkOutDate.Year == DateTime.Now.Year) {
                    var lastDay = new DateTime(checkOutDate.Year, checkOutDate.Month, DateTime.DaysInMonth(checkOutDate.Year, checkOutDate.Month));
                    var rentPerDay = tenantDetail.MonthlyRent > 0 ? tenantDetail.MonthlyRent / lastDay.Day : 0;
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    pd.RentForMonthStartDate = fromDate;
                    pd.RentForMonthEndDate = checkOutDate;
                    int days = checkOutDate.Day;
                    tenantDetail.MonthlyRent = Math.Round(rentPerDay * days, 2);
                    pd.DepositAmount = Math.Round(rentPerDay * days,2); 
                    if (pd.DepositAmount < 1) { pd.DepositAmount = 1; }
                      
                } else {
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    pd.RentForMonthStartDate = fromDate;
                    pd.RentForMonthEndDate = fromDate.AddMonths(1).AddDays(-1);
                    pd.DepositAmount = tenantDetail.MonthlyRent;
                }
                
                //  else if (tenantDetail != null && tenantDetail.MonthlyRent != pd.DepositAmount)
                //    return JsonResultHelper.Error(string.Format("{0}{1}","Amount should be ",tenantDetail.MonthlyRent));

                if (!string.IsNullOrWhiteSpace(cm.Code))
                {
                    var useCode = _useCodeBL.GetByCode(cm);
                    if (useCode != null && useCode.UseCodeId>0)
                    {
                        couponCode = useCode.Code;
                        couponAmount= Math.Round((pd.DepositAmount * useCode.UseCodeTypePercentage) / 100, 2);
                        couponAmount = couponAmount > 10000 ? 10000 : couponAmount;
                    }
                    else
                        return JsonResultHelper.Error("Invalid voucher code.");
                }
                
                TenantPaymentDetail tpd = new TenantPaymentDetail()
                {
                    PaymentType = "RENT",
                    DepositAmount = pd.DepositAmount,
                    RentForMonthStartDate = pd.RentForMonthStartDate,
                    RentForMonthEndDate = pd.RentForMonthEndDate
                };

                PaymentDetail payment = new PaymentDetail()
                {                   
                    UseCode=couponCode,
                    UseCodeAmount=couponAmount
                };

                TenantMaster de = new TenantMaster()
                {
                    CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                    TenantPaymentDetail = tpd,
                    PaymentDetail=payment
                };

                int paymentId = _tenantBL.AddNewPayment(de);
                if (paymentId>0)
                {
                    string CCAccessCode = System.Configuration.ConfigurationManager.AppSettings["CCAccessCode"].ToString();
                    string txnToken = payBL.EPPaymentRequest(pd.DepositAmount, paymentId, tenantDetail);
                    VMPayment orderData = new VMPayment();
                    orderData.paymentID = paymentId.ToString();
                    orderData.Amount = pd.DepositAmount;
                    orderData.PaytmMID = CCAccessCode;
                    orderData.currency = "INR";
                    orderData.name = tenantDetail.Name;
                    orderData.email = tenantDetail.Email;
                    orderData.contactNumber = tenantDetail.Mobile;
                    orderData.address = "";
                    orderData.description = "";
                    orderData.txnToken = txnToken;

                    return PartialView("Rpayment",orderData); 
                }
                else
                    return JsonResultHelper.Error("Something went wrong.Please try again.");                
            }
            else
                return JsonResultHelper.Error(error);
        }
        



        [HttpPost] 
        [AjaxRequestValidation]
        public ActionResult PaySecurity(TenantPaymentDetail pd)
        {
            string error=PaySecurityValidation.Validate(pd);

            if (!string.IsNullOrEmpty(error))
                return JsonResultHelper.Error(error);

            var tenant=_tenantBL.GetTenantMaster(new TenantMaster() {CreatedBy=Validation.DecryptToInt(CurrentUser.User.UserId)});

            if (tenant == null)
                return JsonResultHelper.Error("you are not registered as tenant.");
            else if (pd.DepositAmount != (tenant.SecurityAmount - tenant.SecurityDeposit))
                return JsonResultHelper.Error(string.Format("Security due amount is ", (tenant.SecurityAmount - tenant.SecurityDeposit)));
           
            TenantPaymentDetail tpd = new TenantPaymentDetail()
            {
                PaymentType = "SECURITY",
                DepositAmount = pd.DepositAmount               
            };
            
            TenantMaster de = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                TenantPaymentDetail = tpd                
            };

            int paymentId=_tenantBL.AddNewPayment(de);

            if(paymentId>0)
            {
                PaymentBL _paymentBL = new PaymentBL();

                string CCAccessCode = System.Configuration.ConfigurationManager.AppSettings["CCAccessCode"].ToString();
                string txnToken = _paymentBL.EPPaymentRequest(pd.DepositAmount, paymentId, tenant);
                VMPayment orderData = new VMPayment();
                orderData.paymentID = paymentId.ToString();
                orderData.Amount = pd.DepositAmount;
                orderData.PaytmMID = CCAccessCode;
                orderData.currency = "INR";
                orderData.name = tenant.Name;
                orderData.email = tenant.Email;
                orderData.contactNumber = tenant.Mobile;
                orderData.address = "";
                orderData.description = "";
                orderData.txnToken = txnToken;

                return PartialView("Rpayment", orderData);
            }         
            else
                return JsonResultHelper.Error("Something went wrong,Please try again.");
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult CheckOut(TenantMaster tm)
        {
            string error=CheckOutValidation.Validate(tm);
            if (!string.IsNullOrEmpty(error))
                return JsonResultHelper.Error(error);

            tm.CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId);
            try
            {
                int id = _tenantBL.CheckOutStay(tm);
                if (id > 0)
                    return JsonResultHelper.Success("your checkout request has been submitted.");
                else
                    return JsonResultHelper.Error("Something went wrong, please try again.");
            }
            catch(Exception e)
            {
                return JsonResultHelper.Error(e.Message);
            }
        }


        [HttpGet]
        public ActionResult DownloadInvoice(int invoiceId)
        {
            PaymentBL _paymentBL = new PaymentBL();
            var data=_paymentBL.GetTenantInvoice(invoiceId,Validation.DecryptToInt(CurrentUser.User.UserId));            
            var invoiceHtml = RenderPartialToString(PartialView("_PaymentInvoice",data));

            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                StringReader sr = new StringReader(invoiceHtml);
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                string invoiceName = "Invoice-" + invoiceId+".pdf";
                return File(stream.ToArray(), "application/pdf",invoiceName);
            } 
        }
    }
}