﻿using System.Web.Mvc;
using FH.Web.Security;
using FH.App.Entity;
using System;
using FH.App.Service;
using System.Collections.Generic;
using Paytm;
using System.Globalization;
using System.Configuration;
using CCA.Util;
using System.Collections.Specialized;

namespace FH.App_Web.Controllers
{
    //[CustomAuthorize]
    public class PaymentController : BaseController
    {
        [HttpPost]
        public new ActionResult Response()
        {           
            try {
                string CCAWorkingKey = ConfigurationManager.AppSettings["CCAWorkingKey"].ToString();
                CCACrypto ccaCrypto = new CCACrypto();
                string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], CCAWorkingKey);
                NameValueCollection Params = new NameValueCollection();
                string[] segments = encResponse.Split('&');
                foreach (string seg in segments)
                {
                    string[] parts = seg.Split('=');
                    if (parts.Length > 0)
                    {
                        string Key = parts[0].Trim();
                        string Value = parts[1].Trim();
                        Params.Add(Key, Value);
                    }
                }


                string PaymentStatus = "FAILURE";
                string RpaymentStatus = Params["order_status"];
                if (RpaymentStatus == "Success")
                {
                    PaymentStatus = "SUCCESS";
                }
                if (RpaymentStatus == "Pending")
                {
                    PaymentStatus = "PENDING";
                }


                PaymentBL _paymentBL = new PaymentBL();
                PaymentDetail de = new PaymentDetail()
                {
                    PaymentId = Int32.Parse(Params["order_id"]),
                    Status = PaymentStatus,  //responseCode == "E000" ? "SUCCESS" : "FAILURE"
                    TracingId = Params["tracking_id"],
                    BankRefNo = Params["bank_ref_no"],
                    Amount = float.Parse(Params["amount"], CultureInfo.InvariantCulture.NumberFormat),
                    PaymentMode = Params["payment_mode"], //method
                    StatusCode = Params["status_code"],
                    StatusMessage = Params["status_message"],
                    PaymentMID = Params["sub_account_id"],
                };
                _paymentBL.UpdatePayment(de);

                return RedirectToAction("Response", new { pid = de.PaymentId });
            }
            catch (Exception e) 
            {
                ViewBag.Error = e.InnerException;
            }
            return RedirectToAction("Response", new { pid = 0});
        }
 

        [HttpGet]
        public new ActionResult Response(int pid=0)
        {
            TenantBL _tenantBL = new TenantBL();
            if(pid>0) 
            { 
                PaymentDetail pd = new PaymentDetail() { PaymentId = pid };

                TenantMaster tm = new TenantMaster()
                {
                    PaymentDetail=pd,
                    //CreatedBy = Validator.Validation.DecryptToInt(CurrentUser.User.UserId)
                }; 

                var detail=_tenantBL.GetPaymentDetail(tm);
                return View(detail);
            }        
            
            return PartialView("_Error");
        }        
    }
}