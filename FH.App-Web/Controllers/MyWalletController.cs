﻿using System.Web.Mvc;
using FH.Web.Security;
using FH.App.Service;
using FH.App.Entity;
using FH.Validator;
using System.Linq;
using FH.Util;
using System;

namespace FH.App_Web.Controllers
{
    [CustomAuthorize]
    public class MyWalletController : BaseController
    {
        private readonly EWalletBL _ewalletBL;

        public MyWalletController()
        {
            _ewalletBL = new EWalletBL();
        }

        [HttpGet]
        public ActionResult Wallet()
        {
            EWalletDE de = new EWalletDE()
            {
                UserId = Validation.DecryptToInt(CurrentUser.User.UserId)
            };

            var data = _ewalletBL.GetAll(de);
            double walletBalance = 0;
           
            if(data!=null)
            {
                walletBalance=data.Where(x => x.TransactionType.ToUpper() == "CREDIT").Sum(x=>x.Amount);                
            }

            ViewBag.WalletBalance =walletBalance;
            return View(data);
        }

        [HttpPost]   
        [AjaxRequestValidation]
        public JsonResult Add(double amount)
        {
           
            if (amount > 1)
            {
                int discount = WebConfigHelper.WalletDiscountPerc;
                double discountAmount = Math.Round((amount*discount)/100,2);
                double payableAmount = Math.Round(amount - discountAmount, 2);
                EWalletDE de = new EWalletDE()
                {
                    UserId =Validation.DecryptToInt(CurrentUser.User.UserId),
                    Amount = payableAmount,                    
                    PaymentType ="EWALLET",
                    UseCode="DISCOUNT",
                    UseCodeAmount=discountAmount
                };

                int orderId=_ewalletBL.AddOrder(de); 
                if (orderId > 0)
                {
                    PaymentBL _paymentBL = new PaymentBL();
                    string url = "";// _paymentBL.EPPaymentRequest(payableAmount,orderId, de);
                    return JsonResultHelper.Success("", url);
                }
                else
                    return JsonResultHelper.Error("Something went wrong,Please try again.");
                
            }
            else
                return JsonResultHelper.Error("Amount should be positive number.");
        }
    }
}