﻿using System.Web.Mvc;
using FH.Web.Security;
using FH.App.Service;
using FH.App.Entity;
using FH.Validator;
using FH.Util;
using FH.App.Web.ViewModel;
using System;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace FH.App_Web.Controllers
{
    [CustomAuthorize]
    public class MyStayControllerOld : BaseController
    {
        private readonly TenantBL _tenantBL;
        private readonly UseCodeMasterBL _useCodeBL;
        
        public MyStayControllerOld()
        {
            _tenantBL = new TenantBL();
            _useCodeBL = new UseCodeMasterBL();
        }

        [HttpGet]
        public ActionResult PayNow()
        {
            TenantMaster de = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId)
            };

            var tenantData = _tenantBL.GetMyCurrentStay(de);

            VMMyStay vm = new VMMyStay();

            if (tenantData != null)
            {
                tenantData.SecurityAmountDue = tenantData.SecurityAmount - tenantData.SecurityDeposit;

                if(tenantData.CheckInDate.Month==DateTime.Now.Month && tenantData.CheckInDate.Year==DateTime.Now.Year)
                {
                    var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / 30 : 0;
                    var fromDate= tenantData.CheckInDate;
                    tenantData.FromDate = fromDate; 
                    tenantData.ToDate=new DateTime(fromDate.Year, fromDate.Month,DateTime.DaysInMonth(fromDate.Year,fromDate.Month));

                    int days = (tenantData.ToDate.Day - tenantData.FromDate.Day)+1;
                    tenantData.MonthlyRent =Math.Round(rentPerDay * days,2);
                }
                else
                {
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    tenantData.FromDate = fromDate;
                    tenantData.ToDate = fromDate.AddMonths(1).AddDays(-1);                    
                }
               
                
                var data = _tenantBL.GetAllPayment(de);

                vm.IsTenantRegistered = true;
                vm.PropertyDetailPageUrl = string.Concat(System.Configuration.ConfigurationManager.AppSettings["WebUrl"].ToString(), "/property/details/", tenantData.PropertyId);                
                vm.MyStayDetail = tenantData;
                vm.MyOrderDTOs = data;                
            }
            else
                vm.IsTenantRegistered = false;
           
            return View(vm);
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult PayRent(TenantPaymentDetail pd, UseCodeMaster cm)
        {
            PaymentBL payBL = new PaymentBL();          
            double couponAmount =0;
            string couponCode = null;

            string error = string.Empty;  // PayRentValidation.Validate(pd, cm);
           
            if (string.IsNullOrWhiteSpace(error))
            {
                TenantMaster tde = new TenantMaster()
                {
                    CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId)
                };

                var tenantDetail = _tenantBL.GetTenantMaster(tde);

                if (tenantDetail == null)
                    return JsonResultHelper.Error("you are not registered as tenant.");

                if (tenantDetail.CheckInDate.Month == DateTime.Now.Month && tenantDetail.CheckInDate.Year == DateTime.Now.Year)
                {
                    var rentPerDay = tenantDetail.MonthlyRent > 0 ? tenantDetail.MonthlyRent / 30 : 0;
                    var fromDate = tenantDetail.CheckInDate;
                    pd.RentForMonthStartDate = fromDate;
                    pd.RentForMonthEndDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));
                    int days = (Convert.ToDateTime(pd.RentForMonthEndDate).Day - Convert.ToDateTime(pd.RentForMonthStartDate).Day)+1;
                    pd.DepositAmount =Math.Round(rentPerDay*days);
                }
                else
                {
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    pd.RentForMonthStartDate = fromDate;
                    pd.RentForMonthEndDate = fromDate.AddMonths(1).AddDays(-1);
                    pd.DepositAmount = tenantDetail.MonthlyRent;
                }
                
                //  else if (tenantDetail != null && tenantDetail.MonthlyRent != pd.DepositAmount)
                //    return JsonResultHelper.Error(string.Format("{0}{1}","Amount should be ",tenantDetail.MonthlyRent));

                if (!string.IsNullOrWhiteSpace(cm.Code))
                {
                    var useCode = _useCodeBL.GetByCode(cm);
                    if (useCode != null && useCode.UseCodeId>0)
                    {
                        couponCode = useCode.Code;
                        couponAmount= Math.Round((pd.DepositAmount * useCode.UseCodeTypePercentage) / 100, 2);
                        couponAmount = couponAmount > 10000 ? 10000 : couponAmount;
                    }
                    else
                        return JsonResultHelper.Error("Invalid voucher code.");
                }
                
                TenantPaymentDetail tpd = new TenantPaymentDetail()
                {
                    PaymentType = "RENT",
                    DepositAmount = pd.DepositAmount,
                    RentForMonthStartDate = pd.RentForMonthStartDate,
                    RentForMonthEndDate = pd.RentForMonthEndDate
                };

                PaymentDetail payment = new PaymentDetail()
                {                   
                    UseCode=couponCode,
                    UseCodeAmount=couponAmount
                };

                TenantMaster de = new TenantMaster()
                {
                    CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                    TenantPaymentDetail = tpd,
                    PaymentDetail=payment
                };

                int paymentId = _tenantBL.AddNewPayment(de);
                if (paymentId>0)
                {
                    string paymentUrl= payBL.EPPaymentRequest(pd.DepositAmount, paymentId, tenantDetail);
                    return JsonResultHelper.Success("", paymentUrl);
                }
                else
                    return JsonResultHelper.Error("Something went wrong.Please try again.");                
            }
            else
                return JsonResultHelper.Error(error);
        }
        
        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult PaySecurity(TenantPaymentDetail pd)
        {
            string error=PaySecurityValidation.Validate(pd);

            if (!string.IsNullOrEmpty(error))
                return JsonResultHelper.Error(error);

            var tenant=_tenantBL.GetTenantMaster(new TenantMaster() {CreatedBy=Validation.DecryptToInt(CurrentUser.User.UserId)});

            if (tenant == null)
                return JsonResultHelper.Error("you are not registered as tenant.");
            else if (pd.DepositAmount != (tenant.SecurityAmount - tenant.SecurityDeposit))
                return JsonResultHelper.Error(string.Format("Security due amount is ", (tenant.SecurityAmount - tenant.SecurityDeposit)));
           
            TenantPaymentDetail tpd = new TenantPaymentDetail()
            {
                PaymentType = "SECURITY",
                DepositAmount = pd.DepositAmount               
            };
            
            TenantMaster de = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                TenantPaymentDetail = tpd                
            };

            int paymentId=_tenantBL.AddNewPayment(de);

            if(paymentId>0)
            {
                PaymentBL _paymentBL = new PaymentBL();
                string url = _paymentBL.EPPaymentRequest(pd.DepositAmount, paymentId, tenant);
                return JsonResultHelper.Success("", url);
            }         
            else
                return JsonResultHelper.Error("Something went wrong,Please try again.");
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult CheckOut(TenantMaster tm)
        {
            string error=CheckOutValidation.Validate(tm);
            if (!string.IsNullOrEmpty(error))
                return JsonResultHelper.Error(error);

            tm.CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId);
            try
            {
                int id = _tenantBL.CheckOutStay(tm);
                if (id > 0)
                    return JsonResultHelper.Success("your checkout request has been submitted.");
                else
                    return JsonResultHelper.Error("Something went wrong, please try again.");
            }
            catch(Exception e)
            {
                return JsonResultHelper.Error(e.Message);
            }
        }


        [HttpGet]
        public ActionResult DownloadInvoice(int invoiceId)
        {
            PaymentBL _paymentBL = new PaymentBL();
            var data=_paymentBL.GetTenantInvoice(invoiceId,Validation.DecryptToInt(CurrentUser.User.UserId));            
            var invoiceHtml = RenderPartialToString(PartialView("_PaymentInvoice",data));

            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                StringReader sr = new StringReader(invoiceHtml);
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                string invoiceName = "Invoice-" + invoiceId+".pdf";
                return File(stream.ToArray(), "application/pdf",invoiceName);
            }
        }
    }
}