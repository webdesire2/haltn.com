﻿using System.Web.Mvc;
using FH.Web.Security;
using FH.App.Service;
using FH.App.Entity;
using FH.Validator;
using FH.Util;
using FH.App.Web.ViewModel;
using System;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Razorpay.Api;
using System.Collections.Generic;
using Newtonsoft.Json;
using Paytm;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Linq;

namespace FH.App_Web.Controllers
{
 
    public class SalesController : BaseController
    {
        private readonly TenantBL _tenantBL;
        private readonly UseCodeMasterBL _useCodeBL;
        private readonly LeadBL _leadBL;

        public SalesController()
        {
            _tenantBL = new TenantBL();
            _useCodeBL = new UseCodeMasterBL();
            _leadBL = new LeadBL();
            if (CurrentUser.User.Roles[0] == "Sales")
            {
                Redirect("Index");
            }
        }

        // GET: Sales
        public ActionResult Index()
        {
            return View();
        }


        //RSendPaymentLink
        //SSendPaymentLink

        [HttpPost]
        public ActionResult RSendPaymentLink(CollectCashData req)
        {
            if (CurrentUser.User.Roles[0] != "Sales")
            {
                return RedirectToAction("Index");
            }
            TenantMaster data = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = req.rentAmount,
                PaymentType = "RENT",
                TenantId = req.TenantId,
                RentForMonthStartDate = req.rentForMonthStartDate,
                RentForMonthEndDate = req.rentForMonthEndDate,
            };

            var result = _tenantBL.RSendPaymentLink(data);

            if (result != null)
            {
                TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), TenantId = req.TenantId };
                var tanentdata = _tenantBL.GetSingleTenant(de);


                var message = "Dear " + tanentdata.Name + ", %0aBill from Telonestay for Rs. " + req.rentAmount + " (Rent). Click2pay telonestay.com/user/Pbl/" + result.Id.ToString() + " to continue reliable services.";
                string OTPTEMP05 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP05"].ToString();
                SendSMS.Send(tanentdata.Mobile, message, OTPTEMP05);

                return JsonResultHelper.Success(result.Id.ToString());
            }
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }

        [HttpPost]
        public ActionResult SSendPaymentLink(CollectCashData req)
        {
            if (CurrentUser.User.Roles[0] != "Sales")
            {
                return RedirectToAction("Index");
            }
            TenantMaster data = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                Payment = req.rentAmount,
                PaymentType = "SECURITY",
                TenantId = req.TenantId,
                RentForMonthStartDate = DateTime.Now,
                RentForMonthEndDate = DateTime.Now,
            };

            var result = _tenantBL.SSendPaymentLink(data);

            if (result != null)
            {
                TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), TenantId = req.TenantId };
                var tanentdata = _tenantBL.GetSingleTenant(de);

                var message = "Dear " + tanentdata.Name + ", %0aBill from Telonestay for Rs. " + req.rentAmount + " (Security). Click2pay telonestay.com/user/Pbl/" + result.Id.ToString() + " to continue reliable services.";
                string OTPTEMP05 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP05"].ToString();
                SendSMS.Send(tanentdata.Mobile, message, OTPTEMP05);

                return JsonResultHelper.Success(result.Id.ToString());
            }
            else
            {
                return JsonResultHelper.Error("Some Error Occer. Please Try Again");
            }
        }
        //DownloadInvoice


        [HttpGet]
        public ActionResult GetTenantPaymentLog(int tenantId = 0)
        {
            if (CurrentUser.User.Roles[0] != "Sales")
            {
                return RedirectToAction("Index");
            }
            if (tenantId > 0)
            {
                TenantMaster de = new TenantMaster() { CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId), TenantId = tenantId };
                var lstLog = _tenantBL.GetTenantPaymentLog(de);



                //TanentNewPayment - This is Cash Collection Start
                var tenantData = _tenantBL.GetTanentStayDetails(de);

                if (tenantData != null)
                {
                    tenantData.SecurityAmountDue = tenantData.SecurityAmount - tenantData.SecurityDeposit;

                    if (tenantData.CheckInDate.Month == DateTime.Now.Month && tenantData.CheckInDate.Year == DateTime.Now.Year)
                    {
                        var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / 30 : 0;
                        var fromDate = tenantData.CheckInDate;

                        tenantData.FromDate = fromDate;
                        tenantData.ToDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));

                        int days = (30 - tenantData.FromDate.Day) + 1;
                        //int days = (tenantData.ToDate.Day - tenantData.FromDate.Day)+1;
                        tenantData.MonthlyRent = Math.Round(rentPerDay * days, 2);
                        if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                    }
                    else if (tenantData.CheckOutDate.Month == DateTime.Now.Month && tenantData.CheckOutDate.Year == DateTime.Now.Year)
                    {
                        var lastDay = new DateTime(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month, DateTime.DaysInMonth(tenantData.CheckOutDate.Year, tenantData.CheckOutDate.Month));
                        var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / lastDay.Day : 0;
                        var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        tenantData.FromDate = fromDate;
                        tenantData.ToDate = tenantData.CheckOutDate;
                        int days = tenantData.ToDate.Day;
                        tenantData.MonthlyRent = Math.Round(rentPerDay * days, 2);
                        if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                    }
                    else
                    {
                        var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        tenantData.FromDate = fromDate;
                        tenantData.ToDate = fromDate.AddMonths(1).AddDays(-1);
                    }

                    foreach (var item in lstLog)
                    {
                        if (item.RentForMonthEndDate == tenantData.ToDate && item.RentForMonthStartDate == tenantData.FromDate && item.DepositAmount == tenantData.MonthlyRent && item.PaymentStatus == "True")
                        {
                            tenantData.MonthlyRent = 0; break;
                        }
                    }

                }

                //TanentNewPayment - This is Cash Collection Start


                VMPaymentDetails result = new VMPaymentDetails();
                result.TenantPaymentHistory = lstLog;
                result.TanentStayDetails = tenantData;

                if (Request.Browser.IsMobileDevice)
                    return PartialView("_Tenant_Payment_Log_Mobile", result);
                else
                    return PartialView("_Tenant_Payment_Log_Desktop", result);
            }

            return JsonResultHelper.Error("Invalid input.");
        }

        [HttpGet]
        public ActionResult PropertyMap(PropertyMapVM req)
        {
            if (CurrentUser.User.Roles[0] != "Sales")
            {
                return RedirectToAction("Index");
            }
            TenantMaster de = new TenantMaster()
            {
                PropertyId = req.Pid,
                CheckInDate = DateTime.Now,
                CreatedBy = 3,
            };
            var result = _tenantBL.SgetAvailableRooms(de);
            var property = _tenantBL.getMyProperties(de);
            //var tenant = _tenantBL.GetAllTenant(de);
            var ddd = result.FirstOrDefault();
            req.Pid = ddd.PropertyId;
            req.PropertyList = property;
            req.PGRoomData = result;

            return View(req);
        }

        [HttpGet]
        public ActionResult occupancyDetails(PropertyMapVM req)
        {
            if (CurrentUser.User.Roles[0] != "Sales")
            {
                return RedirectToAction("Index");
            }
            TenantMaster de = new TenantMaster()
            {
                PropertyId = req.Pid,
                CheckInDate = DateTime.Now,
                CreatedBy = 3,
            };
            var result = _tenantBL.occupancyDetails(de);
            //var tenant = _tenantBL.GetAllTenant(de);
            req.PGRoomData = result;

            return View(req);
        }

        [HttpGet]
        public ActionResult AllLeadList(GetAllLeadModel req)
        {
            if (CurrentUser.User.Roles[0] != "Sales")
            {
                return RedirectToAction("Index");
            }
            LeadDE de = new LeadDE()
            {
                PropertyIds = req.PropertyId,
                Latitude = req.Lat,
                Longitude = req.Lng,
                Gender = req.gender,
                LeadId = req.LeadId,
                PropertyType = "PG",
                LeadSource = req.LeadSource
            };

            var data = _leadBL.GetAllPGLead(de);
            return View(data);
        }

        [HttpGet]
        public ActionResult MyTanent(TenantMaster req)
        {
            if (CurrentUser.User.Roles[0] != "Sales")
            {
                return RedirectToAction("Index");
            }

            ViewBag.RoomNo = req.RoomNo;
            ViewBag.TenantStatus = req.TenantStatus;
            ViewBag.RentPaid = req.RentPaid;

            if (req.TenantStatus == 0)
            {
                ViewBag.TenantStatus = 1;
                ViewBag.RentPaid = 2;
                req.TenantStatus = 1;
                req.RentPaid = 2;
            }


            TenantMaster de = new TenantMaster()
            {
                CreatedBy = Validation.DecryptToInt(CurrentUser.User.UserId),
                TenantStatus = req.TenantStatus,
                RentPaid = req.RentPaid,
                RoomNo = req.RoomNo,
                Mobile = req.Mobile,
                IsActive = true,
            };

            var tenantData = _tenantBL.SGetMyTanent(de);
            var mytanent = _tenantBL.SGetRentPaidTanent(de);

            foreach (var singleData in tenantData)
            {
                singleData.RentPaid = 0;
                foreach (var singletanent in mytanent)
                {
                    if (singletanent.TenantId == singleData.TenantId)
                    {
                        singleData.RentPaid = 1;
                    }
                }
            }

            return View(tenantData);
        }
    }
}