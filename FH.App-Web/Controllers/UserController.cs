﻿using System.Web.Mvc;
using FH.App.Entity;
using FH.Validator;
using FH.App.Service;
using FH.Web.Security;
using FH.Util;
using Newtonsoft.Json;
using System.Web.Security;
using System;
using System.Web;
using System.Linq;
using FH.App.Web.ViewModel;
using static System.Net.WebRequestMethods;

namespace FH.App_Web.Controllers
{
    public class UserController : Controller
    {
        private readonly TenantBL _tenantBL;
        private readonly UseCodeMasterBL _useCodeBL;
        public UserController()
        {
            _tenantBL = new TenantBL();
            _useCodeBL = new UseCodeMasterBL();
        }
        [HttpGet]
        public ActionResult Registration()
        {          
            return View(new UserDetail());
        }
        
        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult RegistrationAjax(UserDetail user)
        {
            if (string.IsNullOrWhiteSpace(user.Name))
                return ValidationError("Name required.");
            else if (user.Name.Length > 50)
                return ValidationError("Name can't be greater than 50 characters.");

            if (string.IsNullOrWhiteSpace(user.Mobile))
                return ValidationError("Mobile no required.");
            else if (Validation.MobileNo(user.Mobile))
                return ValidationError("Invalid mobile no.");

            if (string.IsNullOrWhiteSpace(user.Email))
                return ValidationError("Email required.");
            else if (Validation.EmailAddress(user.Email))
                return ValidationError("Invalid email.");
            else if (user.Email.Length > 50)
                return ValidationError("Email can't be greater than 50 characters.");

            if (string.IsNullOrWhiteSpace(user.Password))
                return ValidationError("Password required.");
            else if (user.Password.Length < 5)
                return ValidationError("Password must be minimum 5 characters.");
            else if (user.Password.Length > 50)
                return ValidationError("Password can't be greater than 50 characters.");

            if (string.IsNullOrWhiteSpace(user.UserType))
                return ValidationError("User type required.");
            else if (user.UserType != "Buyer" && user.UserType != "Owner" && user.UserType != "Agent")
                return ValidationError("Invalid user type.");

            using (var unitOfWork = new UnitOfWork())
            {
                UserDetail userDetail = unitOfWork.UserBL.FindByEmailMobile(user);
                if (userDetail != null)
                {
                    if (userDetail.Mobile == user.Mobile && userDetail.Email == user.Email)
                        return ValidationError("Mobile no & email id already exist.");
                    else if (userDetail.Mobile == user.Mobile)
                        return ValidationError("mobile no already exist.");
                    else
                        return ValidationError("email id already exist.");
                }
                else
                {
                    Random rdmNo = new Random();
                    int OTP = Convert.ToInt16(rdmNo.Next(1000, 9999).ToString("D4"));
                    user.OTP = OTP;
                    user.Password = EncreptDecrpt.Encrypt(user.Password);
                    user.OTPExpiredOn = DateTime.Now.AddHours(2).ToString("yyyy-MM-dd HH:mm:ss");
                    int userId = unitOfWork.UserBL.AddUser(user);
                    if (userId > 0)
                    {                        
                            unitOfWork.Commit();
                        string OTPTEMP01 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP01"].ToString();

                        SendSMS.Send(user.Mobile, "Dear User, Your OTP for Login Haltn is " + OTP + ". Please do not share it. With Regards Haltn", OTPTEMP01);
                        //SendSMS.Send(user.Mobile, "Dear user, %0aYour OTP for Telonestay is " + OTP + ". Please do not share it. %0aWith Regards %0aTelonestay ", OTPTEMP01);
                        return Json(new { Success = true,User=EncreptDecrpt.Encrypt(userId.ToString())});                        
                    }
                    return ValidationError("Something went wrong, please try again.");
                }

            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult NewUserRegistrationAjax(UserDetail user)
        {
            // this is used for create new user while schedule visit
            if (string.IsNullOrWhiteSpace(user.Name))
                return ValidationError("Name required.");
            else if (user.Name.Length > 50)
                return ValidationError("Name can't be greater than 50 characters.");

            if (string.IsNullOrWhiteSpace(user.Mobile))
                return ValidationError("Mobile no required.");
            else if (Validation.MobileNo(user.Mobile))
                return ValidationError("Invalid mobile no.");

            if (string.IsNullOrWhiteSpace(user.Email))
                return ValidationError("Email required.");
            else if (Validation.EmailAddress(user.Email))
                return ValidationError("Invalid email.");
            else if (user.Email.Length > 50)
                return ValidationError("Email can't be greater than 50 characters.");
          
            using (var unitOfWork = new UnitOfWork())
            {
                user.UserType = "Buyer";

                UserDetail userDetail = unitOfWork.UserBL.FindByEmailMobile(user);
                if (userDetail != null)
                {
                    if (userDetail.Mobile == user.Mobile && userDetail.Email == user.Email)
                        return ValidationError("Mobile no & email id already exist.");
                    else if (userDetail.Mobile == user.Mobile)
                        return ValidationError("mobile no already exist.");
                    else
                        return ValidationError("email id already exist.");
                }
                else
                {
                    Random rdmNo = new Random();
                    int OTP = Convert.ToInt16(rdmNo.Next(1000, 9999).ToString("D4"));
                    user.OTP = OTP;                    
                    user.OTPExpiredOn = DateTime.Now.AddHours(2).ToString("yyyy-MM-dd HH:mm:ss");
                    int userId = unitOfWork.UserBL.AddUser(user);
                    if (userId > 0)
                    {
                        unitOfWork.Commit();
                        string OTPTEMP01 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP01"].ToString();
                        SendSMS.Send(user.Mobile, "Dear User, Your OTP for Login Haltn is " + OTP + ". Please do not share it. With Regards Haltn", OTPTEMP01);
                        //SendSMS.Send(user.Mobile, "Dear user, %0aYour OTP for Telonestay is " + OTP + ". Please do not share it. %0aWith Regards %0aTelonestay ", OTPTEMP01);
                        return Json(new { Success = true, User = EncreptDecrpt.Encrypt(userId.ToString()) });
                    }
                    return ValidationError("Something went wrong, please try again.");
                }

            }
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult VerifyOTP(UserDetail user)
        {
            if (user.OTP == null || user.OTP < 999 || user.OTP > 9999)
                return ValidationError("4 digit OTP required.");

            int userId = Validation.DecryptToInt(user.UserId);
            if (userId<=0)
                return ValidationError("Something went wrong,Please try again.");

            using (var uow = new UnitOfWork())
            {
                UserDetail userDetail = uow.UserBL.IsValidOTP(userId, Convert.ToInt16(user.OTP), DateTime.Now);
                if (userDetail != null && !string.IsNullOrEmpty(userDetail.UserId))
                {
                    uow.Commit();
                    if (!string.IsNullOrEmpty(userDetail.Message))
                        return Json(new { Success = false, ErrorMessage = userDetail.Message });
                    else
                    {
                        AuthCookie.AddAuthCookie(userDetail, Request, Response);
                     
                        return Json(new { Success = true });
                    }
                }
                else
                    return Json(new { Success = false, ErrorMessage = "Something went wrong,please try again." });
            }
        }
        
        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult LoginAjax(UserDetail entity)
        {
            if (string.IsNullOrWhiteSpace(entity.Mobile))
                return ValidationError("Mobile no required.");
             else if(Validation.MobileNo(entity.Mobile))
                return ValidationError("Invalid mobile no.");

            if (string.IsNullOrWhiteSpace(entity.Password))
                return ValidationError("Password required.");

            using (var unitOfWork = new UnitOfWork())
            {
                entity.Password = EncreptDecrpt.Encrypt(entity.Password);

                UserDetail userDetail = unitOfWork.UserBL.IsValidLogin(entity);
                if(userDetail != null && !string.IsNullOrEmpty(userDetail.UserId))
                {
                    AuthCookie.AddAuthCookie(userDetail, Request, Response);                    
                    return Json(new { Success = true, userDetail = userDetail });
                }
                else
                    return ValidationError("Invalid Username & Password.");
            }            
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult IsMobileExist(string mobile)
        {
            if (string.IsNullOrWhiteSpace(mobile))
                return ValidationError("Mobile no required.");
            else if(Validation.MobileNo(mobile))
                return ValidationError("Invalid mobile no.");

            using (var uow = new UnitOfWork())
            {
                UserDetail user = new UserDetail();
                Random rdmNo = new Random();
                user.OTP = Convert.ToInt16(rdmNo.Next(1000, 9999).ToString("D4"));
                user.OTPExpiredOn = DateTime.Now.AddHours(2).ToString("yyyy-MM-dd HH:mm:ss");
                user.Mobile = mobile;
                string userId = uow.UserBL.GetUserByMobile(user);
                if (!string.IsNullOrEmpty(userId) && Convert.ToInt32(userId) > 0)
                { 
                    uow.Commit();
                    string OTPTEMP01 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP01"].ToString();
                    SendSMS.Send(user.Mobile, "Dear User, Your OTP for Login Haltn is " + user.OTP + ". Please do not share it. With Regards Haltn", OTPTEMP01);
                    //SendSMS.Send(user.Mobile, "Dear user, %0aYour OTP for Telonestay is " + user.OTP + ". Please do not share it. %0aWith Regards %0aTelonestay ", OTPTEMP01);
                    return Json(new { Success = true, User = EncreptDecrpt.Encrypt(userId.ToString()) });
                }
                else
                    return Json(new { Success = false, ErrorMessage = "Mobile no not exist." });
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);

            return RedirectToAction("index","home");
        }

        
        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult PaySecurity(TenantPaymentDetail pd)
        {
   
            var tenant = _tenantBL.GetTenantMaster(new TenantMaster() { CreatedBy = pd.UserId });

            if (tenant == null)
                return JsonResultHelper.Error("you are not registered as tenant.");

            var amountdue = tenant.SecurityAmount - tenant.SecurityDeposit;
            if (!(pd.DepositAmount <= amountdue)) {
                pd.DepositAmount = amountdue;
            }   
           
             
            TenantPaymentDetail tpd = new TenantPaymentDetail()
            {
                PaymentType = "SECURITY",
                DepositAmount = pd.DepositAmount
            };

            TenantMaster de = new TenantMaster()
            {
                CreatedBy = pd.UserId,
                TenantPaymentDetail = tpd
            };

            int paymentId = _tenantBL.AddNewPayment(de);

            if (paymentId > 0)
            {
                PaymentBL _paymentBL = new PaymentBL();

                string CCAccessCode = System.Configuration.ConfigurationManager.AppSettings["CCAccessCode"].ToString();
                string txnToken = _paymentBL.EPPaymentRequest(pd.DepositAmount, paymentId, tenant);
                VMPayment orderData = new VMPayment();
                orderData.paymentID = paymentId.ToString();
                orderData.Amount = pd.DepositAmount;
                orderData.PaytmMID = CCAccessCode;
                orderData.currency = "INR";
                orderData.name = tenant.Name;
                orderData.email = tenant.Email;
                orderData.contactNumber = tenant.Mobile;
                orderData.address = "";
                orderData.description = "";
                orderData.txnToken = txnToken;

                return PartialView("Rpayment", orderData);
            } 
            else
                return JsonResultHelper.Error("Something went wrong,Please try again.");
        }

        [HttpPost]
        [AjaxRequestValidation]
        public ActionResult PayRent(TenantPaymentDetail pd, UseCodeMaster cm)
        {
            PaymentBL payBL = new PaymentBL();
            double couponAmount = 0;
            string couponCode = null;

            string error = string.Empty;  // PayRentValidation.Validate(pd, cm);

            if (string.IsNullOrWhiteSpace(error))
            {
                TenantMaster tde = new TenantMaster()
                {
                    CreatedBy = pd.UserId
                };

                var tenantDetail = _tenantBL.GetTenantMaster(tde);

                if (tenantDetail == null)
                    return JsonResultHelper.Error("you are not registered as tenant.");

                var checkOutDate = DateTime.ParseExact("0001-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); //DateTime('01/01/0001 00:00:00');
                if (tenantDetail.CheckOutDate != null)
                {
                    checkOutDate = (DateTime)tenantDetail.CheckOutDate;
                }
                if (tenantDetail.CheckInDate.Month == DateTime.Now.Month && tenantDetail.CheckInDate.Year == DateTime.Now.Year)
                {
                    var rentPerDay = tenantDetail.MonthlyRent > 0 ? tenantDetail.MonthlyRent / 30 : 0;
                    var fromDate = tenantDetail.CheckInDate;
                    pd.RentForMonthStartDate = fromDate;
                    pd.RentForMonthEndDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));
                    int days = (30 - Convert.ToDateTime(pd.RentForMonthStartDate).Day) + 1;
                    //int days = (Convert.ToDateTime(pd.RentForMonthEndDate).Day - Convert.ToDateTime(pd.RentForMonthStartDate).Day)+1;
                    pd.DepositAmount = Math.Round(rentPerDay * days,2);
                    if (pd.DepositAmount < 1) { pd.DepositAmount = 1; }
                }
                else if (checkOutDate.Month == DateTime.Now.Month && checkOutDate.Year == DateTime.Now.Year)
                {
                    var lastDay = new DateTime(checkOutDate.Year, checkOutDate.Month, DateTime.DaysInMonth(checkOutDate.Year, checkOutDate.Month));
                    var rentPerDay = tenantDetail.MonthlyRent > 0 ? tenantDetail.MonthlyRent / lastDay.Day : 0;
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    pd.RentForMonthStartDate = fromDate;
                    pd.RentForMonthEndDate = checkOutDate;
                    int days = checkOutDate.Day;
                    tenantDetail.MonthlyRent = Math.Round(rentPerDay * days, 2);
                    pd.DepositAmount = Math.Round(rentPerDay * days, 2);
                    if (pd.DepositAmount < 1) { pd.DepositAmount = 1; }
                }
                else
                {
                    var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    pd.RentForMonthStartDate = fromDate;
                    pd.RentForMonthEndDate = fromDate.AddMonths(1).AddDays(-1);
                    pd.DepositAmount = tenantDetail.MonthlyRent;
                }

                //  else if (tenantDetail != null && tenantDetail.MonthlyRent != pd.DepositAmount)
                //    return JsonResultHelper.Error(string.Format("{0}{1}","Amount should be ",tenantDetail.MonthlyRent));

                if (!string.IsNullOrWhiteSpace(cm.Code))
                {
                    var useCode = _useCodeBL.GetByCode(cm);
                    if (useCode != null && useCode.UseCodeId > 0)
                    {
                        couponCode = useCode.Code;
                        couponAmount = Math.Round((pd.DepositAmount * useCode.UseCodeTypePercentage) / 100, 2);
                        couponAmount = couponAmount > 10000 ? 10000 : couponAmount;
                    }
                    else
                        return JsonResultHelper.Error("Invalid voucher code.");
                }

                TenantPaymentDetail tpd = new TenantPaymentDetail()
                {
                    PaymentType = "RENT",
                    DepositAmount = pd.DepositAmount,
                    RentForMonthStartDate = pd.RentForMonthStartDate,
                    RentForMonthEndDate = pd.RentForMonthEndDate
                };

                PaymentDetail payment = new PaymentDetail()
                {
                    UseCode = couponCode,
                    UseCodeAmount = couponAmount
                };

                TenantMaster de = new TenantMaster()
                {
                    CreatedBy = pd.UserId,
                    TenantPaymentDetail = tpd, 
                    PaymentDetail = payment
                };

                int paymentId = _tenantBL.AddNewPayment(de);
                if (paymentId > 0)
                {
                    string CCAccessCode = System.Configuration.ConfigurationManager.AppSettings["CCAccessCode"].ToString();
                    string txnToken = payBL.EPPaymentRequest(pd.DepositAmount, paymentId, tenantDetail);
                    VMPayment orderData = new VMPayment();
                    orderData.paymentID = paymentId.ToString();
                    orderData.Amount = pd.DepositAmount;
                    orderData.PaytmMID = CCAccessCode;
                    orderData.currency = "INR";
                    orderData.name = tenantDetail.Name;
                    orderData.email = tenantDetail.Email;
                    orderData.contactNumber = tenantDetail.Mobile;
                    orderData.address = "";
                    orderData.description = "";
                    orderData.txnToken = txnToken;

                    return PartialView("Rpayment", orderData);
                }
                else
                    return JsonResultHelper.Error("Something went wrong.Please try again.");
            }
            else
                return JsonResultHelper.Error(error);
        }



        [HttpGet]
        public ActionResult Pbl(int pid)
        {
            int paymentlinkI = pid;
            if(paymentlinkI > 0)
            { 
                TenantMaster tde = new TenantMaster()
                {
                    CreatedBy = paymentlinkI
                };

                var PaymentLinkDetail = _tenantBL.GetPaymentLinkDetail(tde);

                if (PaymentLinkDetail.TenantId > 0)
                {
                    TenantMaster tanentInfo = new TenantMaster()
                    {
                        CreatedBy = PaymentLinkDetail.UserId
                    }; 
                    var tenantData = _tenantBL.GetMyCurrentStay(tanentInfo);
                    if (tenantData != null)
                    {
                        var checkOutDate = DateTime.ParseExact("0001-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture); //DateTime('01/01/0001 00:00:00');
                        if (tenantData.CheckOutDate != null)
                        {
                            checkOutDate = (DateTime)tenantData.CheckOutDate;
                        }
                        // tenantData.Name = PaymentLinkDetail.Name;
                        tenantData.SecurityAmountDue = tenantData.SecurityAmount - tenantData.SecurityDeposit;
                        if (PaymentLinkDetail.DepositAmount <= tenantData.SecurityAmountDue && PaymentLinkDetail.PaymentType == "SECURITY") {
                            tenantData.SecurityAmountDue = PaymentLinkDetail.DepositAmount;
                        } 
                        tenantData.UserId = PaymentLinkDetail.UserId;
                        if (tenantData.CheckInDate.Month == DateTime.Now.Month && tenantData.CheckInDate.Year == DateTime.Now.Year)
                        {
                            var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / 30 : 0;
                            var fromDate = tenantData.CheckInDate;
                            tenantData.FromDate = fromDate;
                            tenantData.ToDate = new DateTime(fromDate.Year, fromDate.Month, DateTime.DaysInMonth(fromDate.Year, fromDate.Month));

                            int days = (30 - tenantData.FromDate.Day) + 1;
                            //int days = (tenantData.ToDate.Day - tenantData.FromDate.Day)+1;
                            tenantData.MonthlyRent = Math.Round(rentPerDay * days, 2);
                            if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                        } else if (checkOutDate.Month == DateTime.Now.Month && checkOutDate.Year == DateTime.Now.Year) {
                            var lastDay = new DateTime(checkOutDate.Year, checkOutDate.Month, DateTime.DaysInMonth(checkOutDate.Year, checkOutDate.Month));
                            var rentPerDay = tenantData.MonthlyRent > 0 ? tenantData.MonthlyRent / lastDay.Day : 0;
                            var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                            tenantData.FromDate = fromDate;
                            tenantData.ToDate = checkOutDate;
                            int days = checkOutDate.Day;
                            tenantData.MonthlyRent = Math.Round(rentPerDay * days, 2);
                            if (tenantData.MonthlyRent < 1) { tenantData.MonthlyRent = 1; }
                        } else  {
                            var fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                            tenantData.FromDate = fromDate;
                            tenantData.ToDate = fromDate.AddMonths(1).AddDays(-1);
                        }
                        tenantData.PaymentType = PaymentLinkDetail.PaymentType;

                        if (PaymentLinkDetail.PaymentType == "RENT")
                        { 
                            var data = _tenantBL.GetAllPayment(tanentInfo);
                            //MyOrderDTO
                            foreach (var item in data)
                            {
                                if (item.RentForMonthEndDate == tenantData.ToDate && item.RentForMonthStartDate == tenantData.FromDate && item.Amount == tenantData.MonthlyRent && item.PaymentStatus == "SUCCESS")
                                {
                                    tenantData.MonthlyRent = 0;
                                    break;
                                }

                            }
                        }

                    }
                    return View(tenantData);
                }
                else
                {
                    return RedirectToAction("Login", new { msg = "Some Error Occer" });
                }
            }else
            {
                return RedirectToAction("Login", new { msg = "Some Error Occer" });
            }
        }

        [HttpGet]
        public ActionResult ResetPassword(UserDetail user)
        {
            if(!string.IsNullOrEmpty(user.Mobile) && Validation.MobileNo(user.Mobile)==false)
            {
                using (var uow = new UnitOfWork())
                {
                    Random rdmNo = new Random();
                    user.OTP = Convert.ToInt16(rdmNo.Next(1000, 9999).ToString("D4"));
                    user.OTPExpiredOn = DateTime.Now.AddHours(2).ToString("yyyy-MM-dd HH:mm:ss");

                    string userId=uow.UserBL.GetUserByMobile(user);

                    if(!string.IsNullOrEmpty(userId) && Convert.ToInt32(userId)>0)
                    {
                        uow.Commit(); 

                        string OTPTEMP01 = System.Configuration.ConfigurationManager.AppSettings["OTPTEMP01"].ToString();
                        SendSMS.Send(user.Mobile, "Dear User, Your OTP for Login Haltn is " + user.OTP + ". Please do not share it. With Regards Haltn", OTPTEMP01);
                        //SendSMS.Send(user.Mobile, "Dear user, %0aYour OTP for Telonestay is " + user.OTP + ". Please do not share it. %0aWith Regards %0aTelonestay ", OTPTEMP01);
                        UserDetail userDetail = new UserDetail()
                        { 
                            UserId = EncreptDecrpt.Encrypt(userId),
                            Mobile = user.Mobile
                        };

                        return View(userDetail);
                    }
                    else
                    {
                        return RedirectToAction("Login1", new { msg = "Mobile no doesn't exist.",mobile=user.Mobile });
                    }                    
                }
            }

            return RedirectToAction("Login",new {msg="Incorrect mobile no." });
        }

        [HttpPost]
        public ActionResult ResetPassword(string mobile, string password, string confirmPassword, int otp)
        {
            bool isValidModal = true;

            if (Validation.RequiredField(mobile))
            { TempData["mobile"] = ValidationMessage.required; isValidModal = false; }
            else if (Validation.MobileNo(mobile))
            { TempData["mobile"] = ValidationMessage.invalidMobileNo; isValidModal = false; }

            if (Validation.RequiredField(password))
            { TempData["password"] = ValidationMessage.required; isValidModal = false; }
            else if (Validation.MaxLength(password, 50))
            { TempData["password"] = "max 50 characters allowed."; isValidModal = false; }
            else if (Validation.MinLength(password, 5))
            { TempData["password"] = "manimum 5 characters required."; isValidModal = false; }
            else if (password != confirmPassword)
            { TempData["confirmPassword"] = "confirm password doesn't match."; isValidModal = false; }

            if (otp.ToString().Length != 4)
            { TempData["otp"] = "invalid OTP."; isValidModal = false; }

            UserDetail agent = new UserDetail();
            agent.Mobile = mobile;
            agent.Password = EncreptDecrpt.Encrypt(password);
            agent.OTP = otp;         

            if (isValidModal)
            {
                agent.OTPExpiredOn = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                using (UnitOfWork uow = new UnitOfWork())
                {
                    UserDetail userDetail = uow.UserBL.ResetPassword(agent);
                    uow.Commit();
                    if (userDetail!=null && !string.IsNullOrEmpty(userDetail.UserId) && Convert.ToInt16(userDetail.UserId) > 0)
                        return RedirectToAction("login", "user", new { msg = "prs" });
                    else
                    {
                        TempData["error"] = "invalid OTP";
                        return RedirectToAction("ResetPassword", new { mobile= mobile });
                    }
                }
            }

            agent.Password = "";
            return View(agent);
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult ResetPasswordAjax(UserDetail user)
        {
            int userId =Validation.DecryptToInt(user.UserId);
            if (userId <= 0)
                return ValidationError("Invalid input");

            if (string.IsNullOrWhiteSpace(user.Password))
                return ValidationError("Password required.");
            else if(user.Password.Length>50)
                return ValidationError("Password can't be greater than 50 characters.");
            else if(user.Password.Length<5)
                return ValidationError("Password can't be less than 5 characters.");

            if(string.IsNullOrWhiteSpace(user.ConfirmPassword))
                return ValidationError("Confirm password required.");
            else if(!user.Password.Equals(user.ConfirmPassword))
                return ValidationError("Confirm password not match.");

            if (user.OTP == null || user.OTP < 999 || user.OTP > 9999)
                return ValidationError("4 digit OTP required.");
            
            using (UnitOfWork uow = new UnitOfWork())
            {
                user.OTPExpiredOn = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                user.UserId = userId.ToString();
                user.Password = EncreptDecrpt.Encrypt(user.Password);
                UserDetail userDetail = uow.UserBL.ResetPassword(user);                
                if (userDetail != null && !string.IsNullOrEmpty(userDetail.UserId))
                {
                    uow.Commit();

                    if (!string.IsNullOrEmpty(userDetail.Message))
                        return Json(new { Success = false, ErrorMessage = userDetail.Message });
                    else
                    {
                        AuthCookie.AddAuthCookie(userDetail, Request, Response);
                        return Json(new { Success = true }); }
                }
                else
                    return Json(new { Success = false, ErrorMessage = "Something went wrong,please try again." });
            }

        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult MyProfile()
        {
            using (var uow = new UnitOfWork())
            {
               return View(uow.UserBL.GetUserByUserId(Validation.DecryptToInt(CurrentUser.User.UserId)));
            }            
        }

        [HttpPost]
        [AjaxRequestValidation]
        public JsonResult UpdateProfile(UserDetail input)
        {
            
            if (string.IsNullOrWhiteSpace(input.Name))
                return ValidationError("Name required.");
            else if (input.Name.Length > 50)
                return ValidationError("Name can't be more than 50 characters.");
            else if (input.Name.Any(char.IsDigit))
                return ValidationError("Name shouldn't contain numeric value.");

            if(string.IsNullOrWhiteSpace(input.Email))
                return ValidationError("Email required.");
            else if(Validation.EmailAddress(input.Email))
                return ValidationError("Invalid email.");

            int userId = Validation.DecryptToInt(CurrentUser.User.UserId);
            if (userId>0)
            {
                input.UserId = userId.ToString();
                using (var uow = new UnitOfWork())
                {
                    uow.UserBL.UpdateUser(input);
                    uow.Commit();
                    return Json(new {Success=true});
                }
            }
            return Json(new { Success = false,ErrorMessage="Something went wrong,Please try again." });
        }

        private JsonResult ValidationError(string message)
        {
            return Json(new { Suceess = false,ErrorMessage=message});
        }
        
    }
}
