﻿using System;
using System.Configuration;

namespace FH.App_Web
{
    public class WebConfigHelper
    {
        public static string LogoUrl = ConfigurationManager.AppSettings["LogoUrl"];
        public static string WebsiteFavIcon = ConfigurationManager.AppSettings["FavIconUrl"];
        public static string WebsiteName= ConfigurationManager.AppSettings["WebsiteName"];
        public static string CompanyPhone= ConfigurationManager.AppSettings["CompanyPhone"];
        public static string ShoppingSiteUrl = ConfigurationManager.AppSettings["ShoppingSiteUrl"];
        public static string WebUrl= ConfigurationManager.AppSettings["WebUrl"];
        public static int WalletDiscountPerc =Convert.ToInt32(ConfigurationManager.AppSettings["AddWalletDiscountPerc"]); 
        public static string WalletDiscountMsg = ConfigurationManager.AppSettings["AddWalletDiscountMsg"];
    }
}