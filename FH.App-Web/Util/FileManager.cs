﻿using System.Configuration;
namespace FH.Util
{
    public class FileManager
    {
        public static string getFileFullPath(string file)
        {
            string isProdMode = ConfigurationManager.AppSettings["IsProdMode"].ToString();
            string galleryPathAgentFolderProd = ConfigurationManager.AppSettings["GalleryForlderPath"].ToString();
            string amazonBucketProd= ConfigurationManager.AppSettings["AmazonBucketPathProd"].ToString();
            string amazonBucketDev = ConfigurationManager.AppSettings["AmazonBucketPathDev"].ToString();
            if (isProdMode == "1")
            {
                if (file.Contains("/Uploads/"))
                    return galleryPathAgentFolderProd + file;
                else
                    return amazonBucketProd + file;
            }
            else
                return amazonBucketDev + file;
        }
    }
}