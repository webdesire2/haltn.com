﻿using System.Text.RegularExpressions;

namespace FH.App_Web
{
    public static class Common
    {
        public static string ReplaceSpace(this string input)
        {
            try
            {
                if (!string.IsNullOrEmpty(input))
                {
                    string output = input.Replace(","," ");
                    output = output.Replace("+", "plus");
                    output = output.Replace("/", "-");                    
                    output = output.Replace(".", "-");
                    output =Regex.Replace(output, @"\s+"," ");
                    return output.Replace(" ", "-");
                }
            }
            catch
            {
                return input;

            }
            return input;            
        }
    }
}