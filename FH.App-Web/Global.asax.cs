﻿using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using FH.App.Entity;
using FH.Web.Security;
using System.Web.Optimization;
using System.Web.Http;

namespace FH.App_Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiRouteConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            MvcHandler.DisableMvcResponseHeader = true;
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];            
            if (authCookie != null)
            {

                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                UserDetail serializeModel = JsonConvert.DeserializeObject<UserDetail>(authTicket.UserData);
                CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                newUser.UserId = serializeModel.UserId.ToString();
                newUser.Name = serializeModel.Name;
                newUser.Email = serializeModel.Email;
                newUser.Mobile = serializeModel.Mobile;
                newUser.UserType = serializeModel.UserType;
                newUser.Roles = new string[1] { serializeModel.UserType};

                HttpContext.Current.User = newUser;
            }
        }
    }
}
