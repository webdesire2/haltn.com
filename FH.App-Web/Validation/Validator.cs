﻿using System;
using System.Text.RegularExpressions;

namespace FH.Validator
{
    public class Validation
    {
        public static bool RequiredField(string propertyValue)
        {
            if (string.IsNullOrEmpty(propertyValue))
                return true;
            else
                return false;
        }

        public static bool EmailAddress(string propertyValue)
        {            
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(propertyValue);
            if (match.Success)
                return false;
            else
                return true;
        }

        public static bool MobileNo(string propertyValue)
        {
            Regex regex = new Regex(@"^[0-9]{10}$");
            Match match = regex.Match(propertyValue);
            if (match.Success)
                return false;
            else
                return true;

        }

        public static bool IsInt(object propertyValue)
        {
            int value;            
            if (int.TryParse(propertyValue.ToString(), out value))
                return true;
            else
                return false;
        }

        public static bool MaxLength(string propertyValue, int maxLenght)
        {
            if (string.IsNullOrEmpty(propertyValue))
                return false;
            if (propertyValue.Length > maxLenght)
                return true;
            else
                return false;
        }

        public static bool MinLength(string propertyValue, int minLenght)
        {
            if (propertyValue.Length <= minLenght)
                return true;
            else
                return false;
        }

        public static  bool IsDatetime(string datetime)
        {
            DateTime output;
            if (DateTime.TryParse(datetime, out output))
                return true;
            else
                return false;
        }

        public static int DecryptToInt(string input)
        {
            if (string.IsNullOrEmpty(input))
                return 0;
            else
            {
                string decInput = FH.Web.Security.EncreptDecrpt.Decrypt(input);
                if (string.IsNullOrEmpty(decInput))
                    return 0;
                else if(Convert.ToInt32(decInput) > 0)
                    return Convert.ToInt32(decInput);
                else
                    return 0;
            }
        }
    }
}