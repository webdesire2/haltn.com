﻿using FH.App.Entity;
using System;

namespace FH.Validator
{
    public class PayRentValidation
    {
        public static string Validate(TenantPaymentDetail pd,UseCodeMaster cm)
        {   
                if (pd.DepositAmount <= 0)
                return "Rent amount required.";
            if (pd.RentForMonthStartDate == null)
                return "Rent month from date required.";
            if (pd.RentForMonthEndDate == null)
                return "Rent month to date reqquired.";

            if (pd.RentForMonthStartDate >= pd.RentForMonthEndDate)
                return "To date should be bigger than from date.";

            if (Convert.ToDateTime(pd.RentForMonthStartDate).AddDays(31) < pd.RentForMonthEndDate)
                return "To date more than 1 month.";
            return string.Empty;
        }
    }

    public class PaySecurityValidation
    {
        public static string Validate(TenantPaymentDetail pd)
        {
            if (pd.DepositAmount <= 0)
                return "Security amount required.";

            return string.Empty;
        }
    }

    public class CheckOutValidation
    {
        public static string Validate(TenantMaster tm)
        {

            if (tm.CheckOutDate ==null)
                return "check out date required.";
            if (tm.CheckOutDate < DateTime.Now.AddDays(30)) {
                return "Minimum 1 Month Notice Period Will be thier.";
            }

            return string.Empty;
        }
    }
}