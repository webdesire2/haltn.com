﻿using System.Collections.Generic;

namespace FH.App_Web
{
    public class FurnishingDropdownValues
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstValues = new Dictionary<string, string>();
            lstValues.Add("1", "1");
            lstValues.Add("2", "2");
            lstValues.Add("3", "3");
            lstValues.Add("3+", "3+");            
            return lstValues;
        }

    }
}