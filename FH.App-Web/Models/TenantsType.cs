﻿using System.Collections.Generic;

namespace FH.App_Web
{
    public class TenantType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lsttype = new Dictionary<string, string>();
            lsttype.Add("Family", "Family");
            lsttype.Add("Bachelors", "Bachelors");
            lsttype.Add("Company", "Company");
            lsttype.Add("All", "All");
            return lsttype;
        }
    }
}