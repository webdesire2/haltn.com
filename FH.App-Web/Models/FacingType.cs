﻿using System.Collections.Generic;

namespace FH.App_Web
{
    public class FacingType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstFacing = new Dictionary<string, string>();
            lstFacing.Add("North","North");
            lstFacing.Add("South","South");
            lstFacing.Add("East","East");
            lstFacing.Add("West","West");
            lstFacing.Add("North-East", "North-East");
            lstFacing.Add("South-East", "South-East");
            lstFacing.Add("North-West", "North-West");
            lstFacing.Add("South-West", "South-West");
                                    
            return lstFacing;
        }
    }
}