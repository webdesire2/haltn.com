﻿using System.Collections.Generic;

namespace FH.App_Web
{
    public class WaterSupplyType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstWaterSupply = new Dictionary<string, string>();
            lstWaterSupply.Add("COR","Corporation");
            lstWaterSupply.Add("BOR","Borewell");
            lstWaterSupply.Add("BOT","Both");
            
            return lstWaterSupply;
        }
    }
}