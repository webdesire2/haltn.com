﻿using System.Collections.Generic;

namespace FH.App_Web
{
    public class BHKType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstBHK = new Dictionary<string, string>();
            lstBHK.Add("1BHK","1 BHK");
            lstBHK.Add("2BHK","2 BHK");
            lstBHK.Add("3BHK","3 BHK");
            lstBHK.Add("4BHK","4 BHK");
            lstBHK.Add("4+BHK","4+ BHK");
            return lstBHK;
        }
    }
}