﻿
using System.Collections.Generic;

namespace FH.App_Web
{
    public class RoomType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstType = new Dictionary<string, string>();
            lstType.Add("Private Room", "Private Room");
            lstType.Add("Shared Room", "Shared Room");            
            return lstType;
        }

    }
}