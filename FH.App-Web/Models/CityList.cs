﻿
using System.Collections.Generic;

namespace FH.App_Web
{
    public class City
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstCity = new Dictionary<string, string>();            
            lstCity.Add("GN", "Gurgaon");            
            return lstCity;
        }
    }
}