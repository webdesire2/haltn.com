﻿using System.Collections.Generic;

namespace FH.App_Web
{
    public class ParkingType
    {
        public static IDictionary<string, string> GetAll()
        {
            IDictionary<string, string> lstType = new Dictionary<string, string>();
            lstType.Add("BI", "Bike");
            lstType.Add("CA", "Car");
            lstType.Add("BC", "Bike and Car");
            lstType.Add("NO", "None");
            return lstType;
        }

    }
}