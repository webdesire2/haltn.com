﻿using FH.App.Entity;
using System.Collections.Generic;

namespace FH.App.Web.ViewModel
{ 
    public class VMPayment
    {
        public string paymentID { get; set; }
        public double Amount { get; set; }
        public string PaytmMID { get; set; }
        public string currency { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string contactNumber { get; set; }
        public string address { get; set; }
        public string description { get; set; }  
        public string access_code { get; set; }
        public string txnToken { get; set; }
    }
}