﻿namespace FH.App.Web.ViewModel
{
    public class VMPGCheckOut
    {
        public int RoomId { get; set; }
        public int Qty { get; set; }
    }
}