﻿using FH.App.Entity;
using System;
using System.Collections.Generic;

namespace FH.App.Web.ViewModel
{
    public class VMPaymentDetails
    {
        public MyCurrentStayDTO TanentStayDetails { get; set; }
        public IEnumerable<RTenantPaymentDetail> TenantPaymentHistory { get; set; }
    }

    public class CollectCashData
    {
        public int TenantId { get; set; }
        public double rentAmount { get; set; }
        public DateTime? rentForMonthStartDate { get; set; }
        public DateTime? rentForMonthEndDate { get; set; }
    }

    public class PropertyMapVM
    {
        public int Pid { get; set; }
        public int Tid { get; set; }
        public int RoomNo { get; set; } 
        public string Mobile { get; set; }
        public IEnumerable<PropertyList> PropertyList { get; set; }
        public IEnumerable<TenantMaster> TenantMaster { get; set; }
        public IEnumerable<PGRoomData> PGRoomData { get; set; }
    }
}  