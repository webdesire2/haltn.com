﻿using FH.App.Entity;
using System.Collections.Generic;

namespace FH.App.Web.ViewModel
{
    public class VMMyStay
    {   
        public bool IsTenantRegistered { get; set; }
        public string PropertyDetailPageUrl { get; set; }
        public MyCurrentStayDTO MyStayDetail { get; set; }
        public IEnumerable<MyOrderDTO> MyOrderDTOs { get; set; }
    }
}