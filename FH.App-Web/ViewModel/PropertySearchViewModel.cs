﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FH.App_Web.ViewModel
{
    public class PropertySearchViewModel
    {
        public string PropertyType { get; set; }
        public string Apartment { get; set; }
        public string Bhk { get; set; }
    }
}