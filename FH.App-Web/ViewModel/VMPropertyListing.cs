﻿using System.Collections.Generic;
using FH.App.Entity;
using FH.App_Entity;
using FH.Util;
namespace FH.App_Web
{
    public class VMPropertyListing
    {
        public IEnumerable<PropertyList> PropertyList { get; set; }
        public PropertyMaster_PG PropertyMaster_PG { get; set; }

        public IEnumerable<SEOCategory> simCategory { get; set; }
        public Pager Paging { get; set; }
        public string Locality { get; set; }
        public PropertySearchParameter Input { get; set; }


    }
}