﻿$(function () {
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('RequestVerificationToken', $('#RequestVerificationTokenAjax').val());
        },
        error: function (x, status, error) {
            if (x.status == 403) {
                alert("Sorry, your session has expired. Please login again to continue");
                window.location.href = "/user/Login";
            }
            else if (x.status == 406) {
                alert("Invalid request");
            }
            else if (x.status == 400) {
                var response = JSON.parse(x.responseText);
                if (response.Success == false) {
                    var error = response.Error;
                    for (var key in error) {
                        $("#lbl" + key).text(error[key]);
                    }
                }
            }
            else {
                alert("Something went wrong.please try again.");
            }
        }
    });
});

function showError(text) {
    $('.validation_error').text(text).addClass('active');
    setTimeout(function () { $('.validation_error').text('').removeClass('active'); }, 3000);
    return false;
}

function showSuccessMessage(text) {
    $('.success-message').text(text).addClass('active');
    setTimeout(function () { $('.success-message').text('').removeClass('active'); }, 3000);
}

var loading = {
    show: function (btn) {
        $(btn).html('Please Wait... <span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    },
    hide: function (btn, text) {
        $(btn).html(text).removeAttr("disabled");
    }
}

function reload(text) {
    $('.success-message').text(text).addClass('active');
    setTimeout(function () { location.reload() }, 2000);
}

function redirect(url) {
    window.location = url;
}
function showLoginPopUp(tab) {
    $('.customMenuRight').removeClass('customMenuRightActive');
    $('.backDropBg').removeClass('backDropShow');
    $('body').css('overflow', 'auto');
    $('#loginModal').modal('show');
    $('#loginModal .nav-tabs a[href="#' + tab + '"]').tab('show');
}

function loginBtn() {
    $('#loginModal').modal('show');
}

function validateEmail(f, i) {
    var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return regex.test(i) ? '' : 'Invalid ' + f + '.';
}
function validateMobile(f, i) {
    var mobilefilter = /^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$/;
    return mobilefilter.test(i) ? '' : 'Invalid ' + f + '.';
}
function validateAlpha(f, i) {
    var regex = new RegExp("^[a-zA-Z ]+$");
    return regex.test(i) ? '' : f + " can't be contain numeric & special characters.";
}
function validateInt(f, i) {
    var regex = new RegExp("^[0-9]+$");
    return regex.test(i) ? '' : f + " should be numeric.";
}
function isInt(e) {
    var value = String.fromCharCode(e.which);
    var regex = new RegExp("^[0-9]+$");
    return regex.test(value);
}
function isAlpha(event) {
    var value = String.fromCharCode(event.which);
    var regex = new RegExp("^[a-zA-Z ]+$");
    return regex.test(value);
}
function validateRequired(f, i) {
    if (i.trim() == "" || i == undefined || i == null)
        return f + ' is required.';
    return '';
}
function validateMinLen(f, i, o) {
    if (typeof o == 'object' && (o != undefined && o != null)) {
        for (var type in o) {
            if (type == "l" && i.length <= o[type]) { return f + ' must be more than ' + o[type] + ' characters.' }
            else
                return '';
        }
    }
}
function validateMaxLen(f, i, o) {
    if (typeof o == 'object' && (o != undefined && o != null)) {
        for (var type in o) {
            if (type == "l" && i.length > o[type]) { return f + ' must be less than ' + o[type] + ' characters.' }
            else
                return '';
        }
    }
}
function validateOTP(f, i) {
    if (i == "" || isNaN(i) || i.length != 4)
        return '4 digit otp required.'

    return '';
}
function validate(fld, val, rule) {
    if (rule != "" && rule != null) {
        for (var i in rule) {
            if (typeof rule[i] == "string") {
                var m = validationType(fld, rule[i], val);
                if (m != '') return m;
            }
            else if (typeof rule[i] == "object") {
                for (var j in rule[i]) {
                    var n = validationType(fld, j, val, rule[i][j]);
                    if (n != '') return n;
                }
            }
        }
    }
    return '';
}
function validationType(f, t, i, o) {
    switch (t) {
        case 'req': return validateRequired(f, i);
        case 'email': return validateEmail(f, i);
        case 'mobile': return validateMobile(f, i);
        case 'minLen': return validateMinLen(f, i, o);
        case 'maxLen': return validateMaxLen(f, i, o);
        case 'alpha': return validateAlpha(f, i);
        case 'int': return validateInt(f, i);
        case 'otp': return validateOTP(f, i);
    }
}

$('.enquiry-form-btn').click(function () {
    $('.enquiry-form-parent').toggleClass('active');
});

$(document).keypress(function (event) {
    var keycode = event.keyCode ? event.keyCode : event.which;
    if (keycode === 13) {
        if ($('#loginModal').css('display') === 'block' && $('#login').hasClass('active')) {
            login();
        }
    }
});

$("body").on("click", ".user-type", function () { $('.user-type').removeClass('active'); $(this).addClass('active'); });

function login() {
    var mobile = $('#LoginMobile').val().trim();
    var password = $('#LoginPassword').val();
    var err = '';
    if (err = validate('Mobile no', mobile, ['mobile'])) return showError(err);
    if (err = validate('Password', password, ["req"])) return showError(err);
    $('#btnLogin').html('Please Wait...<span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.post("/user/loginajax", { Mobile: mobile, Password: password }, function (data) {
        if (data.Success == true) {
            location.reload();
        }
        else {
            showError(data.ErrorMessage); $('#btnLogin').html('Login').removeAttr("disabled");
        }
    });
}

function registrationVerify() {
    var name = $('#RegName').val().trim();
    var mobile = $('#RegMobile').val().trim();
    var email = $('#RegEmail').val().trim();
    var password = $('#RegPassword').val();
    var userType = $('#register .user-type.active').data('key');
    var err = "";
    if (err = validate("Name", name, ['req', 'alpha', { 'maxLen': { 'l': '50' } }])) return showError(err);
    if (err = validate("Mobile no", mobile, ['mobile'])) return showError(err);
    if (err = validate("Email", email, ['req', 'email'])) return showError(err);
    if (err = validate("Password", password, ['req', { 'minLen': { 'l': '5' } }, { 'maxLen': { 'l': '50' } }])) return showError(err);

    if (userType != "Buyer" && userType != "Owner" && userType != "Agent")
        return showError("User type required.")

    $('#btnRegistrationVerify').html('Please Wait...<span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.post("/user/registrationAjax", { Name: name, Mobile: mobile, Email: email, Password: password, UserType: userType }, function (data) {
        if (data.Success == true) {
            $('#regosterBox').hide(); $('#register #verificationCode').show();
            $('#verificationCode .spn-verify-mobile').text(mobile);
            $('#RegistrationUser').val(data.User);
        }
        else
            showError(data.ErrorMessage);

        $('#btnRegistrationVerify').html('Register').removeAttr("disabled");
    });

}

function verifyOTP() {
    var otp = $('#RegistrationOTP').val().trim(), user = $('#RegistrationUser').val(), err = '';
    if (err = validate("OTP", otp, ['otp'])) return showError(err);
    $('#btnVerifyOTP').html('Please Wait...<span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.post("/user/verifyOTP", { OTP: otp, UserId: user }, function (data) {
        if (data.Success == true)
            reload('You have successfully registered.');
        else
            showError(data.ErrorMessage);
        $('#btnVerifyOTP').html('Submit').removeAttr("disabled");
    });
}

function validateMobileNo() {
    var mobile = $('#LoginMobile').val().trim(), err = '';
    if (err = validate('Mobile no', mobile, ['mobile'])) return showError(err);

    $('#btnReset').html('Please Wait...<span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.post("/user/isMobileExist", { mobile: mobile }, function (data) {
        if (data.Success == true) { $('#loginBox').hide(); $('#resetPasswordBox').show(); $('#ResetUser').val(data.User); }
        else
            showError(data.ErrorMessage);
        $('#btnReset').html('Reset Now!').removeAttr("disabled");
    });
}

function ResetPassword() {
    var user = $('#ResetUser').val(), password = $('#ResetNewPassword').val(), confirmPassword = $('#ResetConfirmPassword').val(), otp = $('#ResetOTP').val(), err = '';

    if (err = validate("Password", password, ['req', { 'minLen': { 'l': '5' } }, { 'maxLen': { 'l': '50' } }])) return showError(err);
    if (err = validate("Confirm Password", confirmPassword, ['req'])) return showError(err);
    if (password != confirmPassword) return showError("Confirm password not match.");
    if (err = validate("OTP", otp, ['otp'])) return showError(err);
    $('#btnSubmitReset').html('Please Wait...<span class="spinner-border spinner-border-sm"></span>').attr("disabled", true);
    $.post("/user/resetpasswordajax", { UserId: user, Password: password, ConfirmPassword: confirmPassword, OTP: otp }, function (data) {
        if (data.Success == true) { reload('Password reset successfully.'); }
        else
            showError(data.ErrorMessage);
        $('#btnSubmitReset').html('Reset Passowrd').removeAttr("disabled");
    });
} 