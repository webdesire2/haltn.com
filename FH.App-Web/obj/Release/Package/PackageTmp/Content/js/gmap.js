﻿function nearBy() {
    var nearByPlace = {
        airport: [],
        bus_station: [],
        train_station: [],
        hospital: [],
        atm: [],
        school: [],
        movie_theature: [],
        shopping_mall: [],
        gym:[],
        restaurant: []        
    }
    map = {
        place: nearByPlace,
        currentPosition:{lat:parseFloat($('#hdnLat').val()), lng:parseFloat($('#hdnLng').val())},
        mapObject:null,
        currentPin:null,
        directionRender: new google.maps.DirectionsRenderer({ suppressMarkers: true }),
        directionService: new google.maps.DirectionsService(),
        markerImage: {
            source: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
            destination: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        },        
        addPinForCurrentPosition: function () {
            var b =new google.maps.Marker({
                map: this.mapObject,               
                position: map.currentPosition,
                icon:map.markerImage.source
            });
            
            this.currentPin = b;
        },
        removePinForCurrentPosition: function() {
            this.currentPin.setMap(null)
        },
        reset: function () {
            this.mapObject= new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                maxZoom:14,
                center: map.currentPosition,
                disableDefaultUI: true,
                zoomControl: true,
                scrollwheel: false                
            });

            this.directionRender.setMap(this.mapObject);
        },
        createMarker: function (a, b, c, d) {
            var latLng = {lat:parseFloat(a),lng:parseFloat(b)}
            var marker = new google.maps.Marker({
                position: latLng,
                map: this.mapObject,
                icon: c               
            });

            var infowindow = new google.maps.InfoWindow({
                content: "<div class='infoDiv'>"+d+"</div>"
            });
            google.maps.event.addListener(marker, 'mouseover', function () {
                infowindow.open(this.mapObject, marker);
                var requets = {
                    origin:map.currentPosition,
                    destination:latLng,
                    travelMode: 'TRANSIT'
                };
                map.directionService.route(requets, function (result, status) {
                    if (status == 'OK') {
                        map.directionRender.setDirections(result);
                    }
                })
            });

            google.maps.event.addListener(marker, 'mouseout', function () {
                infowindow.close(this.mapObject, marker);
            });
        },  
      
        calculateDistance: function (a, b) {
            var service = new google.maps.DistanceMatrixService;
            for (var i = 0, g = []; i < a.length; i++) {
                g.push(a[i].geometry.location);
            }
            (new google.maps.DistanceMatrixService).getDistanceMatrix({
                origins: [map.currentPosition],
                destinations: g,
                travelMode:'DRIVING',
                unitSystem: google.maps.DirectionsUnitSystem.METRIC
            }, callback)
            function callback(result,status)
            {               
                for (var f = 0; f < result.destinationAddresses.length; f++) {
                    var name = a[f].name, lat, lng;
                    if (a[f].geometry ? (lat = a[f].geometry.location.lng(), lng = a[f].geometry.location.lat()) : (lat = a[f].latitude, lng = a[f].longitude), name.length >42 && (name = a[f].name.substring(0,42) + "...")) {
                        $('#' + b + '-list').append('<tr><td class="list-group-item nb-list-item" data-type="' + b + '" data-lat="' + lat + '" data-lng="' + lng + '" data-name="' + name + '" title="' + a[f].name + '">' + name + '<span class="float-right">' + result.rows[0].elements[f].distance.text + ' | ' + result.rows[0].elements[f].duration.text + '</span></td></tr>');
                    }
                    else
                    {
                        $('#' + b + '-list').append('<tr><td class="list-group-item nb-list-item" data-type="' + b + '" data-lat="' + lat + '" data-lng="' + lng + '" data-name="' + name + '" title="' + a[f].name + '">' + name + '<span class="float-right">' + result.rows[0].elements[f].distance.text + ' | ' + result.rows[0].elements[f].duration.text + '</span></td></tr>');
                    }
                }                    
            }                
        },
        showUtilityOnMap:function(a)
        {
            if (a == 'airport') {
                var airPortplace = new Array;
                if ($('#hdnCity').val()== 'GN')
                {                    
                    var f = {};
                    f.name = "Indira Gandhi International Airport ", f.geometry = {}, f.geometry.location = new google.maps.LatLng(28.54944559999999, 77.1180861), f.vicinity = "New Delhi, Delhi", airPortplace.push(f);
                }
                map.place[a] = airPortplace;
                map.calculateDistance(airPortplace, a);
                map.showOnMap(airPortplace);
            }
            else
                if (map.place[a] == null || map.place[a].length == 0) {                    
                var service = new google.maps.places.PlacesService(map.mapObject);
                service.nearbySearch({
                    location: this.currentPosition,
                    rankBy: google.maps.places.RankBy.DISTANCE,
                    type: a
                }, callback);

                function callback(results, status) {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        if (results.length > 5)
                        { results = results.slice(0, 5); }                        
                        map.place[a] = results;
                        map.calculateDistance(results, a);
                        map.showOnMap(results);
                    }
                }
            }
            else {
                map.calculateDistance(map.place[a], a);
                map.showOnMap(map.place[a]);
            }            
        },
        showOnMap: function (a) {            
            var e, f, g = new google.maps.LatLngBounds;            
            for (var h = 0; h < a.length; h++) {
                map.createMarker(a[h].geometry.location.lat(), a[h].geometry.location.lng(), map.markerImage.destination, a[h].name),
                a[h].geometry ? (f = a[h].geometry.location.lng(), e = a[h].geometry.location.lat()) : (e = a[h].latitude, f = a[h].longitude), g.extend(new google.maps.LatLng(e, f))
            }
            g.extend(map.currentPosition), this.mapObject.fitBounds(g)
        },
        showUtilityOnMapSearchCallBack:function(a)
        {
            map.showOnMap(a);
        },
        showTransitInfo:function(a)
        {           
            map.reset(), map.addPinForCurrentPosition();
            if ($('#transitInfo').data("loaded") != 1) {
                map.showUtilityOnMap('airport'), map.showUtilityOnMap('bus_station'), map.showUtilityOnMap('train_station'), $('#transitInfo').data("loaded", 1);
            }
            else {
                (a==undefined || a=="")?(map.showAirport(), map.showBusStop(), map.showTrainStation()):
                      map.showUtilityOnMapSearchCallBack(map.place[a])
                
            }
        },
        showEssentialInfo:function(a)
        {
            map.reset(), map.addPinForCurrentPosition();
            if ($('#essentialInfo').data("loaded") != 1) {
                map.showUtilityOnMap('hospital'), map.showUtilityOnMap('atm'), map.showUtilityOnMap('school'), $('#essentialInfo').data("loaded", 1);
            }
            else {
                (a == undefined || a == "") ? (map.showHospital(), map.showBankAndATM(), map.showSchool()) :
                      map.showUtilityOnMapSearchCallBack(map.place[a])
            }           
        },
        showUtilityInfo: function (a) {
            map.reset(), map.addPinForCurrentPosition();
            if ($('#utilityInfo').data("loaded") != 1) {
                map.showUtilityOnMap('movie_theater'), map.showUtilityOnMap('shopping_mall'), map.showUtilityOnMap('gym'), $('#utilityInfo').data("loaded", 1);
            }
            else {
                (a == undefined || a == "") ? (map.showMovieTheature(), map.showShoppingMall(), map.showGym()) :
                      map.showUtilityOnMapSearchCallBack(map.place[a])

            }           
        },
        showRestaurentInfo: function (a) {
            map.reset(), map.addPinForCurrentPosition();
            if($('#restaurentInfo').data("loaded") != 1) {
                map.showUtilityOnMap('restaurant'), $('#restaurentInfo').data("loaded", 1);
            }
            else {                
                (a == undefined || a == "") ? (map.showRestaurent()) :
                      map.showUtilityOnMapSearchCallBack(map.place[a])
            }
        },
        showAirport:function()
        {
            map.showUtilityOnMapSearchCallBack(map.place.airport);
        },        
        showBusStop: function () {
            map.showUtilityOnMapSearchCallBack(map.place.bus_station);
        },
        showTrainStation: function () {
            map.showUtilityOnMapSearchCallBack(map.place.train_station);
        },
        showHospital: function () {
            map.showUtilityOnMapSearchCallBack(map.place.hospital);
        },
        showBankAndATM: function () {
            map.showUtilityOnMapSearchCallBack(map.place.atm);
        },
        showSchool: function () {
            map.showUtilityOnMapSearchCallBack(map.place.school);
        },
        showMovieTheature: function () {
            map.showUtilityOnMapSearchCallBack(map.place.movie_theater);
        },
        showShoppingMall: function () {
            map.showUtilityOnMapSearchCallBack(map.place.shopping_mall);
        },
        showGym: function () {
            map.showUtilityOnMapSearchCallBack(map.place.gym);
        },
        showRestaurent: function () {
            map.showUtilityOnMapSearchCallBack(map.place.restaurant);
        }
    }
}

