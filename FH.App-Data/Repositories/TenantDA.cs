﻿using Dapper;
using System.Collections.Generic;
using FH.App.Entity;
using System.Data;
using System.Data.SqlClient;
using System;
using System.Linq;

namespace FH.App.Data
{
    public class TenantDA
    {
        string _connectionString = string.Empty;

        public TenantDA()
        {
            _connectionString = ConnectionFactory.getConString;
        }

        public IEnumerable<T> Execute<T>(TenantMaster de, TenantCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_tenent_mgmt", TenantParam(de, callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch(Exception e)
                {
                    string ex = e.Message;
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }

        public T ExecuteWithTransaction<T>(TenantMaster de, TenantCallValue param)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                IDbTransaction tran = con.BeginTransaction();
                try
                {
                    var result = con.QueryFirstOrDefault<T>("udsp_tenent_mgmt", TenantParam(de, param), tran, commandType: CommandType.StoredProcedure);
                    tran.Commit();
                    return result;
                }
                catch (Exception e)
                {
                    tran.Rollback();
                    throw new ApplicationException();
                }
                finally
                {
                    tran.Dispose();
                    con.Dispose();
                }
            }
        }

        private DynamicParameters TenantParam(TenantMaster de, TenantCallValue callValue)
        {
            var paymentDetail = new PaymentDetail();
            var tenantPayment = new TenantPaymentDetail();

            if (de.PaymentDetail != null)
                paymentDetail = de.PaymentDetail;

            if (de.TenantPaymentDetail != null)
                tenantPayment = de.TenantPaymentDetail;

            var param = new DynamicParameters();
            param.Add("@QueryType", (int)callValue);
            param.Add("@CreatedBy", de.CreatedBy);
            param.Add("@CheckOutDate", de.CheckOutDate);
            param.Add("@PropertyId", de.PropertyId);
            param.Add("@TenantId", de.TenantId);
            param.Add("@TenantStatus", de.TenantStatus);
            param.Add("@RoomNo", de.RoomNo);
            param.Add("@Mobile", de.Mobile);
            param.Add("@RentPaid", de.RentPaid);
            if (de.CheckInDate.Year > 1) {
                param.Add("@CheckInDate", de.CheckInDate);
            }  

            if (de.Name != null)
            { 
                param.Add("@Name", de.Name);
                param.Add("@Email", de.Email);
                param.Add("@Mobile", de.Mobile);
                param.Add("@RoomNo", de.RoomNo);
                param.Add("@RoomId", de.RoomId);
                param.Add("@Occupancy", de.Occupancy);
                param.Add("@SecurityAmount", de.SecurityAmount);
                param.Add("@MonthlyRent", de.MonthlyRent);
                param.Add("@CheckInDate", de.CheckInDate);
                param.Add("@SecurityDeposit", de.SecurityDeposit);
            }
             
            param.Add("@DepositAmount",tenantPayment.DepositAmount);
            param.Add("@PaymentType", tenantPayment.PaymentType);
            param.Add("@RentForMonthStartDate",tenantPayment.RentForMonthStartDate);
            param.Add("@RentForMonthEndDate", tenantPayment.RentForMonthEndDate);
            param.Add("@PaymentStatus", tenantPayment.PaymentStatus);

            param.Add("@PaymentId", paymentDetail.PaymentId);
            param.Add("@Amount", paymentDetail.Amount);
            param.Add("@Status",paymentDetail.Status);
            param.Add("@TracingId", paymentDetail.TracingId);
            param.Add("@BankRefNo", paymentDetail.BankRefNo);
            param.Add("@PaymentMode", paymentDetail.PaymentMode);
            param.Add("@StatusCode", paymentDetail.StatusCode);
            param.Add("@StatusMessage", paymentDetail.StatusMessage);
            param.Add("@GatewayFullResponse", paymentDetail.GatewayFullResponse);
            param.Add("@UseCode", paymentDetail.UseCode);
            param.Add("@UseCodeAmount", paymentDetail.UseCodeAmount);

            if (de.Payment > 0)
            { 
                param.Add("@Payment", de.Payment);
                param.Add("@PaymentType", de.PaymentType);
                param.Add("@RentForMonthStartDate", de.RentForMonthStartDate);
                param.Add("@RentForMonthEndDate", de.RentForMonthEndDate);
            }

            //if (de.UseCodeMaster != null)
            //{
            //    param.Add("@UseCodeId", de.UseCodeMaster.UseCodeId);
            //}


            //if (de.PaymentDetails != null)
            //{
            //    DataTable dtPaymentDetail = Util.Convert.ToDataTable<PaymentDetail>(de.PaymentDetails);
            //    param.Add("@PaymentDetail", dtPaymentDetail.AsTableValuedParameter("PaymentDetail"));
            //}

            //if (de.TenantPaymentDetails != null)
            //{
            //    DataTable dtTenantPaymentDetail = Util.Convert.ToDataTable<TenantPaymentDetail>(de.TenantPaymentDetails);
            //    param.Add("@TenantPaymentDetail", dtTenantPaymentDetail.AsTableValuedParameter("TenantPaymentDetail"));
            //}

            return param;
        }
    }

    public enum TenantCallValue
    {
        AddNewPayment=4,
        GetAllPayment=5,
        GetPaymentDetail=6,
        GetTenantMaster=7,
        GetMyCurrentStay=8,
        AddCheckOut=9,
        GetPaymentLinkDetail = 21,
        GetMyTanent = 35,
        GetRentPaidTanent = 24,
        GetTenantPaymentLog = 25,
        GetTanentStayDetails = 26,
        RSendPaymentLink = 20,
        SSendPaymentLink = 20,
        GetSingleTenant = 17,
        AddNewTanent = 27,
        getAvailableRooms = 30,
        GetRTenantMaster = 31,
        AddRNewPayment = 32,
        SGetMyTanent = 37,
        SGetRentPaidTanent = 38,
        SgetAvailableRooms = 39,
        getMyProperties = 36,
        occupancyDetails = 41,
    }
}
