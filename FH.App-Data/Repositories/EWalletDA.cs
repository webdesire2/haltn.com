﻿using FH.App.Entity;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System;

namespace FH.App.Data
{
    public class EWalletDA
    {
        string _connectionString = string.Empty;

        public EWalletDA()
        {
            _connectionString = ConnectionFactory.getConString;
        }

        public IEnumerable<T> Execute<T>(EWalletDE de, EWalletCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_ewallet", EWalletParam(de, callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }

        public T ExecuteWithTransaction<T>(EWalletDE de, EWalletCallValue param)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                IDbTransaction tran = con.BeginTransaction();
                try
                {
                    var result = con.QueryFirstOrDefault<T>("udsp_ewallet", EWalletParam(de, param), tran, commandType: CommandType.StoredProcedure);
                    tran.Commit();
                    return result;
                }
                catch (Exception e)
                {
                    tran.Rollback();
                    throw new ApplicationException();
                }
                finally
                {
                    tran.Dispose();
                    con.Dispose();
                }
            }
        }

        private DynamicParameters EWalletParam(EWalletDE de, EWalletCallValue callValue)
        {           
            var param = new DynamicParameters();
            param.Add("@QueryType", (int)callValue);
            param.Add("@UserId", de.UserId);            
            param.Add("@PaymentId", de.PaymentId);
            param.Add("@Amount", de.Amount);
            param.Add("@Status", de.Status);
            param.Add("@TracingId",de.TracingId);
            param.Add("@BankRefNo",de.BankRefNo);
            param.Add("@PaymentMode",de.PaymentMode);
            param.Add("@StatusCode",de.StatusCode);
            param.Add("@StatusMessage",de.StatusMessage);
            param.Add("@GatewayFullResponse",de.GatewayFullResponse);
            param.Add("@PaymentType", de.PaymentType);
            param.Add("@UseCode", de.UseCode);
            param.Add("@UseCodeAmount", de.UseCodeAmount);
          
            return param;
        }
    }

    public enum EWalletCallValue
    {
        GetAll=1,
        GetWalletBalance=2,
        AddDebitNode=3,
        AddOrder=4
    }
}
