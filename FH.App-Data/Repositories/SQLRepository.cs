﻿using Dapper;
using FH.App.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FH.App_Data.Repositories
{
    public class SQLRepository
    {
        string _connectionString = string.Empty;

        public SQLRepository()
        {
            _connectionString = ConnectionFactory.getConString;
        }

        public IEnumerable<T> Execute<T>(string query)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>(query, null, commandType: CommandType.Text);
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }
    }
}
