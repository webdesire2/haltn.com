﻿using Dapper;
using System.Data;
using FH.App.Entity;
using System.Collections.Generic;
using System.Data.SqlClient;
using System;

namespace FH.App.Data
{
    public class PaymentDA
    {
        string _connectionString = string.Empty;

        public PaymentDA()
        {
            _connectionString = ConnectionFactory.getConString;
        }

        public IEnumerable<T> Execute<T>(PaymentDetail de, PaymentCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_payment", PaymentParam(de,callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }
        
        private DynamicParameters PaymentParam(PaymentDetail de, PaymentCallValue callValue)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", (int)callValue);
            param.Add("@UserId", de.CreatedBy);
            param.Add("@PaymentId", de.PaymentId);
            param.Add("@Amount", de.Amount);
            param.Add("@Status", de.Status);
            param.Add("@TracingId", de.TracingId);
            param.Add("@BankRefNo", de.BankRefNo);
            param.Add("@PaymentMode", de.PaymentMode);
            param.Add("@StatusCode", de.StatusCode);
            param.Add("@StatusMessage", de.StatusMessage);
            param.Add("@GatewayFullResponse", de.GatewayFullResponse);
            param.Add("@PaymentMID", de.PaymentMID);
            return param;
        }
    }

    public enum PaymentCallValue
    {
        UpdatePayment = 1,
        GetTenantInvoice=2
    }    
}