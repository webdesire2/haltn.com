﻿using FH.App.Entity;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System;

namespace FH.App.Data
{
    public class UseCodeMasterDA
    {
        string _connectionString = string.Empty;

        public UseCodeMasterDA()
        {
            _connectionString = ConnectionFactory.getConString;
        }

        public IEnumerable<T> Execute<T>(UseCodeMaster de, UseCodeCallValue callValue)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                try
                {
                    var result = con.Query<T>("udsp_use_code_mgmt", UseCodeParam(de, callValue), null, commandType: CommandType.StoredProcedure);
                    return result;
                }
                catch (Exception e)
                {
                    throw new ApplicationException();
                }
                finally
                {
                    con.Dispose();
                }
            }
        }

        public T ExecuteWithTransaction<T>(UseCodeMaster de, UseCodeCallValue param)
        {
            using (IDbConnection con = new SqlConnection(_connectionString))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                IDbTransaction tran = con.BeginTransaction();
                try
                {
                    var result = con.QueryFirstOrDefault<T>("udsp_use_code_mgmt", UseCodeParam(de, param), tran, commandType: CommandType.StoredProcedure);
                    tran.Commit();
                    return result;
                }
                catch (Exception e)
                {
                    tran.Rollback();
                    throw new ApplicationException();
                }
                finally
                {
                    tran.Dispose();
                    con.Dispose();
                }
            }
        }

        private DynamicParameters UseCodeParam(UseCodeMaster de, UseCodeCallValue callValue)
        {
            if (de == null)
                de = new UseCodeMaster();

            var param = new DynamicParameters();
            param.Add("@UseCodeId", de.UseCodeId);
            param.Add("@Code", de.Code);
            param.Add("@UseCodeType", de.UseCodeType);
            param.Add("@UseCodeTypePercentage", de.UseCodeTypePercentage);
            param.Add("@CreatedBy", de.CreatedBy);
            param.Add("@IsActive", de.IsActive);
            param.Add("@UpdatedBy", de.UpdatedBy);
            param.Add("@QueryType", (int)callValue);
            return param;
        }
    }

    public enum UseCodeCallValue
    {
        GetByCode=5,
        GetActiveCode=6
    }
}
