﻿using Dapper;
using FH.App.Entity;
using System.Data;

namespace FH.App.Data
{
   public class UserDA:GenericRepository<UserDetail>,IUserDA
    {
        IDbTransaction _transaction;
        public UserDA(IDbTransaction transaction):base(transaction)
        {
            _transaction = transaction;
        }
        public UserDetail FindByEmailMobile(UserDetail entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType",1);            
            param.Add("@Email", entity.Email);
            param.Add("@Mobile", entity.Mobile);
            var data=Get("Usp_UserDetails", param);
            return data;
        }
        public string GetUserByMobile(UserDetail entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType",8);            
            param.Add("@Mobile",entity.Mobile);
            param.Add("@OTP", entity.OTP);
            param.Add("@OTPExpiredOn", entity.OTPExpiredOn);

            var data = Get("Usp_UserDetails", param);
            if (data != null)
                return data.UserId;

            return string.Empty;
        }
        public int AddUser(UserDetail entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType",2);
            param.Add("@Name", entity.Name);
            param.Add("@Email", entity.Email);
            param.Add("@Mobile", entity.Mobile);
            param.Add("@Password",entity.Password);
            param.Add("@OTP", entity.OTP);
            param.Add("@OTPExpiredOn", entity.OTPExpiredOn);
            param.Add("@UserType", entity.UserType);
            return  SqlMapper.QueryFirstOrDefault<int>(_transaction.Connection, "usp_UserDetails", param, _transaction, commandType: CommandType.StoredProcedure);
                      
        }
        public UserDetail IsValidLogin(UserDetail entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 3);
            param.Add("@Mobile", entity.Mobile);
            param.Add("@Password", entity.Password);
            var data = Get("usp_UserDetails", param);
            return data;
        }
        public UserDetail ResetPassword(UserDetail user)
        {            
                var param = new DynamicParameters();
                param.Add("@QueryType", 6);
                param.Add("@UserId", user.UserId);
                param.Add("@Password",user.Password);
                param.Add("@OTP", user.OTP);
                param.Add("@OTPExpiredOn", user.OTPExpiredOn);

                return SqlMapper.QueryFirstOrDefault<UserDetail>(_transaction.Connection, "usp_UserDetails", param, _transaction, commandType: CommandType.StoredProcedure);                
        }
        public UserDetail IsValidOTP(int userId,int otp,System.DateTime otpExpiredOn)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType",7);
            param.Add("@UserId",userId);                        
            param.Add("@OTP", otp);
            param.Add("@OTPExpiredOn",otpExpiredOn.ToString("yyyy-MM-dd HH:mm:ss"));
            var data = Get("usp_UserDetails", param);
            return data;
        }

        public UserDetail GetUserByUserId(int userId)
        {
            var param = new DynamicParameters();
            param.Add("@UserId",userId);
            string query = "SELECT UserId,Name,Email,Mobile,UserType FROM User_Details WITH(NOLOCK) WHERE UserId=@UserId";
            return SqlMapper.QueryFirstOrDefault<UserDetail>(_transaction.Connection,query,param, _transaction, commandType: CommandType.Text);
        }
        public void UpdateUser(UserDetail entity)
        {
            var param = new DynamicParameters();
            param.Add("@Name",entity.Name);
            param.Add("@Email",entity.Email);
            param.Add("@UserId", entity.UserId);
            string query = "UPDATE User_Details SET Name=@Name,Email=@Email,ModifiedOn=getdate() WHERE UserId=@UserId";
            SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
        }
        public void AddQuickEnquiry(QuickEnquiry entity)
        {
            var param = new DynamicParameters();
            param.Add("@Name", entity.UserName);
            param.Add("@Email", entity.UserEmail);
            param.Add("@Mobile", entity.UserMobile);
            param.Add("@Description", entity.Description);
            string query = "INSERT INTO Enquiry_Details(UserName,UserMobile,UserEmail,Description) VALUES(@Name,@Mobile,@Email,@Description)";
            SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
        }
        public TenantPaymentDetail GetPaymentLinkDetail(int paymentLinkId)
        { 
            var param = new DynamicParameters();
            param.Add("@PaymentLinkId", paymentLinkId);
            string query = "SELECT * FROM PaymentLink WHERE PaymentLinkId = @PaymentLinkId";
            return SqlMapper.QueryFirstOrDefault<TenantPaymentDetail>(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
        }

        public MyCurrentStayDTO GetMyCurrentStay(int TenantId)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 8);
            param.Add("@CreatedBy", TenantId); 
            return SqlMapper.QueryFirstOrDefault<MyCurrentStayDTO>(_transaction.Connection, "udsp_tenent_mgmt", param, _transaction, commandType: CommandType.StoredProcedure);

        }

        public MyOrderDTO GetAllPayment(int TenantId)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 5);
            param.Add("@CreatedBy", TenantId);
            return SqlMapper.QueryFirstOrDefault<MyOrderDTO>(_transaction.Connection, "udsp_tenent_mgmt", param, _transaction, commandType: CommandType.StoredProcedure);

        }


    }


    public interface IUserDA
    {
        UserDetail FindByEmailMobile(UserDetail entity);
       int AddUser(UserDetail entity);
        UserDetail IsValidLogin(UserDetail entity);
        UserDetail ResetPassword(UserDetail user);
        UserDetail IsValidOTP(int userId, int otp, System.DateTime otpExpiredOn);
        string GetUserByMobile(UserDetail entity);
        UserDetail GetUserByUserId(int userId);
        void UpdateUser(UserDetail entity);
        void AddQuickEnquiry(QuickEnquiry entity);
        TenantPaymentDetail GetPaymentLinkDetail(int paymentLinkId);
        MyCurrentStayDTO GetMyCurrentStay(int TenantId);
        MyOrderDTO GetAllPayment(int tenantId);
    }
}
