﻿using System;
using System.Collections.Generic;
using System.Data;
using FH.App.Entity;
using Dapper;
using System.Linq;
using FH.App_Entity;

namespace FH.App.Data
{
   public class PropertyDA:IPropertyDA
    {
        private IDbTransaction _transaction;
        public PropertyDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }
        public IEnumerable<PropertyList> GetAllProperty(PropertySearchParameter input, out int totalRecord)
        {
            totalRecord = 0;
            try
            {
                IEnumerable<PropertyList> lstProperty;
                if (input.PropertyType.ToUpper() == "PG")
                {
                    var param = new DynamicParameters();
                    param.Add("@PageIndex", input.PageIndex);
                    param.Add("@PageSize", input.PageSize);
                    param.Add("@Latitude", input.Latitude);
                    param.Add("@Longitude", input.Longitude);
                    param.Add("@Gender", input.TenantGender);
                    param.Add("@RoomType", input.RoomType);
                    param.Add("@MinPrice", input.MinPrice);
                    param.Add("@MaxPrice", input.MaxPrice);
                    param.Add("@Locality", input.Locality);
                    param.Add("@City", input.City);
                    param.Add("@TotalRecord", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    lstProperty = SqlMapper.Query<PropertyList>(_transaction.Connection, "Usp_Property_Search_PG_test", param, _transaction, commandType: CommandType.StoredProcedure);
                    totalRecord = param.Get<int>("@TotalRecord");
                }
                else
                {
                    var param = new DynamicParameters();
                    param.Add("@PageIndex", input.PageIndex);
                    param.Add("@PageSize", input.PageSize);
                    param.Add("@PropertyType", input.PropertyType);
                    param.Add("@Latitude", input.Latitude);
                    param.Add("@Longitude", input.Longitude);
                    param.Add("@Apartment", input.Apartment);
                    param.Add("@BhkType", input.Bhk);
                    param.Add("@Furnishing", input.FurnishingType);
                    param.Add("@PreferredTenant", input.PreferredTenant);
                    param.Add("@Facing", input.Facing);
                    param.Add("@Gender", input.TenantGender);
                    param.Add("@RoomType", input.RoomType);
                    param.Add("@MinPrice", input.MinPrice);
                    param.Add("@MaxPrice", input.MaxPrice);
                    param.Add("@Locality", input.Locality);
                    param.Add("@City", input.City);
                    param.Add("@TotalRecord", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    lstProperty = SqlMapper.Query<PropertyList>(_transaction.Connection, "Usp_Property_Search", param, _transaction, commandType: CommandType.StoredProcedure);
                    totalRecord = param.Get<int>("@TotalRecord");
                }     
                return lstProperty;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public VMProperty GetProperty(int propertyId)
        {
            VMProperty vmProperty = new VMProperty();
            var param = new DynamicParameters();
            param.Add("@propertyId", propertyId);

            using (var multiRecord = SqlMapper.QueryMultiple(_transaction.Connection, "usp_Property_Search_Details", param, _transaction, commandType: CommandType.StoredProcedure))
            {
                string propertyType = multiRecord.ReadSingleOrDefault<string>();
                if (!string.IsNullOrEmpty(propertyType) && propertyType.ToUpper() == "FLAT" || propertyType.ToUpper() == "FLATMATE" || propertyType.ToUpper() == "ROOM")
                {
                    PropertyMaster propMaster = new PropertyMaster();
                    propMaster.PropertyDetail = multiRecord.ReadSingleOrDefault<PropertyDetail>();
                    propMaster.Furnishing = multiRecord.Read<FurnishingDetail>();
                    propMaster.Galleries = multiRecord.Read<PropertyGallery>();
                    propMaster.AmenityMaster = multiRecord.Read<AmenityMaster>();
                    propMaster.PropertyAmenities = multiRecord.Read<PropertyAmenity>();                    
                    vmProperty.PropertyMaster = propMaster;

                    dynamic metaTags = multiRecord.ReadSingleOrDefault();
                    if (metaTags != null)
                    {
                        vmProperty.MetaTitle = metaTags.MetaTitle;
                        vmProperty.MetaKeyword = metaTags.MetaKeyword;
                        vmProperty.MetaDesc = metaTags.MetaDesc;
                        vmProperty.MetaJson = metaTags.JsonDesc;
                    }
                }
                else if(propertyType.ToUpper() == "PG")
                {
                    PropertyMaster_PG pgMaster = new PropertyMaster_PG();

                    pgMaster.PropertyDetail_PG= multiRecord.ReadSingleOrDefault<PropertyDetail_PG>();
                    pgMaster.RoomDetail = multiRecord.Read<PGRoomDetail>();
                    pgMaster.PgRules = multiRecord.Read<PGRule>();
                    pgMaster.Galleries = multiRecord.Read<PropertyGallery>();
                    pgMaster.AmenityMaster = multiRecord.Read<AmenityMaster>();
                    pgMaster.PropertyAmenities = multiRecord.Read<PropertyAmenity>();
                    pgMaster.RoomAmenities = multiRecord.Read<RoomAmenity>();                   
                    vmProperty.PropertyMaster_PG = pgMaster;

                    dynamic metaTags = multiRecord.ReadSingleOrDefault();
                    if (metaTags != null)
                    {
                        vmProperty.MetaTitle = metaTags.MetaTitle;
                        vmProperty.MetaKeyword = metaTags.MetaKeyword;
                        vmProperty.MetaDesc = metaTags.MetaDesc;
                        vmProperty.MetaJson = metaTags.JsonDesc;
                    }
                }
            }

            return vmProperty;            
        }        
        public LeadDetails ScheduleVisitVerify(LeadDetails entity)
        {
            try
            {
                var param = new DynamicParameters();
                param.Add("@QueryType",1);
                param.Add("@PropertyId", entity.PropertyId);
                param.Add("@UserId", entity.UserId);
                param.Add("@VisitDate", entity.VisitDate);
                param.Add("@VisitTime", entity.VisitTime);
                param.Add("@OTP", entity.OTP);
                param.Add("@OTPExpiredOn", entity.OTPExpiredOn);

                return SqlMapper.QueryFirstOrDefault<LeadDetails>(_transaction.Connection, "Usp_ScheduleVisit", param, _transaction, null, CommandType.StoredProcedure);
                
            }
            catch
            {
                throw;
            }
        }        
        public LeadDetails ScheduleVisit(LeadDetails entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType",2);
            param.Add("@PropertyId", entity.PropertyId);
            param.Add("@UserId", entity.UserId);
            param.Add("@VisitDate", entity.VisitDate);
            param.Add("@VisitTime", entity.VisitTime);            
            return SqlMapper.QueryFirstOrDefault<LeadDetails>(_transaction.Connection, "Usp_ScheduleVisit", param, _transaction, null, CommandType.StoredProcedure);
        }
        public LeadDetails VeriyEnquiryDetail(LeadDetails entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType", 1);
            param.Add("@PropertyId", entity.PropertyId);
            param.Add("@UserId", entity.UserId);            
            param.Add("@OTP", entity.OTP);
            param.Add("@OTPExpiredOn", entity.OTPExpiredOn);

            return SqlMapper.QueryFirstOrDefault<LeadDetails>(_transaction.Connection, "Usp_EnquiryDetail", param, _transaction, null, CommandType.StoredProcedure);
            
        }
        public LeadDetails EnquiryDetail(LeadDetails entity)
        {
            var param = new DynamicParameters();
            param.Add("@QueryType",2);
            param.Add("@PropertyId", entity.PropertyId);
            param.Add("@UserId", entity.UserId);            
            return SqlMapper.QueryFirstOrDefault<LeadDetails>(_transaction.Connection, "Usp_EnquiryDetail", param, _transaction, null, CommandType.StoredProcedure);
        }
        public IEnumerable<PropertyList> GetFlat(bool isFurnish,bool isForBechelors)
        {
            string query = "";
            query = "SELECT TOP 5 PM.PropertyId,PM.PropertyType,(PD.BhkType+' Flat In '+PM.Locality) Locality,PD.PropertySize,PD.ExpectedRent,PG.FilePath ImageUrl FROM Property_Master PM WITH(NOLOCK)";
            query += " INNER JOIN Property_Details PD WITH(NOLOCK) ON PM.PropertyId = PD.PropertyId";
            query += " CROSS APPLY (SELECT TOP 1 PropertyId, FilePath FROM Property_Gallery_Details WITH(NOLOCK) WHERE PropertyId = PM.PropertyId)PG";
            query += " WHERE PropertyType = 'FLAT' AND UPPER(PropertyStatusByFH) = 'APPROVED'";
            if(isFurnish)
            query += " AND UPPER(PD.Furnishing) IN ('FURNISHED','SEMI-FURNISHED')";
            if(isForBechelors)
            query += " AND PD.PreferredTenant='Bachelors'";
            query += " ORDER BY PM.PropertyId DESC";
            return SqlMapper.Query<PropertyList>(_transaction.Connection, query, null, _transaction, commandType: CommandType.Text);
        }
        public IEnumerable<PropertyList> GetFlatMate(string gender)
        {
            gender = (!string.IsNullOrEmpty(gender) ? gender.ToUpper():"MALE");
            string query = "";
            query = "SELECT TOP 5 PM.PropertyId,PM.PropertyType,('Flatmate For '+PD.BhkType+' Flat In '+PM.Locality) Locality,PD.PropertySize,PD.ExpectedRent,PG.FilePath ImageUrl FROM Property_Master PM WITH(NOLOCK)";
            query += " INNER JOIN Property_Details PD WITH(NOLOCK) ON PM.PropertyId = PD.PropertyId";
            query += " CROSS APPLY (SELECT TOP 1 PropertyId, FilePath FROM Property_Gallery_Details WITH(NOLOCK) WHERE PropertyId = PM.PropertyId)PG";
            query += " WHERE PropertyType = 'FLATMATE' AND UPPER(PropertyStatusByFH) = 'APPROVED'";
            query += " AND UPPER(TenantGender)='"+gender+"'";
            query += " ORDER BY PM.PropertyId DESC";
            return SqlMapper.Query<PropertyList>(_transaction.Connection, query, null, _transaction, commandType: CommandType.Text);
        }
        public IEnumerable<PropertyList> GetPG(string gender)
        {
            gender = (!string.IsNullOrEmpty(gender) ? gender.ToUpper() : "MALE");
            string query = "";
            query = "SELECT TOP 5 PM.PropertyId,PM.PropertyType,('PG In '+PM.Locality) Locality,PD.ExpectedRent,PG.FilePath ImageUrl FROM Property_Master PM WITH(NOLOCK)";
            query += " INNER JOIN Property_Details_PG PD WITH(NOLOCK) ON PM.PropertyId = PD.PropertyId";
            query += " OUTER APPLY(SELECT TOP 1 PropertyId, FilePath FROM Property_Gallery_Details WITH(NOLOCK) where PropertyId=PM.PropertyId ) PG ";
            query += " WHERE PropertyType ='PG' AND UPPER(PropertyStatusByFH) = 'APPROVED'";
            query += " AND UPPER(TenantGender)='"+gender+"'";
            query += " ORDER BY PM.PropertyId DESC";
            return SqlMapper.Query<PropertyList>(_transaction.Connection, query, null, _transaction, commandType: CommandType.Text);
        }
        public IEnumerable<PropertyList> GetRoom()
        {            
            string query = "";
            query = "SELECT TOP 5 PM.PropertyId,PM.PropertyType,(PD.RoomType+' In '+PM.Locality) Locality,PD.PropertySize,PD.ExpectedRent,PG.FilePath ImageUrl FROM Property_Master PM WITH(NOLOCK)";
            query += " INNER JOIN Property_Details PD WITH(NOLOCK) ON PM.PropertyId = PD.PropertyId";
            query += " CROSS APPLY (SELECT TOP 1 PropertyId, FilePath FROM Property_Gallery_Details WITH(NOLOCK) WHERE PropertyId = PM.PropertyId)PG";
            query += " WHERE PropertyType ='ROOM' AND UPPER(PropertyStatusByFH) = 'APPROVED'";
            query += " AND UPPER(PD.RoomType) IN('PRIVATE ROOM','SHARED ROOM')";
            query += " ORDER BY PM.PropertyId DESC";
            return SqlMapper.Query<PropertyList>(_transaction.Connection, query, null, _transaction, commandType: CommandType.Text);
        }       
        public IEnumerable<PropertyList> GetWishListProperty(string propertyIds)
        { 
            var param = new DynamicParameters();
            param.Add("@PropertyIds", propertyIds);
            return SqlMapper.Query<PropertyList>(_transaction.Connection, "usp_get_wishlist_property", param, _transaction, commandType: CommandType.StoredProcedure);            
        }
        public IEnumerable<SEOCategory> GetSimilarCategories(string input)
        {
            string query = "";       
             //query = "DECLARE @geo GEOGRAPHY;  SET @geo= geography::Point(28.467708,77.0653,4326) SELECT  * FROM SEO_Category WHERE  (@geo.STDistance(geography::Point(ISNULL(Lat,0),ISNULL(Lng,0),4326)))/1000 <= 1";
            query = "DECLARE @Lat FLOAT; SET @Lat = (SELECT Lat FROM SEO_Category WHERE [Url] = '" + input + "'); DECLARE @Lng FLOAT; SET @Lng = (SELECT Lng FROM SEO_Category WHERE [Url] =  '" + input + "' ); DECLARE @geo GEOGRAPHY;   SET @geo = geography::Point(@Lat, @Lng, 4326) SELECT TOP (20) * FROM SEO_Category WHERE(@geo.STDistance(geography::Point(ISNULL(Lat,0),ISNULL(Lng, 0),4326)))/ 1000 <= 2";
            return SqlMapper.Query<SEOCategory>(_transaction.Connection, query, null, _transaction, commandType: CommandType.Text);
        }

        public IEnumerable<PropertyList> GetSimilarProperty(PropertySearchParameter input)
        {
            var param = new DynamicParameters();
            param.Add("@PropertyType", input.PropertyType);
            param.Add("@Bhk", input.Bhk);
            param.Add("@Lat", input.Latitude);
            param.Add("@Lng", input.Longitude);
            return SqlMapper.Query<PropertyList>(_transaction.Connection, "usp_Similar_Property_On_Details", param, _transaction, commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<CityAreaMaster> GetCityLocalityList()
        {
            string query = "SELECT * FROM City_Locality_Master WITH(NOLOCK) WHERE ISACTIVE=1";
            return SqlMapper.Query<CityAreaMaster>(_transaction.Connection,query,null, _transaction, commandType: CommandType.Text);
        }
        public IEnumerable<PropertyFeedBack> GetPGFeedBack(int pid)
        {
            string query = "SELECT * FROM PropertyFeedback WITH(NOLOCK) WHERE ISACTIVE=1 AND PropertyId="+pid;
            return SqlMapper.Query<PropertyFeedBack>(_transaction.Connection, query, null, _transaction, commandType: CommandType.Text);
        }
        public void AddPGFeedback(PropertyFeedBack entity)
        {
            try
            {
                string query = "INSERT INTO PropertyFeedBack(PropertyId,FoodQuality,Security,Wifi,Staff,Maintenance,Description,FeedBackByName,IsActive,CreatedBy) ";
                query += "VALUES(@PropertyId,@FoodQuality,@Security,@Wifi,@Staff,@Maintenance,@Description,@FeedBackByName,@IsActive,@CreatedBy)";
                var param = new DynamicParameters();
                param.Add("@PropertyId", entity.PropertyId);
                param.Add("@FoodQuality", entity.FoodQuality);
                param.Add("@Security", entity.Security);
                param.Add("@Wifi", entity.Wifi);
                param.Add("@Staff", entity.Staff);
                param.Add("@Maintenance", entity.Maintenance);
                param.Add("@Description", entity.Description);
                param.Add("@FeedBackByName", entity.FeedBackByName);
                param.Add("@IsActive",false);
                param.Add("@CreatedBy",entity.CreatedBy);
                SqlMapper.Execute(_transaction.Connection, query, param, _transaction, commandType: CommandType.Text);
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<PGRoomDetail> GetPGRoomDetail(int propertyId)
        {
            string query = string.Format("SELECT * FROM PG_ROOM_DETAILS WHERE PropertyId={0}", propertyId);
            var roomDetail = SqlMapper.Query<PGRoomDetail>(_transaction.Connection,query, null, _transaction, commandType: CommandType.Text);
            return roomDetail;
        }
        
        public dynamic GetPropertyListMetaTag(string categorySlug)
        {
            string query = string.Format("SELECT TOP 1 * FROM SEO_Category WHERE Url='{0}'",categorySlug);
            return SqlMapper.Query(_transaction.Connection, query, null, _transaction, commandType: CommandType.Text).FirstOrDefault();            
        }

        public IEnumerable<PGRoomMeta> GetPGRoomMetaDetails()
        {
            string query = string.Format("SELECT Url,PropertyType, Heading FROM SEO_Category");
            var roomMetaDetails = SqlMapper.Query<PGRoomMeta>(_transaction.Connection, query, null, _transaction, commandType: CommandType.Text);
            return roomMetaDetails;
        }
    }

    public interface IPropertyDA
    {
        IEnumerable<PropertyList> GetAllProperty(PropertySearchParameter input, out int totalRecord);        
        VMProperty GetProperty(int propertyId);
        LeadDetails ScheduleVisitVerify(LeadDetails entity);
        LeadDetails ScheduleVisit(LeadDetails entity);
        LeadDetails VeriyEnquiryDetail(LeadDetails entity);
        LeadDetails EnquiryDetail(LeadDetails entity);
        IEnumerable<PropertyList> GetFlat(bool isFurnish, bool isForBechelors);
        IEnumerable<PropertyList> GetFlatMate(string gender);
        IEnumerable<PropertyList> GetPG(string gender);
        IEnumerable<PropertyList> GetRoom();
        IEnumerable<PropertyList> GetWishListProperty(string propertyIds);
        IEnumerable<PropertyList> GetSimilarProperty(PropertySearchParameter input);
        IEnumerable<CityAreaMaster> GetCityLocalityList();
        IEnumerable<PropertyFeedBack> GetPGFeedBack(int pid);
        void AddPGFeedback(PropertyFeedBack entity);
        IEnumerable<PGRoomDetail> GetPGRoomDetail(int propertyId);
        dynamic GetPropertyListMetaTag(string categorySlug);
        IEnumerable<PGRoomMeta> GetPGRoomMetaDetails();
        IEnumerable<SEOCategory> GetSimilarCategories(string input);
    }
}
