﻿//using Dapper;
//using System.Data;
//using FH.App.Entity;
//using System.Collections.Generic;
//using System.Data.SqlClient;
//using System;

//namespace FH.App.Data
//{
//    public class SeoDA
//    {
//        string _connectionString = string.Empty;

//        public SeoDA()
//        {
//            _connectionString = ConnectionFactory.getConString;
//        }

//        public IEnumerable<T> Execute<T>()
//        {
//            using (IDbConnection con = new SqlConnection(_connectionString))
//            {
//                if (con.State == ConnectionState.Closed)
//                    con.Open();
//                try
//                {

//                }
//                catch (Exception)
//                {

//                    throw;
//                }
//                {
//                    var result = con.Query<T>("udsp_payment", PaymentParam(de,callValue), null, commandType: CommandType.StoredProcedure);
//                    return result;
//                }
//                catch (Exception e)
//                {
//                    throw new ApplicationException();
//                }
//                finally
//                {
//                    con.Dispose();
//                }
//            }
//        }

//        private DynamicParameters SEOParam(SEODE de, SEOCallValue callValue)
//        {
//            if (de == null)
//                de = new SEODE();

//            var param = new DynamicParameters();
//            param.Add("@CallValue", (int)callValue);
//            param.Add("@Id", de.Id);
//            param.Add("@Category", de.Category == null ? "" : de.Category);
//            param.Add("@Url", de.Url == null ? "" : de.Url);
//            param.Add("@MetaTitle", de.MetaTitle == null ? "" : de.MetaTitle);
//            param.Add("@MetaDesc", de.MetaDesc == null ? "" : de.MetaDesc);
//            param.Add("@MetaKeyword", de.MetaKeyword == null ? "" : de.MetaKeyword);
//            param.Add("@MetaJson", de.MetaJson == null ? "" : de.MetaJson);
//            param.Add("@CreatedBy", de.UserId);
//            param.Add("@PageNo", de.PageNo);
//            param.Add("@PageSize", de.PageSize);
//            param.Add("@PropertyId", de.PropertyId);
//            param.Add("@CategoryId", de.CategoryId);
//            param.Add("@City", de.City == null ? "" : de.City);
//            param.Add("@Lat", de.Lat == null ? "" : de.Lat);
//            param.Add("@Lng", de.Lng == null ? "" : de.Lng);
//            param.Add("@Heading", de.Heading == null ? "" : de.Heading);
//            param.Add("@HeadingContent", de.HeadingContent == null ? "" : de.HeadingContent);
//            param.Add("@PropertyType", de.PropertyType == null ? "" : de.PropertyType);
//            param.Add("@Prefix", de.Prefix == null ? "" : de.Prefix);
//            param.Add("@SearchKey", de.SearchKey);
//            return param;
//        }
//    }

//    public enum SEOCallValue
//    {
//        AddCategory = 1,
//        GetAllCategoryWithPaging = 2,
//        AddPropertyCategory = 3,
//        RemocePropertyCategory = 4,
//        GetPropertyCategory = 5,
//        GetCategryByAutoSearch = 6,
//        AddPropertySEO = 7,
//        GetCategoryById = 8,
//        UpdateCategory = 9,
//        DeleteCategory = 10,
//        AddLocality = 12,
//        GetAllLocalityBySearch = 13

//    }
//}