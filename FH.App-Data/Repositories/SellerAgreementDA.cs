﻿
using System.Data;
using Dapper;

namespace FH.App.Data.Repositories
{
    public class SellerAgreementDA
    {
        IDbTransaction _transaction;

        public SellerAgreementDA(IDbTransaction transaction)
        {
            _transaction = transaction;
        }

        public void UpdateAgreementStatus(int userId)
        {
            var param = new DynamicParameters();
            param.Add("@SellerId", userId);
            param.Add("@QueryType", 2);
            SqlMapper.Execute(_transaction.Connection, "udsp_vendor_agreement",param, _transaction, commandType: CommandType.StoredProcedure);            
        }
    }
}
